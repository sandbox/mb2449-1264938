<?php

/**
 * @file
 * Builds all the blocks needed by the module
 */

/**
 * a listing of available menu blocks
 */
function classifieds_generate_menu_blocks_list(&$blocks, $start) {

  //select all parent terms from the classifieds vid
  $vid = _classifieds_get_vid();

  //load up all parent terms
  $sql = "SELECT * FROM {term_data} AS a JOIN {term_hierarchy} AS b ON a.tid=b.tid WHERE a.vid=%d AND b.parent=0";
  $parent_terms = db_query($sql, $vid);

  while (($data = db_fetch_object($parent_terms)) !== FALSE) {
    $blocks[$start]['info'] = 'Classified Ad Menu: ' . $data->name;
    $blocks[$start]['cache'] = BLOCK_NO_CACHE;

    //incremement the count
    $start++;
  }
}

/**
 * generates the view block for menu blocks
 */
function classifieds_generate_menu_blocks_view(&$block, $delta, $start) {

  //select all parent terms from the classifieds vid
  $vid = _classifieds_get_vid();

  //find the parent terms
  $sql = "SELECT * FROM {term_data} AS a JOIN {term_hierarchy} AS b ON a.tid=b.tid WHERE a.vid=%d AND b.parent=0";
  $parent_terms = db_query($sql, $vid);

  while (($data = db_fetch_object($parent_terms)) !== FALSE) {
    //figure out if this is our hit...
    if ($start == $delta) {
      break;
    }

    //incremement the count
    $start++;
  }

  //create the list of items...
  $tid = $data->tid;
  $parent_url = _classifieds_sanitize_url($data->name);

  //load up the child terms
  $sql = "SELECT * FROM {term_data} AS a JOIN {term_hierarchy} AS b ON a.tid=b.tid WHERE a.vid=%d AND b.parent=%d ORDER BY a.weight";
  $child_terms = db_query($sql, $vid, $tid);
  $i = 0;

  while (($data_child = db_fetch_object($child_terms)) !== FALSE) {
    $url = url('classifieds/' . $parent_url . '/' . _classifieds_sanitize_url($data_child->name));
    $items[$i] = array('data' => '<a href="' . $url . '">' . $data_child->name . ' (' . classifieds_term_count_nodes($data_child->tid) . ')</a>', 'class' => 'leaf dhtml-menu');
    //advance the counter
    $i++;
  }

  //create the list
  $title = NULL;
  $type = 'ul';
  $attributes = array(
    'class' => 'menu',
  );

  //build an item list, then convert it to a menu
  $item_list = theme('item_list', $items, $title, $type, $attributes);
  $item_list = str_replace('item-list', 'menu-block', $item_list);

  //build the block
  $block['subject'] = '<a href="/classifieds/' . _classifieds_sanitize_url($data->name) . '">' . $data->name . ' (' . classifieds_term_count_nodes($data->tid) . ')</a>';
  $block['content'] = $item_list;
}

/**
 * Count the number of nodes that have a given tid associated with it
 */
function classifieds_term_count_nodes($tid) {

  //count the nodes that have the given tid associated with it, and are published
  $sql = "SELECT COUNT(*) FROM {term_node} AS a JOIN {node} AS b ON a.nid=b.nid WHERE a.tid=%d AND b.status=1";
  $count = db_result(db_query($sql, $tid));

  return $count;
}

//TODO: Rename all the simple_form stuff....
/**
 * This is the form that gets displayed for the search classifieds block
 * @param $form_state
 */
function classifieds_search_form($form_state) {

  //Search bar
  $form['search'] = array(
    '#title' => t('Enter ad keycode or search term(s)'),
    '#type' => 'textfield',
    //'#description' => 'blah blah blha',
    '#size' => 35,
  );

  //build array of parent categories
  $cats = array();
  $cats['any'] = '-Any Category-';
  $vid = _classifieds_get_vid();
  $taxonomy = taxonomy_get_tree($vid, 0, -1, 1);

  //drupal_set_message(dpm($taxonomy));
  //loop through and save
  foreach ($taxonomy as $id => $data) {
    $cats[$data->name] = $data->name;
  }

  //category drop down
  $form['cats'] = array(
    '#type' => 'select',
    '#options' => $cats,
    '#multiple' => FALSE,
  );

  //find button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Find'),
  );

  //return
  return $form;
}

/**
 * submit form for classifieds search - this finds the keyword
 * or passes the search on to the views
 */
function classifieds_search_form_submit($form, &$form_state) {

  $search = $form_state['values']['search'];
  $cat = $form_state['values']['cats'];

  if (strlen($search) == 3) {

    //look up keyword
    $sql = "SELECT nid FROM {classifieds_print_ads} AS a WHERE a.keyword='%s'";
    $nid = db_result(db_query($sql, $search));

    //check to see if keyword is valid
    if ($nid != '') {

      //look up the path...
      $src = 'node/' . $nid;

      $sql = "SELECT dst FROM {url_alias} AS a WHERE a.src='%s'";
      $dst = db_result(db_query($sql, $src));

      drupal_goto($dst);
    }
    else {
      //perform default search
      classifieds_search_form_redirect($search, $cat);
    }
  }
  else { //we need to just pass this to the view
    //call up redirect function
    classifieds_search_form_redirect($search, $cat);
  }
}

/**
 * classifieds redirect function for the search form
 */
function classifieds_search_form_redirect($search, $cat) {

  //format the search text
  $search = check_plain($search);
  $search = str_replace(' ', '+', $search);

  //figure out what cat to search
  if ($cat == 'any') {
    $cat = '';
  }
  else {
    $cat = '/' . classifieds_sanitize_url($cat);
  }

  //redirect to view
  drupal_goto('classifieds' . $cat, 'search=' . $search);
}

