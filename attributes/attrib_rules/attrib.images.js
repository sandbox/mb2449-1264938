// $Id: block.js,v 1.2 2007/12/16 10:36:53 goba Exp $

//THIS IS MODIFIED... I NEED TO GET MY DRAG TABLE TO RELOAD ON DROP... INSTEAD OF
//DISPLAYING THAT DUMB MESSAGE

// $Id: block.js,v 1.2 2007/12/16 10:36:53 goba Exp $

/**
 * Move a block in the blocks table from one region to another via select list.
 *
 * This behavior is dependent on the tableDrag behavior, since it uses the
 * objects initialized in that behavior to update the row.
 */


//this hides the message
Drupal.theme.prototype.tableDragChangedWarning = function () {
	  //return '<div class="warning">dfad' + Drupal.theme('tableDragChangedMarker') + ' ' + Drupal.t("Changes made in this table will not be saved until the form is submitted.") + '</div>';
      
};


/**
 * Stub function. Allows a custom handler when a row is dropped.
 * 
 * TODO: I eventually want to reload $form['field_image'] after every drop....not sure how though
 */
Drupal.tableDrag.prototype.onDrop = function() {
	
  // make the ajax request to reload this form
  //$.getJSON("http://classifieds.localhost/?q=classifieds/image_js/c-transport/field_image",
  //  function(data){
  //    // append the form to the container
  //    $('#field-image-items').replaceWith(data);           
  //  }
  //);
};