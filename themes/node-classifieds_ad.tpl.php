<?php

function classifieds_build_node($node){

//This is a custom node for classified ads to be included...

//debug
//dpm($node);

//include the collapsable javascript file
//drupal_add_js('misc/collapse.js');

//add in the java slide
$javascript = '
$(document).ready(function(){

  $(".slidingDiv").hide();
  $(".show_hide").show();

	$(".show_hide").click(function(){
	$(".slidingDiv").slideToggle();
	});

});
';
drupal_add_js($javascript, 'inline');

//build bread crumbs
if($node->path != ''){ //only if this isnt a preview...
  $arg = explode('/',$node->path);
  $term1 = taxonomy_get_term(_classifieds_get_term_by_name($arg[1]));
  $term2 = taxonomy_get_term(_classifieds_get_term_by_name($arg[2]));

  //breadcrumbs
  $breadcrumb = array();
  $breadcrumb[] = l('Home', '<front>'); //this doesnt show, but i dont care
  $breadcrumb[] = l('Classifieds', 'classifieds');
  $breadcrumb[] = l($term1->name, 'classifieds/'.$arg[1]);
  $breadcrumb[] = l($term2->name, 'classifieds/'.$arg[1].'/'.$arg[2]);
  $breadcrumb[] = l(drupal_get_title(), $node->path); // Link to current URL

  // Set Breadcrumbs
  drupal_set_breadcrumb($breadcrumb);

} else{ ?>

  <h2 id="page-title" class="inkline" style="font-size: 1.6em;"><?php print $node->title; ?></h2>

<?php
}
?>

<table border="0">
  <tr>
    <td colspan="2">
      <table>
        <tr style="font-style:italic;">
          <td rowspan="2" style="width:185px;vertical-align:middle;text-align:center;">
            <input type="submit" name="show_reply" value="~ CONTACT SELLER ~" class="show_hide" style="background-color:#131F50;color:#FFFFFF;" <?php if($node->path == ''){ print ' disabled="disabled"';} ?> />
          </td>
          <td>Location</td>
          <td>Seller</td>
          <td>Posted On</td>
        </tr>
        <tr>
          <td>
            <?php
              if($node->address != ''){
                print $node->address.'<br />';
              }
              print $node->location;
            ?>
          </td>
          <td>
            <?php
              print $node->seller;
            ?>
          </td>
          <td>
            <?php
              print format_date($node->created, 'custom', 'l, F jS \@ g:i a' );
            ?>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td style="height:1em;">
      <?php
        //Pull details out of the node
        $details_raw = array();

        //find parent cats
        $parent_array = _classifieds_get_parents();

        //add in the third category if it exists....
        if(count($node->taxonomy) == 3){

          //sort through taxonomy terms
          $terms = _classifieds_node_sort_taxonomy_terms($node);

          //sorted through the taxonomy and figured out
          //what tids do what
          //$parent_tid = taxonomy_get_term($terms[0]);
          $child_tid = taxonomy_get_term($terms[1]);
          $third_tid = taxonomy_get_term($terms[2]);

          //add details field
          $details_raw[] = array(
            0 => '<strong>Type of '.$child_tid->name.': </strong>',
            1 => $third_tid->name
          );
        }

        $forbidden_fields = array('field_image');

        //spit out details here...
        foreach($node->content as $key => $data){
          if(substr($key,0,6) == 'field_' && !in_array($key,$forbidden_fields)){
            $value = $node->$key;

            if($value[0]['view'] != ''){

                $details_raw[] = array(
                  0 => '<strong>'.$node->content[$key]['field']['#title'].': </strong>',
                  1 => $value[0]['view']
                );

                if(count($value) > 1){
                  $i=1;
                  while($i<count($value)){
                    $details_raw[] = array(
                      0 => '',
                      1 => $value[$i]['view']
                    );
                    $i++;
                  }
                }
            }

          }
        }

        //Build details table
        $details = array();

        //
        for($i=0;$i<count($details_raw);$i++){
           $details[] = array(
             $details_raw[$i][0],
             $details_raw[$i][1],
           );
        }

        //figure out if this is a preview or not. if it is a preivew
        //we cant show the form, or else it will fuck up the create ad form
        if($node->path != ''){
        ?>
          <div class="slidingDiv">
             <div id="classifieds-reply">
              <fieldset class="group-email">
                <legend>Reply</legend>
                  <?php
                     if($node->field_phone[0]['value'] != ''){
                       ?>

                <span style="font-style:italic;">Call:</span>
                <span style="font-size:1.4em;">

                      <?php print $node->field_phone[0]['value']; ?>
                </span>
                <?php } ?>

                <?php if($node->field_phone[0]['value'] != '' && $node->field_email[0]['email'] != ''){ ?>

                <hr />
                 <div style="text-align:center;margin-top:-1em;color:#999999;font-weight:bold;">OR</div>
                 <hr />
                 <?php } ?>

                   <?php
                     if($node->field_email[0]['email'] != ''){
                       print '<span style="font-style:italic;">Send E-mail Message: </span>';
                       print drupal_get_form('classifieds_reply_form',$node->nid,$node->title);
                     }
                   ?>
                <hr />
                <div style="width:100%;text-align:right;margin-top:-1em;"><a href="#reply" class="show_hide">HIDE</a></div>
              </fieldset>
            </div>
          </div>
        <?php } ?>
    </td>
    <td rowspan="2">
               <?php

              if(count($details) > 0){
                ?>

        <fieldset class="group-details" style="width:225px;">
          <legend>Details</legend>
            <div class="classifieds-phone">

              <?php
                print theme_table(null, $details);
              ?>


            </div>
        </fieldset>
            <?php
              }


              if($node->field_image[0]['view'] != ''){

              ?>

        <fieldset class="group-phone" style="width:225px;">
          <legend>Photos</legend>
            <div class="classifieds-phone">
              Click to enlarge.
              <?php

                //Figure out how many images this field has, and put them
                //into a nicely formatted table.

                $no_of_images = count($node->content['field_image']['field']['items']) - 4;

                //drupal_set_message($no_of_images);

                $data = array();

                for($i=0;$i<$no_of_images;$i=$i+2){

                  $data[] = array(
                    $node->content['field_image']['field']['items'][$i]['#children'],
                    $node->content['field_image']['field']['items'][$i+1]['#children'],
                  );

                }

                print theme_table(null, $data);
              ?>
            </div>
        </fieldset>

        <?php } ?>
    </td>
  </tr>
  <tr>
    <td style="font-size:1.2em;">
      <?php 
        print $node->content['body']['#value'];

        if($node->keycode != ''){
          print '<br /><em>Go directlyto this ad using keycode: '.$node->keycode.'</em>';
        }
      ?>
    </td>
  </tr>
</table>

<?php } ?>