<?php
/**
 * @file
 * Image Attribute Item
 */
class imagesAttribute extends itemAttribute {

  /**
   * buildAd
   */
  public function buildAd(&$form, $form_state) {

    //refactor snippet
    if (!isset($this->attributes['images'])) {
      //no image field attribute set, so hide the field_image element.
      //unset($form['field_image']);
      dpm('no image stuff... dont do build Ad');
    }



    //I need to get the database row
    $data = $this->getData($this->attributes[$this->name]);

    //set no of images
    $no_of_images = $data->images;
    $this->setNoOfImages($no_of_images);

    //count current number of images- incase the back button was hit
    $image_count = count($form['field_image']) - count($form['field_image']['field_image_add_more']) - 2;

    //delete any blank images (if you hit the previous button,
    //an extra blank image spot will be added)
    if ($image_count > 1 && $form['field_image'][$image_count - 1]['#default_value']['fid'] == 0) {
      unset($form['field_image'][$image_count - 1]);
    }

    //if the previous button is hit, we need to make sure
    //that the image script doesnt automatically add another field
    if ($image_count >= $no_of_images) { //ghost the submit button... and delete the extra photo fields...
      $dif = $image_count - $no_of_images; //this will be 1... but could possibly be more...
      //loop through and delete any extra photos
      while ($dif > 0) {

        //delete the photo
        unset($form['field_image'][$no_of_images + $dif - 1]);

        //manage counter
        $dif--;
      }

      //ghost the 'add another photo' button
      $form['field_image']['field_image_add_more']['#disabled'] = TRUE;
    }

    $sale_rules = $this->classifiedData['sale_rules'];

    //define variables...
    $free_photos = $sale_rules['images']['free'];
    $cost_per_photo = number_format($sale_rules['images']['price'], 2);

    //build form...
    $images_text = 'Include up to ' . $no_of_images . ' photos.';
    $images_text .= ' The first ' . $free_photos . ' photos are free, and each additional is $' . $cost_per_photo . '.';

    //include a form item for the description text...
    $form['images_text'] = array(
      '#type' => 'item',
      '#title' => 'something',
      '#description' => $images_text,
    );

    //print add more instructions
    $this->printMessage($form);
  }

  /**
   * searchFilter -
   * modifieds the search filter block according to images....
   */
  public function searchFilter(&$form, $form_state) {

    //I need to get the number of images
    $data = $this->getData($this->attributes[$this->name]);

    //if no images are allowed, remove the 'has photo' button
    if ($data->images == 0) {
      unset($form['image']);
    }
  }

  ///////////////HELPER FUNCTIONS///////////////////////////
  private function setNoOfImages($no_of_images) {
    $_SESSION['classifieds']['no_of_images'] = $no_of_images;
  }

  public function printMessage(&$form) {
    //Hijack the 'add another photo' button's ahah callback
    $ahah_path = explode('/', $form['field_image']['field_image_add_more']['#ahah']['path']);
    $ahah_path[0] = 'classifieds';
    $ahah_path = implode('/', $ahah_path);
    $form['field_image']['field_image_add_more']['#ahah']['path'] = $ahah_path;

    //change title of 'add another item'
    $form['field_image']['field_image_add_more']['#value'] = 'Add another photo';

    //eventually, i want to make it so that the images are automatically reloaded if they are moved, like when
    //you hit the 'add another item' button.
    $form['field_image']['#title'] = 'Add Photos';
  }

}

///////////////IMAGE CENTRIC/////////////////////////////
/**
 * This is called by form alter hook
 * @param type $form 
 */
function classifieds_attributes_images_mod_button(&$form) {

  //module_load_include('inc', 'classifieds', '/attributes/attrib_rules/attrib');

  $attributes = array();

  $img = new imagesAttribute($attributes); //attributes array doesnt matter, 
  //since the function we call  does the same thing regardless

  $img->printMessage($form);
}

/**
 * ADMIN FORMS
 */

/**
 * The elements that this attribute will add to the
 * create attribute admin form.
 */
function classifieds_attrib_images_form() {

  //check to see if the update field is here... if so modify the form.
  if (is_numeric(arg(5))) {
    //arg(4) for is an aid... use the aid to select your attrib_id
    //look up days
    $sql = "SELECT * FROM {classifieds_attrib_images} AS a JOIN {classifieds_attributes} as b ON a.attrib_id=b.attrib_id WHERE b.aid = %d";

    $result = db_query($sql, arg(5));

    $data = db_fetch_object($result); //this var will hold the default values...

    $images = $data->images;


    //set hidden field
    $form['update_images'] = array(
      '#type' => 'hidden',
      '#value' => $data->attrib_id,
    );
  }

  $options = array(0 => 'No Image', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

  $form['images'] = array(
    '#title' => t('Maximum number of images'),
    '#type' => 'select',
    '#default_value' => $images,
    '#options' => $options,
    '#multiple' => FALSE,
  );

  return $form;
}

/**
 * Submit the elements added to the form
 *
 * this has to save the form information, and return the id
 */
function classifieds_attrib_images_form_submit($form, $form_state) {

  //drupal_set_message(dpm($form));
  //check to see if we should insert or update(for edit forms)
  if (is_numeric($form['update_images']['#value'])) {

    //insert into database
    db_query('UPDATE {classifieds_attrib_images} SET images=%d WHERE attrib_id=%d', $form['images']['#value'], $form['update_images']['#value']);

    //return id
    $new_id = $form['update_images']['#value'];
  }
  else {
    //insert into database
    db_query('INSERT INTO {classifieds_attrib_images} (images) VALUES(%d)', $form['images']['#value']);

    //get id
    $new_id = db_last_insert_id('classifieds_attrib_images', 'attrib_id');
  }

  //return attrib_id
  return $new_id;
}

/**
 * deletes this attibute type
 */
function classifieds_attrib_images_delete($attrib_id) {

  $sql = "DELETE FROM {classifieds_attrib_images} WHERE attrib_id = %d";

  db_query($sql, $attrib_id);
}

/**
 * print out information for a images attribute given a attrib_id
 */
function _classifieds_attrib_images_print($attrib_id) {

  //lookup the number of days...
  $sql = "SELECT * FROM {classifieds_attrib_images} AS a WHERE a.attrib_id = %d";

  $result = db_query($sql, $attrib_id);

  $data = db_fetch_object($result);

  //format results
  $output = array(
    'name' => 'Images',
    'desc' => '<li><em>Images Allowed:</em> ' . $data->images . '</li>',
  );

  //return
  return $output;
}