<?php

/**
 * @file
 * Responsible for updating category attributes
 */

/**
 * page call back
 *
 * This is the 'list' page under admin/attributes
 */
function classifieds_admin_attributes_page() {

  //LIST DEFAULT ATTRIBUTES
  $output .= '<strong>Default Attributes</strong><br />';

  //add a colspan...$row[] = array('data' => 'b', 'colspan' => 2);
  $headers = array(
    array('data' => t('Type'), 'field' => 'attrib_type'),
    array('data' => t('Name'), 'field' => 'name'),
    array('data' => ''), //'colspan' => 2
  );

  //sql
  $sql = "SELECT a.aid,attrib_type,name FROM {classifieds_attributes} AS a JOIN {classifieds_attrib2cat} AS b ON a.aid = b.aid WHERE
  b.tid = 0";

  $sql .= tablesort_sql($headers);

  //run query
  $result = db_query($sql);

  //set up data array
  $data = array();
  $i = 1;

  //your basic while loop to get the data
  while ($tmp = db_fetch_array($result)) {

    //add links...
    $tmp['link_edit'] = l(t('Modify'), 'admin/classifieds/config/categories/' . $tmp['attrib_type'] . '/' . $tmp['aid']);

    //save the data
    $data[$i] = $tmp;

    //we dont want to show these fields
    unset($data[$i]['aid']);

    //advance the counter
    $i++;
  }

  //save to output
  $output .= theme('table', $headers, $data);


  //LIST REGULAR ATTRIBUTES
  $output .= '<strong>Category-Specific Attributes</strong><br />';

  //add a colspan...$row[] = array('data' => 'b', 'colspan' => 2);
  $headers = array(
    array('data' => t('Type'), 'field' => 'attrib_type'),
    array('data' => t('Name'), 'field' => 'name'),
    array('data' => t('Tids'), 'field' => 'tid'),
    array('data' => '') //'colspan' => 2
  );

  //sql
  $sql = "SELECT a.aid,attrib_type,name,tid FROM {classifieds_attributes} AS a JOIN {classifieds_attrib2cat} AS b ON a.aid = b.aid WHERE b.tid <>0 GROUP BY aid";

  $sql .= tablesort_sql($headers);

  //run query
  $result = db_query($sql);

  //set up data array
  $data = array();
  $i = 1;

  //your basic while loop to get the data
  while ($tmp = db_fetch_array($result)) {

    //add links...
    $tmp['link_edit'] = l(t('Modify'), 'admin/classifieds/config/categories/' . $tmp['attrib_type'] . '/' . $tmp['aid']);


    //find all attributes that are enabled
    //make sure that all attributes found are not in the query for all default tid's
    $sql_tids = "SELECT tid FROM {classifieds_attrib2cat} WHERE aid = %d";

    $result_tids = db_query($sql_tids, $tmp['aid']);

    $list = '<ul>';
    while (($data_tids = db_fetch_object($result_tids)) !== FALSE) {
      $term = taxonomy_get_term($data_tids->tid);
      $list .= '<li>' . $term->name . '</li>';
    }
    $list .= '</ul>';


    //overwrite tid to show all tids
    $tmp['tid'] = $list;


    //save the data
    $data[$i] = $tmp;

    //we dont want to show these fields
    unset($data[$i]['aid']);

    //advance the counter
    $i++;
  }


  //save to output
  $output .= theme('table', $headers, $data);

  //return
  return $output;
}

/**
 * Need a function to validate...
 * 
 * NOTE: I DONT THINK THIS CODE BELOW DOES ANYTHING... CAN PROBABLY BE DELETED....
 */
function classifieds_admin_attributes_validate($form, &$form_state) {

  //check to make sure the field is numeric...
  if (!is_numeric($form_state['values']['classifieds_ad_duration'])) {
    form_set_error('classifieds_ad_duration', t('This must be a number.'));
  }

  //and between 1 and 90 days.
  if ($form_state['values']['classifieds_ad_duration'] < 0 ||
      $form_state['values']['classifieds_ad_duration'] > 90) {
    form_set_error('classifieds_ad_duration', t('This must be between 1 and 90 days.'));
  }
}

/**
 * Need a function for submission
 */
function classifieds_admin_attributes_submit($form, &$form_state) {

  //drupal_set_message('some stuff'.dpm($form,TRUE));

  db_query("INSERT INTO {classifieds_attrib_duration} (days) VALUES (%d)", $form['classifieds_ad_duration']['#value']);

  //print out message...
  drupal_set_message('Attribute has been saved.');
}

/**
 * Update Taxonomy... does stuff if a term is deleted?
 *
 * if a tid is deleted... remove from the attributes tables
 */
function classifieds_attrib_remove_tid($tid) {

  //this will remove all attribute association with the the specified tid.
  $sql = "DELETE FROM {classifieds_attrib2cat} WHERE tid = %d";
  db_query($sql, $tid);

  //note that some attributes might now have no terms selected.
}

/**
 * Define the add attribute form. This form works for all attributes.
 *
 * This form will also handle edit/deletion.
 */
function classifieds_add_attribute_form($form_state, $attribute) {

  //include attribute file
  module_load_include('inc', 'classifieds', 'attributes/attrib_rules/attrib');

  //drupal_set_message(dpm($form_state));
  // create a delete variable in case the form is new
  if (!isset($form_state['storage']['delete'])) {
    $form_state['storage']['delete'] = FALSE;
  }

  // if the delete button has been clicked, we must show the delete confirmation.
  if ($form_state['storage']['delete'] == TRUE && is_numeric(arg(5))) {
    //the form is being deleted
    //get the attrib name
    $attrib = db_fetch_object(db_query("SELECT * FROM {classifieds_attributes} WHERE aid=%d", arg(5)));

    $form['question'] = array(
      '#title' => t('Are you sure?'),
      '#type' => 'item',
      '#description' => t('This action will delete "@name" and can not be undone.', array('@name' => $attrib->name)),
    );

    $form['aid'] = array(
      '#type' => 'hidden',
      '#value' => arg(5),
    );

    $form['attrib_type'] = array(
      '#type' => 'hidden',
      '#value' => $attrib->attrib_type,
    );

    $form['attrib_id'] = array(
      '#type' => 'hidden',
      '#value' => $attrib->attrib_id,
    );

    $form['submit_delete_confirm'] = array(
      '#type' => 'submit',
      '#value' => t('Confirm'),
    );
  }
  else { //show the regular form
    //Is this an edit form?
    $edit_form = FALSE;

    //determine if the argument has been set...
    if (is_numeric(arg(5))) {

      //this is an edit form.... so modify the form where appropriate.
      $edit_form = TRUE;

      //Get basic default values
      $sql = "SELECT * FROM {classifieds_attributes} AS a WHERE a.aid = %d";

      $result = db_query($sql, arg(5));

      $data = db_fetch_object($result); //this var will hold the default values...
      //save default values
      $aid = $data->aid;
      $name = $data->name;

      //Loop through category values
      $sql = "SELECT * FROM {classifieds_attrib2cat} AS a WHERE a.aid = %d";

      $result = db_query($sql, $aid);

      //start default value
      $cat_options = array();

      while (($data = db_fetch_object($result)) !== FALSE) {
        if ($data->tid == 0) {
          $default = 1; //set form value
        }
        else {
          $cat_options[$data->tid] = $data->tid;
        }
      }
    }

    //define attribute name for personal use
    $form['name'] = array(
      '#title' => t('Name'),
      '#type' => 'textfield',
      '#description' => t('Enter a human-readable name for admin purposes.'),
      '#default_value' => $name,
      '#maxlength' => 40,
      '#required' => FALSE,
      '#size' => 30,
    );

    $form['apply_to'] = array(
      '#type' => 'fieldset',
      '#title' => t('Apply attribute'),
      '#description' => 'Please choose at least one of these options.',
      '#collapsible' => TRUE,
    );

    $form['apply_to']['default'] = array(
      //'#title' => t('Special conditions'),
      '#type' => 'checkboxes',
      '#options' => array(1 => 'Default'),
      '#default_value' => array($default),
      '#description' => (t('Applies to all categories, unless overridden. <em>Note: There can only be one default per attribute. Also, if this is checked, any categories associated with this attribute will be deleted.</em>')),
    );

    $form['apply_to']['fields'] = array(
      '#type' => 'fieldset',
      '#title' => t('Select categories'),
      '#description' => t('Select what categories this attribute should be applied to.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['apply_to']['fields']['list'] = array(
      '#type' => 'select',
      //'#title' => t('Select which categories to apply this attribute to'),
      '#multiple' => TRUE,
      '#default_value' => $cat_options,
      '#size' => 9,
      '#options' => _classifieds_generate_cat_options(),
      '#description' => 'Press CTRL to select multiple fields.'
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add Attribute'),
      '#validate' => array('classifieds_add_attribute_form_validate_custom'), //only validate if we are submitting
      '#weight' => 100,
    );

    if ($edit_form == TRUE) {
      //modify the submit button
      $form['submit']['#value'] = 'Save Attribute';

      //add an edit button...
      $form['submit_delete'] = array(
        '#type' => 'submit',
        '#value' => t('Delete Attribute'),
        '#weight' => 105,
      );

      //include a hidden field
      $form['update'] = array(
        '#type' => 'hidden',
        '#value' => $aid,
      );
    }

    //given the attribute type, include the proper file
    require_once(dirname(__FILE__) . '/attrib_rules/attrib.' . $attribute . '.inc');

    //given the attribute type, run the proper function to
    //include it's specific form attributes
    $form_add = call_user_func('classifieds_attrib_' . $attribute . '_form');

    //insert a hidden field here... so we know what type of form this is...
    $form['type'] = array(
      '#type' => 'hidden',
      '#value' => $attribute,
    );

    //merge both forms
    $form = array_merge($form, $form_add); //NOTE: THIS IS DUMB, PASS A REFERENCE INSTEAD....
    //TEST STUFF
    //drupal_set_message(dpm($tax_tree));
  }

  return $form;
}

/**
 * Validate my attribute...
 */
function classifieds_add_attribute_form_validate_custom($form, $form_state) {

  //check to make sure that name is filled in
  if (!$form['name']['#value']) {
    form_set_error('name', t('Please enter a name for this attribute.'));
  }

  //check to make sure there isnt a default for this attribute type already...
  if ($form['apply_to']['default']['#value'][1] &&
      _classifieds_attrib_check_default($form['type']['#value'], $form['update']['#value']) == TRUE) {
    form_set_error('default', t('There is already a default set for this attribute type.'));
  }

  //check to make sure this attribute is a default or has a category selected
  if (empty($form['apply_to']['default']['#value']) &&
      empty($form['apply_to']['fields']['list']['#value'])) {

    form_set_error('default', t('This attribute must be applied to a category, or marked as default.'));
  }

  //figure out what type of attribute this is
  $attribute = $form['type']['#value'];

  //if categories are selected, we must make sure that each category only has one
  //attribute type associated wit hit
  if (!empty($form['apply_to']['fields']['list']['#value'])) {

    //loop through the values
    foreach ($form['apply_to']['fields']['list']['#value'] as $key => $value) {

      //the key is the tid
      // see if this tid already has a attribute of this type assigned to it
      $sql = "SELECT * FROM {classifieds_attrib2cat} AS a JOIN {classifieds_attributes} AS b ON a.aid=b.aid WHERE a.tid = %d AND b.attrib_type = '%s'";

      $result = db_query($sql, $key, $attribute);

      $data = db_fetch_object($result);

      if (!empty($data) && $data->aid != arg(4)) { //arg 4 is the aid
        //find category name
        $cat = db_fetch_object(db_query("SELECT name FROM {term_data} WHERE tid=%d", $data->tid));

        //this category has an attribute of this type, so set a form error...
        $cat_error .= 'The category "' . $cat->name . '" already has a attribute of this type assigned to it (<a href="' . url('admin/classifieds/attributes/' . $attribute . '/' . $data->aid) . '">' . $data->name . '</a>).<br />';
      }
    }

    //now that we ran through all the cats selected, see if it has built an error message
    if (isset($cat_error)) {
      form_set_error('list', $cat_error);
    }
  }

  //given the attribute type, include the proper file
  require_once(dirname(__FILE__) . '/attrib_rules/attrib.' . $attribute . '.inc');

  //given the attribute type, run the proper function to
  //include it's specific form attributes
  $form_add = call_user_func('classifieds_attrib_' . $attribute . '_form_validate', $form, $form_state);
}

/**
 * Submit my attribute...
 *
 */
function classifieds_add_attribute_form_submit($form, &$form_state) {

  //check to see if the delete button was clicked
  if ($form_state['clicked_button']['#value'] == $form_state['values']['submit_delete']) {

    //set the variables, so the form rebuilds a confirmation message
    $form_state['rebuild'] = TRUE;
    $form_state['storage']['delete'] = TRUE;
  }

  //check to see if the confirm delete button was clicked
  elseif ($form_state['clicked_button']['#value'] == $form_state['values']['submit_delete_confirm']) {
    //delete the item

    $attribute = $form_state['values']['attrib_type'];

    //call attribute function delete
    require_once(dirname(__FILE__) . '/attrib_rules/attrib.' . $attribute . '.inc');
    call_user_func('classifieds_attrib_' . $attribute . '_delete', $form_state['values']['attrib_id']);

    //delete call categories associated with this attribute
    $sql = "DELETE FROM {classifieds_attrib2cat} WHERE aid = %d";
    db_query($sql, $form_state['values']['aid']);

    //finally, delete the attribute itself
    $sql = "DELETE FROM {classifieds_attributes} WHERE aid = %d";
    db_query($sql, $form_state['values']['aid']);


    //message
    drupal_set_message('Attribute has been deleted.');

    //redirect the user
    $form_state['redirect'] = 'admin/classifieds/attributes';

    //delete this so drupal knows we need to submit the form
    unset($form_state['storage']);
  }

  //user this one for the submit button...
  elseif ($form_state['clicked_button']['#value'] == $form_state['values']['submit']) {

    //do normal save here...

    drupal_set_message('Attribute saved.');

    //we must save the selected attribute first
    $attribute = $form['type']['#value'];

    //given the attribute type, include the proper file
    require_once(dirname(__FILE__) . '/attributes/attrib.' . $attribute . '.inc');

    //given the attribute type, run the proper function to
    //include it's specific form attributes
    $attrib_id = call_user_func('classifieds_attrib_' . $attribute . '_form_submit', $form, $form_state);

    //now we must save the general attribute data
    //CHECK TO SEE IF THIS IS AN INSERT OR AND UPDATE(EDIT) using the update hidden field containt the aid....
    if (is_numeric($form['update']['#value'])) {

      // this is an edit form so update
      db_query("UPDATE {classifieds_attributes} SET name='%s' WHERE aid = %d", $form['name']['#value'], $form['update']['#value']);

      $aid = $form['update']['#value'];
    }
    else {
      // this is a new form
      db_query('INSERT INTO {classifieds_attributes} (attrib_id,attrib_type,name) VALUES(%d,"%s","%s")', $attrib_id, $attribute, $form['name']['#value']);

      //figure out the id number for the last record saved
      $aid = db_last_insert_id('classifieds_attributes', 'aid');
    }

    //if default has been checked, or this attribute already exists and is being edited, then we must
    //delete all old categorys associated with this aid
    if (is_numeric($form['update']['#value']) || $form['apply_to']['default']['#value'][0] == '1') {
      //delete all categorys associated with the aid
      db_query('DELETE FROM {classifieds_attrib2cat} WHERE aid=%d', $aid);
    }

    //finally we must save the category data
    if ($form['apply_to']['default']['#value'][1]) {
      //default is checked... only one record to save
      db_query('INSERT INTO {classifieds_attrib2cat} (aid,tid) VALUES(%d,%d)', $aid, 0);
    }
    else {
      //default is not checked.... sort through the fields...
      //loop through all the fields that are checked....
      foreach ($form['apply_to']['fields']['list']['#value'] as $key => $value) {

        //the key and the value are both the tid that this value is to be applied to....
        db_query('INSERT INTO {classifieds_attrib2cat} (aid,tid) VALUES(%d,%d)', $aid, $value);
      }
    }

    //We must do this or the form will rebuild instead of refreshing.
    unset($form_state['storage']);
  }
}

/**
 * Returns true of false whether a attribute type already
 * has a default value or not.
 */
function _classifieds_attrib_check_default($attrib_type, $aid) {

  //build sql statment to query for a default (tid=0) already saved in our tables
  $sql = "SELECT * FROM {classifieds_attrib2cat} AS a JOIN {classifieds_attributes} AS b ON a.aid = b.aid WHERE
  a.tid = 0 AND b.attrib_type ='%s'";

  // run query
  $result = db_query($sql, $attrib_type);

  $data = db_fetch_object($result);

  //return
  if ($data == FALSE) {
    //default has not been set
    return FALSE;
  }
  elseif ($data->aid == $aid) {
    //we are editing the default... so dont trigger the warning
    return FALSE;
  }
  else {
    //default has already been set
    return TRUE;
  }
}

/**
 * Returns a print out summary of a attribute, given a attrib_type, and attrib_id
 */
function _classifieds_attrib_print_summary($attrib_id, $attrib_type) {

  //call up the attribute specific print summary
  //given the attribute type, include the proper file
  //require_once(dirname(__FILE__) . '/attrib_rules/attrib.' . $attrib_type . '.inc');
  //given the attribute type, run the proper function to
  //include it's specific form attributes
  //$output = call_user_func('_classifieds_attrib_' . $attrib_type . '_print', $attrib_id);
  //return the output
  return $output;
}

