<?php
/**
 * @file
 * Sale rule: Images
 */

function classifieds_sales_images_add_form(&$form) {

  //check to see if the update field is here... if so modify the form.
  if (is_numeric(arg(5))) {
    //arg(5) for is an sid... use the sid to select your sale_id
    //look up days
    $sql = "SELECT * FROM {classifieds_sales_images} AS a JOIN {classifieds_sale_rules} as b ON a.sale_id=b.sale_id WHERE b.sid = %d";
    $result = db_query($sql, arg(5));
    $data = db_fetch_object($result); //this var will hold the default values...
    //set hidden field
    $form['update_images'] = array(
      '#type' => 'hidden',
      '#value' => $data->sale_id,
    );
  }


  $form['price'] = array(
    '#title' => t('Price per Image'),
    '#type' => 'textfield',
    '#description' => t('Enter the price to insert each image. Enter 0 for FREE, or a price (decimals allowed).'),
    '#default_value' => isset($data->price) ? $data->price : 0,
    '#maxlength' => 6,
    '#required' => TRUE,
    '#field_prefix' => '$',
    '#size' => 4,
  );

  $form['free'] = array(
    '#title' => t('No. of Free Images'),
    '#type' => 'textfield',
    '#description' => t('Enter the number of images allowed before charging.'),
    '#default_value' => isset($data->free) ? $data->free : 1,
    '#maxlength' => 2,
    '#required' => TRUE,
    '#size' => 2,
  );
  //return $form;  
}

/**
 * Validate the elements added to the form
 */
function classifieds_sales_images_add_form_validate($form, $form_state) {

  //make sure that this number is numeric
  if (!is_numeric($form_state['values']['free'])) {
    form_set_error('free', t('"No of Free Images" must be a number.'));
  }

  //make sure that this number is numeric
  if (!is_numeric($form_state['values']['price'])) {
    form_set_error('price', t('"Price" must be a number.'));
  }

  //make sure this number is within an acceptable range
  if ($form_state['values']['price'] < 0 || $form_state['values']['price'] > 100) {
    form_set_error('price', t('"Price" must be $0 (FREE) or greater, but less then $100'));
  }

  //make sure this number is within an acceptable range
  if ($form_state['values']['free'] < 0 || $form_state['values']['free'] > 10) {
    form_set_error('free', t('"No of Free Images" must be between 0 and 10'));
  }
}

/**
 * Submit the elements added to the form
 *
 * this has to save the form information, and return the id
 */
function classifieds_sales_images_add_form_submit($form, $form_state) {

  //check to see if we should insert or update(for edit forms)
  if (is_numeric($form['update_images']['#value'])) {

    drupal_set_message('attempting insertion');
    //insert into database
    db_query('UPDATE {classifieds_sales_images} SET price=%f, free=%d WHERE sale_id=%d', $form['price']['#value'], $form['free']['#value'], $form['update_images']['#value']);

    //return id
    $new_id = $form['update_images']['#value'];
  }
  else {
    //insert into database
    db_query('INSERT INTO {classifieds_sales_images} (price,free) VALUES(%f,%d)', $form['price']['#value'], $form['free']['#value']);

    //get id
    $new_id = db_last_insert_id('classifieds_sales_images', 'sale_id');
  }

  //return attrib_id
  return $new_id;
}

/**
 * Delete a base sale rule
 */
function classifieds_sales_images_delete($sale_id) {
  $sql = "DELETE FROM {classifieds_sales_images} WHERE sale_id = %d";
  db_query($sql, $sale_id);
}

/**
 * standard functions that define what the rule affects (or is it effects?)
 */
function classifieds_sales_images_is_online() {
  return TRUE;
}