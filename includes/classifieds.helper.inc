<?php
/**
 * @file
 * Contains extra functions for use with main module file.
 * TODO: sort through, remove old functions...
 */



/**
 * handler
 */
function _classifieds_add_to_path_auto($src, $dst) {
  db_query('INSERT INTO {url_alias} (src,dst) VALUES ("%s","%s")', $src, $dst);
}

function _classifieds_remove_from_path_auto($dst) {
  db_query('DELETE FROM {url_alias} WHERE dst="%s"', $dst);
}

/**
 * This function checks to see if an alias is already in the database
 * and if it belongs to the current node
 */
function _classifieds_is_alias_valid($nid, $alias) {

  //check to see if the alias already exists
  $sql = "SELECT * FROM {url_alias} WHERE dst LIKE '%s'";
  $result = db_fetch_object(db_query($sql, $alias));

  //we have something in the database already
  if ($result) {

    //check to see if we found a nodes own alias,
    //this could happen during edits
    if ($result->src == 'node/' . $nid) {
      return TRUE;
    }
    else {
      return FALSE; //this alias belongs to another node with a simliar name
    }
  }
  else { //there is nothing in the db with this alias
    return TRUE;
  }
}

/**
 * This function is responsible for generating alias for classified nodes
 * during nodeapi presave.
 */
function _classifieds_build_node_alias(&$node) {

  //load path auto file, NOTE: pathauto modules is required
  module_load_include('inc', 'pathauto', 'pathauto');

  //load sesssion variables for use in path
  $classified_data = classifieds_get_data($node->classified_id);

  //generate standard alias
  $alias = 'classifieds/' . pathauto_cleanstring($classified_data['parent_name']) . '/' . pathauto_cleanstring($classified_data['child_name']) . '/' . pathauto_cleanstring($node->title);

  //check to see if the alias is valid
  if (!_classifieds_is_alias_valid($node->nid, $alias)) {

    //set vars
    $int = 1; //used for adding an integer to the title
    $safe_to_use = FALSE;

    //Loop through alias's until we find one safe to use
    while ($safe_to_use == FALSE) {

      if (_classifieds_is_alias_valid($node->nid, $alias . $int)) {
        $safe_to_use = TRUE;
      }
      else {
        //still not a valid url, add to int and try again
        $int++;
      }
    }

    //resave alias, to new alias that is usuable
    $alias = $alias . $int;
  }

  //save the path alias
  $node->path = $alias;
}

/**
 * helper function that looks up a classified ad's status
 * given a node id
 * 0= created, not paid, not approved
 * 1= paid
 * 2= approved
 * 3= edited, not approved
 */
function _classifieds_get_status($nid) {
  $sql = "SELECT status FROM {classifieds_online_ads} AS a WHERE a.nid=%d";
  $db = db_fetch_object(db_query($sql, $nid));
  return $db->status;
}

/**
 * given a node, change it's status
 */
function _classifieds_set_status($nid, $status) {
  //perform database update operation
  db_query("UPDATE {classifieds_online_ads} SET status=%d WHERE nid=%d", $status, $nid);
}

/**
 * helper function to set the classified session id
 */
function _classifieds_set_session_id($classified_id) {
  $_SESSION['classified_id'] = $classified_id;
}

/*
 * Generates a list of categories, that can be included
 * in select list forms.
 */

function _classifieds_generate_cat_options() {
  //generate list of taxonomy terms
  $tax_tree = taxonomy_get_tree(_classifieds_get_vid(), $parent = 0, $depth = -1, $max_depth = 2);

  //cycle through the taxonomy tree
  foreach ($tax_tree as $key => $value) {

    $string = '';

    //determine whether this term is nested
    if ($value->parents[0] != 0) {
      $string = '--';
    }

    $opt[$value->tid] = $string . $value->name;
  } //end for each
  //return
  return $opt;
}

/**
 * Returns (and possibly creates) a new vocabulary for classifieds.
 * PRO DRUPAL DEV p337
 */
function _classifieds_get_vid() {
  $vid = variable_get('classifieds_vocabulary', '');

  if (empty($vid) || is_NULL(taxonomy_vocabulary_load($vid))) {
    // Check to see if an image gallery vocabulary exists.
    $vid = db_result(db_query("SELECT vid FROM {vocabulary} WHERE
    module='classifieds'"));
    if (!$vid) {
      $vocabulary = array(
        'name' => t('Classifieds'),
        'multiple' => '0',
        'required' => '0',
        'hierarchy' => '1',
        'relations' => '0',
        'module' => 'classifieds',
        'nodes' => array() //fix this... this vocab wont be correct
      );
      taxonomy_save_vocabulary($vocabulary);
      $vid = $vocabulary['vid'];
    }

    variable_set('classifieds_vocabulary', $vid);
  }

  return $vid;
}

/**
 * return a list of parent tids
 */
function _classifieds_get_parents() {

  //build a list of parent terms
  $sql = "SELECT * FROM {classifieds_node_per_cat} JOIN {term_data} ON classifieds_node_per_cat.tid = term_data.tid ORDER BY weight";

  $result = db_query($sql);
  while ($data = db_fetch_object($result)) {

    $options[] = $data->tid;
  }

  return $options;
}

/**
 * determines if this node type is a classified ad or not
 */
function _classifieds_is_classified_node($node_type) {

  //modify node_type to isolate only the name
  $pos = strpos($node_type, '_node_form');

  //node api will not have this ending on it... only form_alter will
  if ($pos != FALSE) {
    $node_type = substr($node_type, 0, $pos);
  }

  //array to hold values
  $nodes = array();

  //build a list of parent terms
  $sql = "SELECT type FROM {classifieds_node_per_cat}";
  $result = db_query($sql);
  while ($data = db_fetch_object($result)) {
    $nodes[] = $data->type;
  }

  //determine if the inputted node name is one of our classified types
  if (in_array($node_type, $nodes)) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * return a list of child tids given a parent tid
 */
function _classifieds_get_children($parent_tid, $depth=1) {

  //build a list of children terms
  $tree = taxonomy_get_tree(_classifieds_get_vid(), $parent_tid, -1, $depth);

  //create array to hold tid values
  $child_tids = array();

  foreach ($tree as $data) {
    $child_tids[] = $data->tid;
  }

  return $child_tids;
}

/**
 * given a $tid, return what parent it belongs too
 * @param unknown_type $tid
 */
function _classifieds_get_parent($tid) {

  //find parent term
  $sql = "SELECT * FROM {term_hierarchy} JOIN {term_data} ON term_hierarchy.tid = term_data.tid WHERE term_hierarchy.tid=%d";
  $result = db_query($sql, $tid);
  $data = db_fetch_object($result);

  //return results
  return $data->parent;
}

/**
 * Given an vocab term id $tid, it will return the content_type
 * @param unknown_type $tid
 */
function _classifieds_get_cat_node($tid) {

  //query for tid.. .if found... return the content type

  $sql = "SELECT * FROM {classifieds_node_per_cat} WHERE tid = %d";
  $result = db_query(db_rewrite_sql($sql), $tid);
  $data = db_fetch_object($result);

  if ($data->type != NULL) {
    return $data->type;
  }
  //if nothing is found... return blank array
  else {
    return 0;
  }
}

/**
 * helper function to remove classified ads from shopping cart
 */
function _classifieds_remove_from_uc_cart($nid) {

  //get cart items
  $items = uc_cart_get_contents();

  //sort through cart items, until we find our original item
  foreach ($items as $item) {
    if ($item->data['classifieds_nid'] == $nid) {
      //get rid of old cart item
      db_query("DELETE FROM {uc_cart_products} WHERE cart_item_id=%d", $item->cart_item_id);
      break; //we are done
    }
  }
}

/**
 * helper function to record sale
 */
function _classifieds_record_sale($nid, $order_id, $ad_summary, $price, $edit=0) {
  //save information into sales database-regardless of price
  db_query('INSERT INTO {classifieds_sales} (nid,order_id,ad_summary,price,edit) VALUES(%d,%d,"%s",%f,%d)', $nid, $order_id, serialize($ad_summary), $price, $edit);
}

/**
 * this helper function, given a nid, will look through the sales data
 * and return any results, sorted most recent first.
 */
function _classifieds_get_sale_records($nid) {

  //data array
  $sale_history = array();

  //loop through and pull results
  $result = db_query("SELECT * FROM {classifieds_sales} WHERE nid=%d ORDER BY timestamp DESC", $nid);
  while ($data = db_fetch_object($result)) {
    $sale_history[] = $data;
  }

  //return data array
  return $sale_history;
}

/**
 * clean up sales records... this removes all the edits in between payment
 */
function _classifieds_sales_records_clean_up($nid) {
  db_query('DELETE FROM {classifieds_sales} WHERE nid=%d and edit=1', $nid);
}

/**
 * determines if an ad has ever been payed for. (marked as paid)
 */
function _classifieds_has_completed_sale_record($nid) {

  $sql = "SELECT COUNT(*) FROM {classifieds_sales} WHERE nid=%d AND edit=0 ORDER BY timestamp DESC";

  if (db_result(db_query($sql, $nid)) > 0) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * censor getter and setter
 */
function _classifieds_get_censor($nid) {
  return db_query("SELECT censor FROM {classifieds_online_ads} WHERE nid=%d", $nid);
}

/**
 * censor setter
 * 0=unapproved
 * 1=approved
 * 2=approved, then edited
 */
function _classifieds_set_censor($nid, $censor) {

  //only publish node, if we are setting the censor to 1
  if ($censor == 1) {
    db_query("UPDATE {node} SET status=%d WHERE nid=%d", 1, $nid);
  }
  elseif ($censor == 0) {
    db_query("UPDATE {node} SET status=%d WHERE nid=%d", 0, $nid);
  }

  //perform db update
  db_query("UPDATE {classifieds_online_ads} SET censor=%d WHERE nid=%d", $censor, $nid);
}

/**
 * returns tid of term name in url form
 */
function _classifieds_get_term_by_name($name) {

  $name = str_replace('-', '%', $name);
  $name = check_plain($name);

  $sql = "SELECT * FROM {term_data} WHERE name LIKE '%s'";
  $result = db_query($sql, $name);
  $data = db_fetch_object($result);

  //return tid
  return $data->tid;
}

/**
 * helper funciton that sorts throough taxonomy terms
 */
function _classifieds_node_sort_taxonomy_terms($node) {

  $terms = taxonomy_node_get_terms($node);

  //find parent cats
  $parent_array = _classifieds_get_parents();

  //find the tid that is the parent... then find the child and third
  //since this taxonomy list is randomly produced
  $i = 0;
  foreach ($terms as $key => $data) {
    if (in_array($key, $parent_array)) {
      $parent_tid = $key;
      break;
    }
    $i++;
  }

  //now find the second tid
  $child_object = taxonomy_get_tree(_classifieds_get_vid(), $parent_tid, -1, 1);
  $child_array = array();

  //turn object into tid array
  foreach ($child_object as $key => $data) {
    //dpm($data->tid);
    $child_array[] = $data->tid;
  }

  //find child tid
  $i = 0;
  foreach ($terms as $key => $data) {
    if (in_array($data->tid, $child_array)) {
      $child_tid = $key;
      break;
    }
    $i++;
  }

  //find the third tid using elimination!
  $found_tids = array();
  $found_tids[] = $parent_tid;
  $found_tids[] = $child_tid;

  foreach ($terms as $key => $data) {
    if (!in_array($data->tid, $found_tids)) {
      $third_tid = $key;
      break;
    }
  }

  //return sorted array of tids
  return array(
    0 => $parent_tid,
    1 => $child_tid,
    2 => $third_tid,
  );
}

/**
 * helper function to see who can access the
 */
function _classifieds_user_tab_access($account) {
  global $user;

  // Anonymous users cannot use or view their ads.
  if (!$user->uid || !$account->uid) {
    return FALSE;
  }

  // User administrators should always have access to ads.
  if (user_access('administer users')) {
    return TRUE;
  }

  //If this is the actual user, accessing their own stuff, let them see it
  if ($user->uid == $account->uid) {
    return TRUE;
  }
}

/**
 * helper function to set new create ad
 */
function _classifieds_place_new_ad($child_tid, $parent_tid) {

  $_SESSION['classifieds_place_new_ad'] = array(
    'parent' => $parent_tid,
    'child' => $child_tid,
  );
}

//HELPER FUNCTIONS HERE TO STAY ARE BELOW... I HAVENT GONE THROUGH THE ONES ABOVE THIS POINT

/**
 * Save Data - This saves the extra form data
 * the classifieds system needs 
 */
function classifieds_save_data($classified_id, $data_array) {
  $data_array = serialize($data_array);
  $_SESSION['classifieds_data'][$classified_id] = $data_array;

  //DEBUG- who is saving to this thing
  /* $trace=debug_backtrace();
    $caller=array_shift($trace);
    $caller=array_shift($trace);

    dpm('called by '.$caller['function']);
    dpm($_SESSION['classifieds_data']); */
}

/**
 * Get Data - THis returns a data array the classified
 * system can use to store stuff in
 */
function classifieds_get_data($classified_id) {
  $data_array = unserialize($_SESSION['classifieds_data'][$classified_id]);
  return $data_array;
}

/**
 * Reset Data- This resets the classified ad system data. This
 * should be called the first time a create ad form is generated.
 */
function classifieds_new_data($classified_id) {
  $data_array = serialize(array());
  $_SESSION['classifieds_data'][$classified_id] = $data_array;
}

/**
 * cleans category title names up, so they can be used as links
 * @param unknown_type $text
 */
function _classifieds_sanitize_url($text) {
  //handle spaces
  $text = str_replace(" ", "-", strtolower($text));

  //handle ampersands
  $text = str_replace("&", "", strtolower($text));
  $text = str_replace("--", "-", strtolower($text));

  //remove any weird stuff that shouldnt be in there
  $text = check_plain($text);

  //return
  return $text;
}