<?php
/**
 * @file
 * This is a views plugin that allows a user to filter classified ads 
 * by seller type.
 */
class classifieds_views_seller_handler_field extends views_handler_field {

  function render($values) {

    //find node id
    $nid = $values->classifieds_online_ads_nid;

    //look up seller type now that we have the nid
    $sql = "SELECT seller FROM {classifieds_online_ads} WHERE nid=%d";
    $result = db_query($sql, $nid);
    $seller = db_result($result);

    if ($seller == 0) {
      $label = 'Private Party';
    }
    else {

      //find parent tid
      $sql = "SELECT tid FROM {term_node} WHERE nid=%d AND tid IN (SELECT tid FROM {classifieds_node_per_cat})";
      $result = db_query($sql, $nid);
      $parent_tid = db_result($result);

      //find child tid
      $sql = "SELECT tid FROM {term_node} WHERE nid=%d AND tid NOT IN (SELECT tid FROM {classifieds_node_per_cat})";
      $result = db_query($sql, $nid);
      $child_tid = db_result($result);

      //include file...
      module_load_include('inc', 'classifieds', '/attributes/attrib.seller.inc');

      //figure out what the lable is
      $label = classifieds_attrib_seller_find_commercial_label($parent_tid, $child_tid);
    }

    //return
    return $label;
  }

}