<?php

/**
 * @file
 * Duration Attribute Item
 */
class durationAttribute extends itemAttribute {

  /**
   * buildAd
   */
  public function buildAd(&$form, $form_state) {

    //given the attrib id... look up the info
    $data = $this->getData($this->attributes[$this->name]);

    //embed a hidden duration field that can be saved later...
    $form['duration'] = array(
      '#type' => 'hidden',
      '#value' => $data->days,
    );
  }

  /**
   * searchFilter -
   * modifieds the search filter block according to images....
   */
  public function searchFilter(&$form, $form_state) {
    return; //this does nothing...
  }

}

//////////////////////////ADMIN FUNCS//////////////////

/**
 * The elements that this attribute will add to the
 * create attribute admin form.
 */
function classifieds_attrib_duration_form() {

  //check to see if the update field is here... if so modify the form.
  if (is_numeric(arg(5))) {
    //arg(4) for is an aid... use the aid to select your attrib_id
    //look up days
    $sql = "SELECT * FROM {classifieds_attrib_duration} AS a JOIN {classifieds_attributes} as b ON a.attrib_id=b.attrib_id WHERE b.aid = %d";

    $result = db_query($sql, arg(5));

    $data = db_fetch_object($result); //this var will hold the default values...

    $days = $data->days;


    //set hidden field
    $form['update_duration'] = array(
      '#type' => 'hidden',
      '#value' => $data->attrib_id,
    );
  }

  $form['duration'] = array(
    '#title' => t('Duration'),
    '#type' => 'textfield',
    '#description' => t('Enter the number of days an ad should be run.'),
    '#default_value' => $days,
    '#maxlength' => 3,
    '#required' => TRUE,
    '#field_suffix' => 'days',
    '#size' => 3,
  );

  return $form;
}

/**
 * Validate the elements added to the form
 */
function classifieds_attrib_duration_form_validate($form, $form_state) {

  //drupal_set_message('potatoes');
  //make sure that this number is numeric
  if (!is_numeric($form_state['values']['duration'])) {
    // We notify the form API that this field has failed validation.
    form_set_error('duration', t('This must be a number.'));
  }

  //make sure this number is within an acceptable range
  if ($form_state['values']['duration'] < 1 || $form_state['values']['duration'] > 180) {
    // We notify the form API that this field has failed validation.
    form_set_error('duration', t('This must be between 1 and 180 days'));
  }
}

/**
 * Submit the elements added to the form
 *
 * this has to save the form information, and return the id
 */
function classifieds_attrib_duration_form_submit($form, $form_state) {

  //drupal_set_message(dpm($form));
  //check to see if we should insert or update(for edit forms)
  if (is_numeric($form['update_duration']['#value'])) {

    //insert into database
    db_query('UPDATE {classifieds_attrib_duration} SET days=%d WHERE attrib_id=%d', $form['duration']['#value'], $form['update_duration']['#value']);

    //return id
    $new_id = $form['update_duration']['#value'];
  }
  else {
    //insert into database
    db_query('INSERT INTO {classifieds_attrib_duration} (days) VALUES(%d)', $form['duration']['#value']);

    //get id
    $new_id = db_last_insert_id('classifieds_attrib_duration', 'attrib_id');
  }

  //return attrib_id
  return $new_id;
}

/**
 * print out information for a duration attribute given a attrib_id
 */
function _classifieds_attrib_duration_print($attrib_id) {

  //lookup the number of days...
  $sql = "SELECT * FROM {classifieds_attrib_duration} AS a WHERE a.attrib_id = %d";

  $result = db_query($sql, $attrib_id);

  $data = db_fetch_object($result);

  //format results
  $output = array(
    'name' => 'Duration',
    'desc' => '<li><em>Days:</em> ' . $data->days . '</li>',
  );

  //return
  return $output;
}

/**
 * deletes this attibute type
 */
function classifieds_attrib_duration_delete($attrib_id) {

  $sql = "DELETE FROM {classifieds_attrib_duration} WHERE attrib_id = %d";

  db_query($sql, $attrib_id);
}
