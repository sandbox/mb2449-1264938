<?php
/**
 * @file
 * Cant remember what this does... something with views
 */
class classifieds_views_parent_category_handler_field extends views_handler_field {

  function render($values) {

    //find node id
    $nid = $values->classifieds_online_ads_nid;

    //find parent tid
    $sql = "SELECT tid FROM {term_node} WHERE nid=%d AND tid IN (SELECT tid FROM {classifieds_node_per_cat})";
    $result = db_query($sql, $nid);
    $data = db_result($result);

    //find parent name
    $get_term = taxonomy_get_term($data);

    //return
    return $get_term->name;
  }

}