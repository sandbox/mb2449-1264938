<?php

/**
 * @file
 * Sale rule: Seller Increase (Print)
 */
function classifieds_sales_print_seller_add_form(&$form) {

  //check to see if the update field is here... if so modify the form.
  if (is_numeric(arg(5))) {
    //arg(5) for is an sid... use the sid to select your sale_id
    //look up days
    $sql = "SELECT * FROM {classifieds_sales_print_seller} AS a JOIN {classifieds_sale_rules} as b ON a.sale_id=b.sale_id WHERE b.sid = %d";
    $result = db_query($sql, arg(5));
    $data = db_fetch_object($result); //this var will hold the default values...
    //set hidden field
    $form['update_seller'] = array(
      '#type' => 'hidden',
      '#value' => $data->sale_id,
    );
  }

  $form['commercial'] = array(
    '#title' => t('Commercial'),
    '#type' => 'textfield',
    '#description' => t('Enter any additional charges to apply on top of a set base price (decimals allowed).'),
    '#default_value' => isset($data->commercial) ? $data->commercial : 0,
    '#maxlength' => 6,
    '#required' => TRUE,
    '#field_prefix' => '$',
    '#size' => 4,
  );

  $form['private'] = array(
    '#title' => t('Private'),
    '#type' => 'textfield',
    '#description' => t('Enter any additional charges to apply on top of a set base price (decimals allowed).'),
    '#default_value' => isset($data->private) ? $data->private : 0,
    '#maxlength' => 6,
    '#required' => TRUE,
    '#field_prefix' => '$',
    '#size' => 4,
  );

  //return $form;  
}

/**
 * Validate the elements added to the form
 */
function classifieds_sales_print_seller_add_form_validate($form, $form_state) {

  //make sure that this number is numeric
  if (!is_numeric($form_state['values']['commercial'])) {
    form_set_error('commercial', t('"Commercial" must be a number.'));
  }

  //make sure that this number is numeric
  if (!is_numeric($form_state['values']['private'])) {
    form_set_error('private', t('"Private" must be a number.'));
  }

  //make sure this number is within an acceptable range
  if ($form_state['values']['private'] < 0 || $form_state['values']['private'] > 100) {
    form_set_error('private', t('"Private" must be $0 (FREE) or greater, but less then $100'));
  }

  //make sure this number is within an acceptable range
  if ($form_state['values']['commercial'] < 0 || $form_state['values']['commercial'] > 100) {
    form_set_error('commercial', t('"Commercial" must be $0 (FREE) or greater, but less then $100'));
  }
}

/**
 * Submit the elements added to the form
 *
 * this has to save the form information, and return the id
 */
function classifieds_sales_print_seller_add_form_submit($form, $form_state) {

  //check to see if we should insert or update(for edit forms)
  if (is_numeric($form['update_seller']['#value'])) {
    //insert into database
    db_query('UPDATE {classifieds_sales_print_seller} SET commercial=%f,private=%f WHERE sale_id=%d', $form['commercial']['#value'], $form['private']['#value'], $form['update_seller']['#value']);

    //return id
    $new_id = $form['update_seller']['#value'];
  }
  else {
    //insert into database
    db_query('INSERT INTO {classifieds_sales_print_seller} (commercial,private) VALUES(%f,%f)', $form['commercial']['#value'], $form['private']['#value']);

    //get id
    $new_id = db_last_insert_id('classifieds_sales_print_seller', 'sale_id');
  }

  //return attrib_id
  return $new_id;
}

/**
 * Delete a base sale rule
 */
function classifieds_sales_print_seller_delete($sale_id) {
  $sql = "DELETE FROM {classifieds_sales_print_seller} WHERE sale_id = %d";
  db_query($sql, $sale_id);
}

function classifieds_sales_print_seller_is_online() {
  return FALSE; //this is a periodical only attribute
}