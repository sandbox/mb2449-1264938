<?php

/**
 * @file
 * Responsible for all things having to do with creating an ad
 */

/*
 * This file contains the abstract class that is responsible for 
 * building a classified ad
 */

class buildAd {

  /**
   * VARS
   */
  private $classifiedData; //holds the classified_data array
  private $isFirstLoad = FALSE; //determines whether the page is loaded
  private $classifiedID; //holds the id number for classified data save...
  private $isFirstEdit = FALSE; //determines whether this is an edit node page's
  //  first time being called up
  private $defaultValues; //this hold the default values

  /**
   * CONSTRUCTOR
   * @param type $form
   * @param type $form_state
   */

  public function __construct(&$form, &$form_state, $action=NULL) {

    //includes sales and attributes classes
    module_load_include('inc', 'classifieds', '/sales/classifieds.sales');
    module_load_include('inc', 'classifieds', '/publish/classifieds.publish');
    module_load_include('inc', 'classifieds', '/attributes/classifieds.attributes');
    module_load_include('inc', 'classifieds', '/includes/classifieds.detail_fields');
    module_load_include('inc', 'classifieds', '/includes/classifieds.ad_summary');

    //This is a regular form_alter hook
    if (empty($action)) {

      //This function must be called every page load
      $this->preliminary($form, $form_state);

      //multi-page switch$form_state['storage']['step']
      switch ($form_state['storage']['step']) {

        case 1: //STEP 1: CREATE AD
          $this->step1($form, $form_state);
          break;

        case 2: //STEP 2: PREVIEW
          $this->step2($form, $form_state);
          break;

        case 3: //STEP 3: PRINT
          $this->step3($form, $form_state);
          break;
      }
    }

    //this is for the nodeapi... for validation, and submission
    else {

      switch ($action) {

        case 'validate':
          $this->validate($form, $form_state);
          break;

        case 'submit':
          $this->submit($form, $form_state);
          break;
      }
    }
  }

  /**
   * dummy constructor... for checked out
   */
  //public function __construct(){
  //pull up classified data?
  //}

  /**
   * PERFORM REDIRECTS, COLLECT PRELIMINARY PARENT/CHILD DATA
   */
  //preliminary information that must be collected
  private function preliminary(&$form, &$form_state) {

    //check to see if this is an edit, and if we need to redirect
    if (isset($form['nid']['#value'])) {
      if (!arg(3)) {
        drupal_goto('node/' . arg(1) . '/edit/classifieds');
      }

      //if this is an edit, load cat data
      $cat_data = array(
        'parent' => taxonomy_get_term($form['#node']->parent_tid),
        'child' => taxonomy_get_term($form['#node']->child_tid),
      );
    }

    //Make sure a session var is set or else, kick them out
    elseif (!isset($_SESSION['classifieds_place_new_ad']) && !isset($form['nid']['#value'])
        && !isset($form_state['storage']['step'])) { //only do this on the first page load...
      //redirect to home page
      drupal_goto();
    }

    //Else this is a new load and we need to  load cat data
    else {

      $cat_data = array(
        'parent' => taxonomy_get_term($_SESSION['classifieds_place_new_ad']['parent']),
        'child' => taxonomy_get_term($_SESSION['classifieds_place_new_ad']['child']),
      );

      //get rid of this var
      unset($_SESSION['classifieds_place_new_ad']);
    }

    /**
     * Perform host of other prebuild activity...
     */
    //Run the first load function
    $this->firstLoad($form, $form_state, $cat_data);

    //Set session data
    $this->setSessionData($form, $form_state);

    //Set form action
    $this->setFormAction($form, $form_state);

    //hide default node add output /modify some things
    $this->modForm($form, $form_state);

    //set default values (if edit)
    $this->setDefaultValues($form, $form_state);

    //load up attributes class (to be built) to modify detail fields
    $this->setupDetailFields($form, $form_state);
  }

  /*
   * FIRST LOAD
   */

  function firstLoad(&$form, &$form_state, $cat_data) {

    if (!isset($form_state['storage']['step'])) {

      //mark the var for other funcs
      $this->isFirstLoad = TRUE;

      //set the form step
      $form_state['storage']['step'] = 1;

      //generate a unique id for this ad
      $unique_id = uniqid('classifieds_', TRUE);
      $form_state['storage']['classified_id'] = $unique_id;
      $this->classifiedID = $unique_id;

      //create new session var
      classifieds_new_data($form_state['storage']['classified_id']);

      //we just reset the session data, so call up the data again (it will now be blank)
      $this->classifiedData = classifieds_get_data($form_state['storage']['classified_id']);

      //check to see if this an edit node form's first page
      if (isset($form['nid']['#value'])) {
        //$form_state['values']['first_edit_load'] = TRUE;
        $this->isFirstEdit = TRUE;

        //save status in classified data array for future use
        //NOT SURE WHAT FUTURE USE...
        $this->classifiedData['edit'] = _classifieds_get_status($form['nid']['#value']);
      }

      //save tid's
      $this->classifiedData['child_tid'] = $cat_data['child']->tid;
      $this->classifiedData['parent_tid'] = $cat_data['parent']->tid;

      //save the tid's name
      $this->classifiedData['child_name'] = $cat_data['child']->name;
      $this->classifiedData['parent_name'] = $cat_data['parent']->name;

      //load most current set of sales rules
      $sales = new sales($this->classifiedData['child_tid'], $this->classifiedData['parent_tid']);
      $this->classifiedData['sale_rules'] = $sales->getSaleRules();

      //save a copy of classifieds data for the ad summary first run
      classifieds_save_data($this->classifiedID, $this->classifiedData);

      //build the first run data for the ad summary block
      classifieds_ad_summary_first_run($this->classifiedID);

      //reload modified classified data
      $this->classifiedData = classifieds_get_data($this->classifiedID);
    }
    //This page has just changed
    else {

      //Call up our data array to hold our classified ad data
      $this->classifiedData = classifieds_get_data($form_state['storage']['classified_id']);
      $this->classifiedID = $form_state['storage']['classified_id'];
    }
  }

  /**
   * set session data
   */
  private function setSessionData(&$form, &$form_state) {

    //Set the form_id session var so the ad_summary block knows whats up
    _classifieds_set_session_id($this->classifiedID);

    //put the classifieds_id in the form as well
    $form['classified_id'] = array(
      '#type' => 'hidden',
      '#value' => $this->classifiedID,
    );
  }

  /**
   * set form action
   */
  private function setFormAction(&$form, &$form_state) {

    if (isset($form['nid']['#value'])) { //Edit form
      //node data is already loaded...
      $form['#action'] = url('../node/' . $form['nid']['#value'] . '/edit/classifieds');
    }
    else { //New form
      //find name
      $clean_name = strtolower(_classifieds_sanitize_url($this->classifiedData['parent_name']));

      //Set the action, or it will redirect if ahah is called,
      //then the validation immediately fails...
      $form['#action'] = url('../classifieds/place/' . $clean_name);
    }


    //add another submit...
    //$form['#submit'][0] = 'menu_node_form_submit';
    $form['#submit'][1] = 'classifieds_create_ad_submit';

    //add a validation function
    $form['#validate'][2] = 'classifieds_create_ad_validate';

    //Set a hidden field letting us know what page we are on....
    $form['step'] = array(
      '#type' => 'hidden',
      '#value' => $form_state['storage']['step'],
    );
  }

  /**
   * 
   */
  private function modForm(&$form, &$form_state) {

    //set theme, misc items, etc
    $form['#theme'] = classifieds_create_ad;
    $form['title']['#size'] = 40;
    $form['title']['#maxlength'] = 40;

    //get rid of menu form items we dont need
    unset($form['menu']);
    unset($form['body_field']['format']);
    unset($form['body_field']['teaser_include']);
    unset($form['revision_information']);
    unset($form['comment_settings']);
    unset($form['author']);
    unset($form['options']);
    unset($form['locations']);
    //unset($form['path']); //- this gets done at the create ad theme instead
    //Add in category and sub-category breadcrumbs
    $form['c_cat'] = array(
      '#title' => t('Category'),
      '#type' => 'item',
      '#description' => $this->classifiedData['parent_name'],
      '#weight' => -10,
    );

    $form['c_sub_cat'] = array(
      '#title' => t('Sub-Category'),
      '#type' => 'item',
      '#description' => $this->classifiedData['child_name'],
      '#weight' => -8,
    );

    //change create ad button name, based on status
    if (isset($classified_data['edit'])) {
      $form['buttons']['submit']['#value'] = 'Modify My Ad';
    }

    //Unset delete button if this is an edit form
    if (isset($form['buttons']['delete'])) {
      unset($form['buttons']['delete']);
    }
  }

  /**
   * 
   */
  private function setDefaultValues(&$form, &$form_state) {

    //Pull up previously filled in form values
    if (isset($form_state['storage']['values'][$form_state['storage']['step']])) {
      $this->defaultValues = $form_state['storage']['values'][$form_state['storage']['step']];
    }

    //fill in default fields for title and body if this isn't an edit forms first load
    if ($this->isFirstEdit != TRUE) {
      //list default values that will only be shown when it is not an edit form?
      $form['title']['#default_value'] = $form_state['storage']['values'][1]['title'];
      $form['body_field']['body']['#default_value'] = $form_state['storage']['values'][1]['body'];
      $form['field_taxonomy']['#default_value'] = array(value => $this->classifiedData['child_tid']);
    }
  }

  /**
   * This function is responsible for setting up and hiding unneccassary detail fields.
   * Also sets up attributes!
   * 
   */
  private function setupDetailFields(&$form, &$form_state) {

    //load up attributes with our tid info
    $attributes = new attributes($this->classifiedData, $this->defaultValues, $this->isFirstEdit, $this->isFirstLoad);

    //have the attributes modify our build ad form
    $attributes->buildAd($form, $form_state);

    //REFACTOR, PUT THE ABOVE IN ITS OWN FUNC... MAKE CONSTRUCTOR SHORTER
  }

  /*
   * 
   */

  protected function step1(&$form, &$form_state) {

    //call up detail_field object
    $detailFields = new detailFields($this->classifiedData['child_tid'], $this->classifiedData['parent_tid'], $this->defaultValues);

    //modify the form
    $detailFields->modifyAd($form, $form_state);




    //retrieve default values
    $default_value = $this->defaultValues;

    // Hide the submit button.
    $form['buttons']['submit']['#access'] = FALSE;

    // Change the 'preview' button to 'Next' and set the submit handler.
    $form['buttons']['preview']['#value'] = 'Continue >>';

    //modify body field
    $form['body_field']['body']['#required'] = TRUE;
    $form['body_field']['body']['#title'] = 'Description';
    $form['body_field']['body']['#rows'] = 6;

    //add state, city, and address fields
    $form['group_contact']['location'] = array(
      '#type' => 'fieldset',
      '#title' => t('Location'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#weight' => 30,
    );
    $form['group_contact']['location']['address'] = array(
      '#title' => t('Address'),
      '#type' => 'textfield',
      '#size' => 35,
      '#maxlength' => 40,
      '#weight' => 5,
    );
    $form['group_contact']['location']['city'] = array(
      '#title' => t('City'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 20,
      '#maxlength' => 30,
      '#weight' => 10,
      '#autocomplete_path' => 'classifieds/cities'
    );
    $form['group_contact']['location']['state'] = array(
      '#title' => t('State'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 20,
      '#maxlength' => 30,
      '#weight' => 15,
      '#autocomplete_path' => 'classifieds/states'
    );

    //add defaults to contact info
    if ($this->isFirstEdit == TRUE) { //first edit load, pull up stored values
      //only need to do this on first page load of edit page....
      $form['group_contact']['location']['city']['#default_value'] = $form['#node']->city;
      $form['group_contact']['location']['address']['#default_value'] = $form['#node']->address;
      $form['group_contact']['location']['state']['#default_value'] = $form['#node']->state;
    }
    else { //else load the regular default values
      $form['group_contact']['location']['address']['#default_value'] = isset($default_value['address']) ? $default_value['address'] : '';
      $form['group_contact']['location']['city']['#default_value'] = isset($default_value['city']) ? $default_value['city'] : '';
      $form['group_contact']['location']['state']['#default_value'] = isset($default_value['state']) ? $default_value['state'] : 'Pennsylvania';
    }
  }

  /**
   * STEP 2
   */
  protected function step2(&$form, &$form_state) {

    //hide all the other fields now....
    $this->clearFields($form, array('preview', 'c_cat', 'c_sub_cat'));

    //then we need to cache the preview
    if (isset($form['#prefix'])) {

      //build a form item to hold the preview
      $_SESSION['classified_preview'] = $form['#prefix'];

      //delete the preview
      unset($form['#prefix']);
    }

    //get information
    $email = $form_state['storage']['values'][1]['field_email'][0]['email'];
    $phone = $form_state['storage']['values'][1]['field_phone'][0]['value'];
    $poc_string = "";

    //build nice contact info string
    if (!empty($email)) {
      $poc_string .= $email . ' (e-mail*)';
      $fine_print = '<br /><strong>*</strong><em>Your e-mail is only shown to confirm that it is correct. Your e-mail address will not be displayed on your ad.</em>';
    }

    if (!empty($phone)) {
      if (!empty($poc_string)) {
        $poc_string .= '<br />';
      }

      $poc_string .= $phone . ' (phone)';
    }

    //show preview
    $form['preview'] = array(
      '#prefix' => $_SESSION['classified_preview'],
      '#value' => '<div id="classifieds-preview-statement-wrapper">
                        <div id="classifieds-preview-poc"><strong>POINT OF CONTACT:<br />' . $poc_string . '</strong>
                          <span id="classifieds-preview-small">' . $fine_print . '</span>
                        </div>
                        <div id="classifieds-preview-statement">The above is a preview of your online ad. If you are satisfied, click continue.</div>
                      </div>',
    );



    // Hide the submit button.
    $form['buttons']['submit']['#access'] = FALSE;
    $form['buttons']['preview']['#access'] = FALSE;

    // Change the 'preview' button to 'Next' and set the submit handler, in order to move
    // to additional pages

    $publish = new publish($classifiedData);

    if ($publish->pickupAd()) { //this category gets picked up
      $form['buttons']['next'] = array(
        '#type' => 'submit',
        '#value' => t('Continue >>'),
        '#weight' => 50,
      );
    }
    else { //show create ad button
      $form['buttons']['submit']['#access'] = TRUE;
      $form['buttons']['submit']['#weight'] = 50;
      $form['buttons']['submit']['#value'] = 'Create My Ad';
    }

    $form['buttons']['previous'] = array(
      '#type' => 'submit',
      '#value' => t('<< Previous'),
      '#weight' => 48,
    );
  }

  /**
   * STEP 3
   */
  protected function step3(&$form, &$form_state) {

    //hide all the other fields now....
    $this->clearFields($form, array('c_cat', 'c_sub_cat'));

    //Change the title
    drupal_set_title('Publish Your Ad');

    //Setup previous button
    $form['buttons']['previous'] = array(
      '#type' => 'submit',
      '#value' => t('<< Previous'),
      '#weight' => 48,
    );

    // Hide the preview button.
    $form['buttons']['preview']['#access'] = FALSE;

    //Change the title of the Submit button
    $form['buttons']['submit']['#value'] = 'Create My Ad';
    $form['buttons']['submit']['#weight'] = 50;

    //save classifiedData before going to publish(where it will be changed)
    classifieds_save_data($this->classifiedID, $this->classifiedData);

    //Call up publish buildAd()
    $publish = new publish($this->classifiedID);

    //add in form details
    $publish->setFormData($this->defaultValues, $this->isFirstEdit, $this->isFirstLoad);

    //modify step 3
    $publish->step3($form, $form_state);
  }

  /**
   * VALIDATE
   */
  protected function validate($form, &$form_state) {

    //set session id - This has to get called again or ad block will drop the info(i think)
    _classifieds_set_session_id($form_state['storage']['classified_id']);

    //load session data
    $classified_data = classifieds_get_data($form['classified_id']['#value']);

    //validate third tid if there
    if (isset($form['field_sub_cat_menu'])) {
      if ($form_state['values']['field_sub_cat_menu'] == $classified_data['child_tid']) {
        form_set_error('field_sub_cat_menu', t('You must enter ' . $form['field_sub_cat_menu']['#title'] . '.'));
      }
    }

    //make sure an e-mail and/or phone is entered
    if ($form_state['storage']['step'] == 1) {
      if (!$form['group_contact']['field_phone'][0]['#value']['value'] && !$form['group_contact']['field_email'][0]['#value']['email']) {
        // We notify the form API that this field has failed validation.
        form_set_error('field_phone', t('Please enter a phone number or e-mail address.'));
      }
    }


    //validate prices if this is the submit button getting hit
    if ($form_state['clicked_button']['#id'] == 'edit-submit') {

      //module_load_include('inc', 'classifieds', '/sales/classifieds_sales');
      //check the values of the form one last time, to prevent
      //someone from disabling javascript and getting some freebies
      //we dont use the $price var because of edits
      $calculated_price = classifieds_sales_calculate_form($form);


      //dpm($calculated_price);
      //dpm($classified_data['ad_summary']['total']);


      if ($calculated_price != $classified_data['ad_summary']['total']) {
        //Refuse to submit the ad....
        form_set_error('');
        drupal_set_message('There has been an error, please be sure JavaScript has not been turned off.
          If you believe you are receieving this message in error, please contact the site administrator.', 'error');
      }
    }
  }

  /**
   * SUBMIT
   */
  protected function submit($form, &$form_state) {


    //Save the values for the current step into the storage array.
    $form_state['storage']['values'][$form_state['storage']['step']] = $form_state['values'];

    //Check the button that was clicked and change the step.
    if ($form_state['clicked_button']['#id'] == 'edit-previous') {
      $form_state['storage']['step']--;
    }
    elseif ($form_state['clicked_button']['#id'] == 'edit-next') {
      $form_state['storage']['step']++;
    }

    //special case for preview
    elseif ($form_state['storage']['step'] == 1 && $form_state['clicked_button']['#id'] == 'edit-preview') {
      $form_state['storage']['step']++;
    }
    elseif ($form_state['storage']['step'] == 3 && $form_state['clicked_button']['#id'] == 'edit-preview') {
      $form_state['storage']['step']--;
    }

    //last step is to save
    elseif ($form_state['clicked_button']['#id'] == 'edit-submit') {
      //merge all the form values so that they are saved automatically
      $form_state['values'] = array_merge($form_state['values'], $form_state['storage']['values'][3], $form_state['storage']['values'][1]);
    }
  }

  /**
   * CHECKED OUT (called after checkout is complete)
   */
  public function checkedOut(&$node, $case='insert') {

    /**
     * This will either be a update or insert
     * If it is an insert, it will either be before, or after pay
     */
    //get the product nid
    //get classified data array
    $classified_data = classifieds_get_data($node->classified_id);

    //determine the price of the incoming classified product
    if (isset($classified_data['ad_summary']['new_total'])) {
      //this is an edit, so we calc price differently
      $price = $classified_data['ad_summary']['new_total'];
    }
    else {
      $price = $classified_data['ad_summary']['total'];
    }

    //build cart item data
    $product = new stdClass();
    $product->nid = _classifieds_sales_get_uc_product();
    $product->default_qty = 1;

    // Build the preliminary add to cart data array.
    $data = array(
      'nid' => $product->nid,
      'classifieds_nid' => intval($node->nid),
      'classifieds_price' => $price,
      'classifieds_ad_summary' => $classified_data['ad_summary'],
      'classifieds_title' => $node->title,
      'classifieds_path' => $node->path,
      'classifieds_desc' => array(
        0 => '<em>Appearing in ' . $classified_data['parent_name'] . ' >> ' . $classified_data['child_name'] . '</em>',
        1 => 'Online Ad: Running for ' . $classified_data['attributes']['duration']['days'] . ' days',
      ),
    );

    //add print ad running date THIS IS BROKEN... FIX IT
    if (isset($node->run_ad)) {
      //add publish include
      module_load_include('inc', 'classifieds', 'publish');

      //set counter
      $i = 0;

      foreach ($node->run_ad as $date) {
        //add dates to ubercart item
        $date_string = _classifieds_publish_create_run_string($date);

        $data['classifieds_desc'][2 + $i] = 'Print Ad: Running from ' . $date_string['start'] . ' to ' . $date_string['end'];

        //increment counter
        $i++;
      }
    }


    //perform status updates depnding on price
    if ($price > 0) { //paid ad
      _classifieds_set_status($node->nid, 0);
    }
    else { //this is a free ad
      _classifieds_set_status($node->nid, 1);

      //insert this free ad into the sales history table
      _classifieds_record_sale($node->nid, 0, $classified_data['ad_summary'], $price);

      //delete any edits associated with it
      _classifieds_sales_records_clean_up($node->nid);

      //remove this nid from ubercart- just incase it was originally a paid ad
      //that had feature removed, to make it free
      _classifieds_remove_from_uc_cart($node->nid);

      //show status message and redirect
      if ($case == 'insert') {
        classifieds_ad_creation_message($node);
      }
      else {
        classifieds_ad_creation_message($node, 'edit');
      }
    }

    //only add items to cart, if we have a fee
    if ($price > 0) {

      //add items to cart based on whether this is insert(new) or update(edit)
      if ($case == 'insert') { //new ad
        uc_cart_add_item($product->nid, $product->default_qty, $data, NULL, FALSE, FALSE);

        //record this sale temporarily as an edit-in case someone wants to make changes...
        _classifieds_record_sale($node->nid, 0, $classified_data['ad_summary'], $price, 1);

        //show status message and redirect
        classifieds_ad_creation_message($node, 'new', TRUE);
      }
      else { //updating a currently existing ad
        //make sure there is only one item for each node by deleting any old
        //cart items
        _classifieds_remove_from_uc_cart($node->nid);


        //Determine if this edit is for a new ad, or for one already paid for (or created, if free)
        if (_classifieds_has_completed_sale_record($node->nid)) { //original ad was payed for(or free) at some time and completed
          //overwrite product to our edit classified ad type
          $product->nid = _classifieds_sales_get_uc_product_edit();

          //add to cart
          uc_cart_add_item($product->nid, $product->default_qty, $data, NULL, FALSE, FALSE);

          //get ordered array of sales records
          $sale_records = _classifieds_get_sale_records($node->nid);

          //record this sale temporarily as an edit
          _classifieds_record_sale($node->nid, 0, $classified_data['ad_summary'], $price, 1);

          //this is for the redirector
          $already_existed = TRUE;
        }
        else { //this is a new ad
          //add my item again
          uc_cart_add_item($product->nid, $product->default_qty, $data, NULL, FALSE, FALSE);

          //record this sale temporarily as an edit-in case someone wants to make changes...
          _classifieds_record_sale($node->nid, 0, $classified_data['ad_summary'], $price, 1);
        }

        //this is an update, so lets check to see if this needs to be censored again
        if (_classifieds_get_censor($node->nid) == 1) {
          _classifieds_set_censor($node->nid, 2);
        }

        //show status message and redirect
        if ($already_existed == TRUE) {
          classifieds_ad_creation_message($node, 'edit', TRUE);
        }
        else {
          classifieds_ad_creation_message($node, 'new', TRUE);
        }
      }
    }
  }

  ///////////////HELPER FUNCTIONS//////////////////////////
  /**
   * function to automaticaly hide all other fields. taken from:
   * http://stellapower.net/content/creating-multi-step-node-forms
   */
  private function clearFields(&$form, $fields) {

    // Hide all the elements we don't want.
    foreach (element_children($form) as $child) {
      if ($child != 'buttons' && !in_array($child, $fields) &&
          (empty($form[$child]['#type']) ||
          ($form[$child]['#type'] != 'hidden'
          && $form[$child]['#type'] != 'value'
          && $form[$child]['#type'] != 'token'))) {
        //$form[$child]['#access'] = FALSE;
        unset($form[$child]);
      }
    }
  }

}
