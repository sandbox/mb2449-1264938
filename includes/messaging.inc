<?php

/**
 * @file
 * TODO: put this file in sales stuff... since it deals with ubercart sales
 * related messages...
 */

/**
 * This function is responsible for displaying user status messages
 * after an ad is created, and redirecting the user to the appropriate
 * page
 */
function classifieds_ad_creation_message($node, $status='new', $paid=FALSE) {

  //Split messages depending on status
  if ($status == 'new') { //This is a NEW AD
    //This is a free ad
    if ($paid == FALSE) {

      drupal_set_message('Thank you for choosing ' . variable_get('site_name', NULL) . '. Your ad has been published and will be approved shortly.');
      drupal_goto($node->path);
    }
    else { //This is a paid for ad
      drupal_set_message('Thank you for choosing ' . variable_get('site_name', NULL) . '. Feel free to place another ad or checkout now in order to publish your ad(s).');
      drupal_goto('cart');
    }
  }
  else { //This is an EDITED AD
    //This is a free ad
    if ($paid == FALSE) {

      drupal_set_message('Thank you for choosing ' . variable_get('site_name', NULL) . '. Your ad has been modified.');
      drupal_goto($node->path);
    }
    else { //This is a paid for ad
      drupal_set_message('Thank you for choosing ' . variable_get('site_name', NULL) . '. Your ad has been modified. Please
        be sure to complete the checkout process or your ad will be unpublished.  If you did not wish to purchase any
        extras, please edit your ad again, removing any features you added.');
      drupal_goto('cart');
    }
  }
}

/**
 * This function gets called after a successful UC order
 */
function classifieds_ad_creation_message_after_order($count) {

  if ($count > 1) {
    drupal_set_message('Thank you for choosing ' . variable_get('site_name', NULL) . '. Your ads have been published and will be approved shortly.');
  }
  else {
    drupal_set_message('Thank you for choosing ' . variable_get('site_name', NULL) . '. Your ad has been published and will be approved shortly.');
  }
}
