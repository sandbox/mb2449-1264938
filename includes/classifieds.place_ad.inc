<?php

/**
 * @file
 * This file is responsible for running the 'Place Ad' block
 */

/**
 * This is the place ad form that gets called from the place ad block
 * @param $form_state
 */
function classifieds_place_ad_form($form_state) {

  //build a list of parent terms
  $sql = "SELECT * FROM {classifieds_node_per_cat} JOIN {term_data} ON classifieds_node_per_cat.tid = term_data.tid ORDER BY weight";
  $options['select'] = '--SELECT CATEGORY--';
  $result = db_query($sql);
  while ($data = db_fetch_object($result)) {
    $options[$data->tid] = $data->name;
  }

  //figure out what get's ghosted (2nd select,submit)
  $second_cat_ghosted = TRUE; //defaults
  $submit_ghosted = TRUE; //defaults
  $one_cat = FALSE;

  //The parent category is set to 'select cat'
  if ($form_state['storage']['content']['simple_select_parent'] == 'select') {
    $submit_ghosted = TRUE;
  }
  elseif ($form_state['storage']['content']['simple_select_parent'] != '') { //meaning, this thing is a number.. not a fresh page or blank
    //get tree of children
    $tree = taxonomy_get_tree(_classifieds_get_vid(), $form_state['storage']['content']['simple_select_parent'], -1, 1);

    //count the children
    if (count($tree) == 0) {
      $submit_ghosted = FALSE;
      $second_cat_ghosted = TRUE;
      $one_cat = TRUE;
    }
    else {
      //show the second category...
      $second_cat_ghosted = FALSE;
    }
  }

  //figure out what to do with child data
  if ($form_state['storage']['content']['simple_select_child'] == 'select') {
    //drupal_set_message('hide the sumbit button');
    if ($one_cat == FALSE) {
      $submit_ghosted = TRUE;
    }
  }
  elseif ($form_state['storage']['content']['simple_select_child'] != '') {
    //drupal_set_message('show the sumbit button');
    $submit_ghosted = FALSE;
  }

  //Start place ad form, collect up to 2 categories, then submit
  $form['content'] = array(
    '#tree' => TRUE,
    '#prefix' => '<div id="simple-select-wrapper">',
    '#suffix' => '</div>',
  );

  $form['content']['simple_select_parent'] = array(
    '#title' => 'Category',
    '#type' => 'select',
    '#default_value' => $form_state['storage']['content']['simple_select_parent'],
    '#options' => $options,
    '#ahah' => array(
      'path' => 'classifieds/js/place',
      'wrapper' => 'simple-select-wrapper',
    ),
  );

  //fill in the form stuff now... or else the form wont load its ahah
  $form['content']['simple_select_child'] = array(
    '#type' => 'select',
    '#options' => classifieds_place_ad_build_subcats($form_state),
    //'#title' => 'Sub-Category',
    '#ahah' => array(
      'path' => 'classifieds/js/place',
      'wrapper' => 'simple-select-wrapper',
    ),
    '#disabled' => TRUE,
    '#default_value' => $form_state['storage']['content']['simple_select_child'],
  );

  //holder to hide rates
  $form['content']['rates'] = array(
    '#type' => 'fieldset',
    '#title' => t('Rates'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  //input the rates...
  $form['content']['rates']['info'] = array(
    '#type' => 'item',
    '#description' => variable_get('classifieds_rates', 'The rates go here.'),
  );

  //build submit button
  $form['content']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create Ad'),
    '#disabled' => TRUE,
  );


  //ghost the proper fields
  if ($second_cat_ghosted == TRUE) {
    $form['content']['simple_select_child']['#disabled'] = TRUE;
  }
  else {
    $form['content']['simple_select_child']['#disabled'] = FALSE;
  }
  if ($submit_ghosted == TRUE) {
    $form['content']['submit']['#disabled'] = TRUE;
  }
  else { //submit has been unghosted...
    $form['content']['submit']['#disabled'] = FALSE;
    //
  }


  //check to see if this default value is in the sub category options... if not... ghost submit
  $def_val_is_in_cat = FALSE;

  foreach ($form['content']['simple_select_child']['#options'] as $key => $label) {
    if ($key == $form['content']['simple_select_child']['#default_value']) {
      $def_val_is_in_cat = TRUE;
      break;
    }
  }

  //sometimes when a sub cat is selected, then a parent category is changed
  //the submit button will not ghost... the above checks for that
  if ($def_val_is_in_cat == FALSE && $one_cat == FALSE) {
    $form['content']['submit']['#disabled'] = TRUE;
  }

  //return form
  return $form;
}

/**
 * Return a FAPI select element whose options are based on values submitted by a
 * separate select field.
 *
 * @param array $form_state
 *     The submitted values of a form. We're mostly interested in $form_state['storage']
 *     which is where all the pertinent information will be.
 * @return array $element
 */
function classifieds_place_ad_build_subcats($form_state) {

  $sql = "SELECT * FROM {term_hierarchy} JOIN {term_data} ON term_hierarchy.tid = term_data.tid WHERE
term_hierarchy.parent = %d ORDER BY term_data.weight";

  //set default
  $opt['select'] = '- SELECT SUB-CAT -';

  if ($form_state['storage']['content']['simple_select_parent']) {
    $result = db_query($sql, $form_state['storage']['content']['simple_select_parent']);
    while (($data = db_fetch_object($result)) !== FALSE) {
      $opt[$data->tid] = $data->name;
    }
  }

  return $opt;
}
