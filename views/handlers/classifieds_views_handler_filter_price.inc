<?php

/**
 * @file
 * The subclass simply adds properties,
 * for field-specific subclasses to use if they need to.
 */
class classifieds_views_handler_filter_price extends views_handler_filter {
  /* this method is used to create the options form for the Views UI when creating a view
   * we use the standard drupal form api to return a form array, with the settings
   * we want to capture.
   */

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Step 1: fetch all our vocabularies, and build an array of options
    //$terms = taxonomy_get_vocabularies();
    //$show = array();
    //foreach($terms as $term) {
    //  $show[$term->vid] = $term->name;
    //}
    // Step 2: create a select field to choose Vocabulary options
    //   this allows you to choose which vocabulary to fetch terms for in the exposed filter
    //$form['filter_vocab'] = array(
    //  '#type' => 'select',
    //  '#title'  => t('Vocabulary'),
    //  '#options'  => $show,
    //  '#default_value'  => $this->options['filter_vocab']
    //);
    // Step 3: Create a checkbox field to select whether date options should be included
    //$form['include_dates'] = array(
    //  '#type' => 'checkbox',
    //  '#title'  => t('Include Date Filters'),
    //  '#default_value'  => $this->options['include_dates']
    //);
  }

  /* I'll be perfectly honest, I have no idea if this is required or not. I *think* it may be
   * as it defines our filter field. However I don't use it. I added it when trying to get
   * things working...
   */

  function value_form(&$form, &$form_state) {
    $form['custom_filter'] = array(
      '#type' => 'textfield'
    );
  }

  /* A custom display for our exposed form. Views normally uses the value_form for this
   * however we're skipping that entirely since we want our exposed form to be a completely
   * different beast. It's entirely possible I could move this to value_form however...
   */

  function exposed_form(&$form, &$form_state) {

    //minimum price text field
    $form['price'] = array(
      '#title' => t('Price'),
      '#type' => 'textfield',
      '#size' => 3,
      '#maxlength' => 8,
      '#element_validate' => array('classifieds_price_validate'),
    );

    $form['price2'] = array(
      '#type' => 'textfield',
      '#size' => 3,
      '#maxlength' => 8,
      '#element_validate' => array('classifieds_price_validate'),
    );
  }

  // the query method is responsible for actually running our exposed filter
  function query() {
    // make sure our base table is included in the query.
    // base table for this is node so it may be redundent...
    $this->ensure_my_table();

    // make sure term node is joined in if needed
    // not exactly optimal since we may not need it if we're filtering by date
    //$this->query->add_table('term_node');
    // get the values of the submitted filter
    $min = check_plain($_GET['price']);
    $max = check_plain($_GET['price2']);

    //find the min
    if ($min) {
      $this->query->add_where($this->options['group'], "classifieds_online_ads.price >= %d", $min);
    }

    //find the min
    if ($max) {
      $this->query->add_where($this->options['group'], "classifieds_online_ads.price <= %d", $max);
    }
  }

}

function classifieds_price_validate($element, &$form_state) {
  if (!empty($element['#value']) && !is_numeric($element['#value'])) {
    form_error($element, t('Price must be numeric.'));
  }

  if ($element['#value'] < 0) {
    form_error($element, t('Price must be a positive integer.'));
  }
}