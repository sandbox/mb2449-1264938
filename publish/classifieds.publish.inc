<?php

/**
 * @file
 * Responsible for all things having to do with publishing a classified ad
 */

/**
 * Responsible for publish functions
 */
class publish {

  /**
   * VARS
   */
  protected $classifiedID; //lets us retreive/save classifiedData
  protected $classifiedData; //read only copy of the classifiedData...
  protected $defaultValues;
  protected $isFirstEdit;
  protected $isFirstLoad;

  /**
   * CONSTRUCTOR
   * @param type $form
   * @param type $form_state
   */
  public function __construct($classifiedID=NULL) {
    //stash the ID
    $this->classifiedID = $classifiedID;

    //recall the classified data
    $this->classifiedData = classifieds_get_data($classifiedID);
    
    //NOTE: if the classifiedID is null, then this is probably the admin form loading this for the scheduling function
  }

  public function setFormData($defaultValues, $isFirstEdit, $isFirstLoad) {
    $this->defaultValues = $defaultValues;
    $this->isFirstEdit = $isFirstEdit;
    $this->isFirstLoad = $isFirstLoad;
  }

  /**
   * builds step 3 on the form
   */
  public function step3(&$form, $form_state) {

    /**
     * ADD JS
     */
    //set url hidden field
    $form['preview-url'] = array('#type' => 'hidden', '#value' => url('classifieds/js/step3/preview'));

    //add js for print preview
    drupal_add_js(drupal_get_path('module', 'classifieds') . '/publish/classifieds.publish.js');


    /**
     * DEFINE VARIABLES NEEDED IN FORM - SCHEDULING STUFF...
     */
    //create period run dates -This is needed up here, so on new ads, we have the date for default run_ad
    $schedule = $this->buildScheduleDropDown(2);

    //reverse the array order
    $schedule['dates'] = array_reverse($schedule['dates'], TRUE);


    /**
     * FIRST VIEW // FIRST EDIT
     */
    //first run of step 3
    if (!isset($this->classifiedData['publish'])) {

      //add publication data to classified data array
      $this->classifiedData['publish']['chars_per_line'] = variable_get('classifieds_publish_chars_per_line', NULL);
      $this->classifiedData['publish']['name'] = variable_get('classifieds_publish_name', NULL);

      //save data
      classifieds_save_data($form['classified_id']['#value'], $this->classifiedData);

      //set some ad summary data if this is NOT an edit form
      if (!isset($form['nid']['#value'])) {

        //forge values
        $form_state['storage']['step'] = 3;

        $post['run_ad'] = array($schedule['key'] => $schedule['key']);

        $form_state['storage']['values'][1]['seller'] = 0;

        //save data
        classifieds_save_data($this->classifiedID, $this->classifiedData);

        //run update seller type, which is the option loaded on the front page
        classifieds_ad_summary_update_periodical($this->classifiedID, $form_state, $post);
      }
    }


    /**
     * FIRST EDIT LOAD ON THIRD STEP - for default data?
     */
    if (isset($form['nid']['#value']) && !isset($form_state['storage']['values'][3])) {

      //load values from db
      //$form['print_ad']['#default_value'] = $form['#node']->print_ad;

      if ($form['#node']->keycode != '') {
        $form['keyword']['#default_value'] = array(1);
      }

      //save previous runs to classified_data
      if (count($prev_defaults) > 0) {
        $classified_data = classifieds_get_data($form['classified_id']['#value']);
        $classified_data['publish']['prev_runs'] = $prev_defaults; //this is so previous runs can be resaved when node is edited
        classifieds_save_data($form['classified_id']['#value'], $classified_data);
      }

      //set default values for run_ad
      $form['run_ad']['#default_value'] = $form['#node']->run_ad;
    }
    else { //default values are the normal settings
      $form['run_ad']['#default_value'] = isset($default_value['run_ad']) ? $default_value['run_ad'] : array($schedule['key'] => $schedule['key']);
      $form['keyword']['#default_value'] = isset($default_value['keyword']) ? $default_value['keyword'] : array(0);
    }


    /**
     * FORM ITEMS
     */
    //show publication letter head...
    $form['description'] = array(
      '#title' => $this->classifiedData['publish']['name'],
      '#type' => 'item',
      '#description' => variable_get('classifieds_publish_desc', ''),
    );

    //show original ad...
    $form['online_ad'] = array(
      '#title' => 'Online Ad',
      '#type' => 'item',
      '#description' => $form_state['storage']['values'][1]['body'],
    );

    //add the keyword field if it is not hidden.
    if ($sale_rules['keyword']['hide'] == 0) {

      $form['keyword'] = array(
        '#type' => 'checkboxes',
        //'#title' => 'Ad Keyword',
        '#options' => array(1 => '<strong>Add Keycode:</strong> Include a 3 digit keycode that will link your print ad to your online ad for only $' . $sale_rules['keyword']['price']),
        '#ahah' => array(
          'event' => 'change',
          'path' => 'classifieds/js/step3/keyword',
          'wrapper' => 'classifieds-price-wrapper2',
        )
      );
    }

    //show print ad text box
    $form['print_ad'] = array(
      '#title' => t('Print Ad'),
      '#type' => 'textarea',
      '#cols' => $this->classifiedData['publish']['chars_per_line'],
      '#rows' => 6,
      '#resizable' => FALSE,
      '#default_value' => isset($default_value['print_ad']) ? $default_value['print_ad'] : $this->generatePrintAd($form, $form_state),
    );

    //build ad rate display...
    $form['ad_rate'] = array(
      '#title' => 'Ad Rates',
      '#type' => 'item',
      '#description' => $this->generateAdRate($form, $form_state),
    );

    //this server as a title if nothing else
    $form['run_ad_disabled'] = array(
      '#title' => t('Run Ad'),
      '#type' => 'checkboxes',
      '#disabled' => TRUE,
    );

    //build run ad- this modifies run_ad_disabled sometimes too
    $runAd = $this->generateAdRun($form, $form_state, $schedule);

    $form['run_ad'] = array(
      //'#title' => t('Run Ad'),
      '#type' => 'checkboxes',
      '#description' => $runAd['string'],
      '#options' => $runAd['options'],
      '#ahah' => array(
        'event' => 'change',
        'path' => 'classifieds/js/step3/days',
        'wrapper' => 'classifieds-preview-days-wrapper',
      ),
      '#prefix' => '<div id ="classifieds-run-ad">',
      '#suffix' => '</div>'
    );

    //show days preview-
    $form['preview_days'] = array(
      '#type' => 'item',
      '#prefix' => '<div id="classifieds-preview-days-wrapper">',
      '#suffix' => '</div>',
      '#description' => classifieds_publish_generate_preview_days($this->generatePreviewDays()),
    );

    //show preview
    $form['preview'] = array(
      '#title' => 'Print Ad Preview',
      '#type' => 'item',
      '#description' => '<div id="classifieds-preview-wrapper">' . classifieds_publish_generate_preview($form, $form_state) . '</div>', //we will have to load this automatically....
    );


    /**
     * SAVE CLASSIFIED DATA
     */
    classifieds_save_data($this->classifiedID, $this->classifiedData);
  }

  /**
   * Determines if the ad in step 2 can be published
   * @return type 
   */
  public function pickupAd() {
    //get exclude array
    $exclude = variable_get('classifieds_publish_exclude', NULL);

    //return it based if we find our tids in the array or not
    if (array_key_exists($this->classifiedData['child_tid'], $exclude)
        || array_key_exists($this->classifiedData['parent_tid'], $exclude)) {
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  /**
   * helper function that builds periods
   *
   * no_of_runs is how many run dates you wants
   * future is how many runs you want to show beyond the current period
   */
  public function buildScheduleDropDown($no_of_runs, $regress=0) {

    //define needed variables
    $schedule = variable_get('classifieds_publish_schedule', NULL);
    $delivery_day = variable_get('classifieds_publish_schedule_delivery_day', NULL);
    $due_day = variable_get('classifieds_publish_schedule_due_day', NULL) - 1;
    $due_time = $this->buildDueTime();

    //period is number of days
    $period = ($schedule - 1) * 7;

    //only perform this functionality for weekly/biweekly scheduled publications
    if ($schedule == 2 || $schedule == 3) {

      //create the base date that will be used to calculate periods
      //starting with the first sunday of 11, adjusted for the deliver day
      $base_date = strtotime(date('Y-m-d', mktime(0, 0, 0, 1, 2 + $delivery_day, 2011)));

      //now find todays date for comparison
      $todays_date = strtotime(date("Y-m-d H:i:s"));

      //subtract the dates, and divide by number of seconds in a day...
      $difference = ($todays_date - $base_date) / 86400;

      //calculate the period
      $current_period = floor($difference / $period);

      //calculate the current period start date
      $current_period_start = strtotime(date('Y-m-d', mktime(0, 0, 0, 1, 2 + $delivery_day + ($current_period * $period), 2011)));

      //figure out what day it is in the period
      $day_of_period = floor(($todays_date - $current_period_start) / 86400);

      //make adjustment if needed
      if ($day_of_period > $due_day) { //past the due date
        $adjust = 1;
      }
      elseif ($day_of_period == $due_day) { //day of due date... check time
        $current_time = strtotime(date("Y-m-d H:i:s"));
        $due_time = strtotime(date("Y-m-d ") . $due_time);

        $check_time = ($due_time - $current_time) / 3600; //how many hours away from the deadline we are

        if ($check_time > 0) { //we still have time before the deadline...
          $adjust = 0;
        }
        else { // time has passed...
          $adjust = 1;
        }
      }
      else { //hasnt hit due date, we are still good
        $adjust = 0;
      }

      //create array to hold publication period start dates
      $dates = array();

      //see if we have to start the dates farther back...
      if ($regress > 0) {
        //alter the current period start
        $current_period_start = strtotime(date('Y-m-d', mktime(0, 0, 0, 1, 2 + $delivery_day + (($current_period - $regress) * $period), 2011)));
      }

      //loop through requested number of entries and build dates
      for ($i = $no_of_runs; $i > 0; $i--) {

        $adjust_1 = $period * ($i + $adjust);
        $adjust_2 = ($period * ($i + 1 + $adjust));

        //create date strings
        $key = date('m-d-Y', mktime(0, 0, 0, date("m", $current_period_start), date("d", $current_period_start) + $adjust_1, date("Y", $current_period_start)));
        $start = date('D, M j', mktime(0, 0, 0, date("m", $current_period_start), date("d", $current_period_start) + $adjust_1, date("Y", $current_period_start)));
        $end = date('D, M j', mktime(0, 0, 0, date("m", $current_period_start), date("d", $current_period_start) + $adjust_2, date("Y", $current_period_start)));

        $dates[$key] = $start . ' -> ' . $end;
      }

      //calculate some extra fields to make life easier down the road
      $current_key = date('m-d-Y', mktime(0, 0, 0, 1, 2 + $delivery_day + (($current_period + $adjust + 1) * $period), 2011));
      $current_due_day = date('l, M jS', mktime(0, 0, 0, 1, 2 + $delivery_day + (($current_period + $adjust) * $period) + $due_day, 2011));
      $time = strtotime($this->buildDueTime());
      $current_due_time = date('g:i a', $time);
    }

    //TODO:A DAILY VERSION OF SCHEDULING...
    //return the dates
    return array('dates' => $dates, 'key' => $current_key, 'due_day' => $current_due_day, 'due_time' => $current_due_time);
  }

  /**
   * helper funciton that builds due time
   */
  function buildDueTime() {

    //get variables
    $hour = variable_get('classifieds_publish_schedule_hour', NULL) + 1;
    $min = variable_get('classifieds_publish_schedule_minutes', NULL);
    $mil = variable_get('classifieds_publish_schedule_mil', NULL);

    //build hour
    if ($mil == 2) {
      $hour = $hour + 12;
    }

    //build minutes
    $min = ($min - 1) * 15;
    if ($min == 0) {
      $min = '00';
    }

    //return
    return $hour . ':' . $min . ':00';
  }

  /////////////HELPER FUNCTIONS//////////////////////

  /**
   * Builds the default print ad automatically for the user on the first load
   */
  private function generatePrintAd(&$form, $form_state) {

    //generate dummy print ad if one has not been created yet...
    if ($form_state['storage']['values'][1]['field_phone'][0]['value']) { //check to see if they used their phone... if not use their email...
      $contact = $form_state['storage']['values'][1]['field_phone'][0]['value'];
    }
    else {
      $contact = $form_state['storage']['values'][1]['field_email'][0]['email'];
    }

    $price = $form_state['storage']['values'][1]['price'];

    //define end_string to append to dummy ad
    $end_string = '...' . ' $' . $price . ' ' . $contact;

    //number of allowed characters grand total
    $chars_allowed = ($this->classifiedData['sale_rules']['lines']['free_lines'] - 1) * $this->classifiedData['publish']['chars_per_line'];

    //number of characters to reserve for the end string
    $chars_reserved_for_end = strlen($end_string);

    //create the dummy ad for the first load...
    $dummy_print_ad = substr($form['online_ad']['#description'], 0, ($chars_allowed - $chars_reserved_for_end));

    $dummy_print_ad .= $end_string;

    //NOTE: IN ORDER TO DO THIS ACCURATELY, YOU WOULD HAVE TO NOW HOW MANY LINES EACH OF THE WORDS TAKE UP
    //USING WORDWRAP(). FUTURE UPGRADE... RIGHT NOW ITS SORT OF APPROXIMATE, AND MIGHT NOT WORK RIGHT
    //PREVIEW ADS WITH A LARGE AMOUNT OF FREE LINES.
    //return
    return $dummy_print_ad;
  }

  /**
   * Builds ad rate form info
   */
  private function generateAdRate(&$form, $form_state) {

    //use this to figure out if its a private or commerical seller
    if ($form_state['storage']['values'][1]['seller'] == 0) { //private party
      $seller = 'private';
      $seller_label = 'Private Party';
    }
    else { //commercial
      //update labels...
      $seller = 'commercial';
      $seller_label = $attributes['seller']['title'];
    }

    //ad category price is base price + seller price (if appicable)
    $base_price = $this->classifiedData['sale_rules']['base']['price'] + $sale_rules['seller'][$seller]; //or commericial... auto select based on input...
    //build text description for users...
    $ad_rate = 'A ' . $seller_label . ' ad in this category starts at $' . $base_price . ' for ' . $this->classifiedData['sale_rules']['lines']['free_lines'] . ' lines.';
    $ad_rate .= ' Each additional line is $' . $this->classifiedData['sale_rules']['lines']['price'] . '.';

    if ($sale_rules['lines']['max_lines'] == 0) {
      $ad_rate .= ' You may use as many lines as you need.<br />';
    }
    else {
      $ad_rate .= ' You are allowed a maximum of ' . $this->classifiedData['sale_rules']['lines']['max_lines'] . ' lines.';
    }

    //return
    return $ad_rate;
  }

  /**
   * Generate text for run ad form elemen
   */
  private function generateAdRun(&$form, $form_state, $schedule) {

    //build drop down options for current runs
    $options = array();
    $i = 0;
    foreach ($schedule['dates'] as $key => $text) {
      if ($i == 0) {
        $options[$key] = 'Next issue (' . $text . ')';
        $i++;
      }
      else {
        $options[$key] = 'Issue after next (' . $text . ')';
      }
    }

    //add in previously ran ads...
    if (isset($form['#node']->run_ad)) {

      //save old run ads
      $prev_options = array();
      $prev_defaults = array();

      //go thru run ad options
      foreach ($form['#node']->run_ad as $key => $run_ad) {

        if (!array_key_exists($run_ad, $options)) {
          $date = _classifieds_publish_create_run_string($run_ad);
          $prev_options[$run_ad] = 'Past issue (' . $date['start'] . ' -> ' . $date['end'] . ')';
          $prev_defaults[] = $run_ad;
        }
      }
    }


    if (count($prev_options) > 0) {
      $form['run_ad_disabled']['#options'] = $prev_options;
      $form['run_ad_disabled']['#default_value'] = $prev_defaults;
    }

    //build due by string
    $string = '<em>All ads for the next issue are due on ' . $schedule['due_day'] . ' by ' . $schedule['due_time'] . '.</em>';

    //return
    return (
        array(
          'options' => $options,
          'string' => $string )
        );
  }

  /**
   * Builds preview days
   */
  private function generatePreviewDays() {

    //general dummy values to give to generate_preview_days
    $fakepost = array(
      'run_ad' => $form['run_ad']['#default_value'],
    );

    //return
    return $fakepost;
  }

}

/**
 * This will save the publishing related (Step 3) data from
 * the create ad form.
 * $case is either 'insert' or 'update'
 */
function classifieds_publish_create_ad_save($node, $case='insert') {

  //retrieve classified ad data
  $classified_data = classifieds_get_data($node->classified_id);

  //vars to be saved in the database...
  $nid = $node->nid;
  $run_dates = array();
  foreach ($node->run_ad as $key => $value) {
    if ($key == $value) {
      $run_dates[] = $key;
    }
  }

  //keyword has been set...
  if ($node->keyword[1] == 1) {
    $keyword = classifieds_publish_generate_keyword();
  }
  else {
    $keyword = '';
  }

  //generate byline....
  $byline = $classified_data['publish']['byline'];

  //build xml print ad
  $text = $node->print_ad;

  //save info
  switch ($case) {
    case 'insert':
      db_query('INSERT INTO {classifieds_print_ads} (nid,byline,text,keyword) VALUES(%d,"%s","%s","%s")', $nid, $byline, $text, $keyword);

      //save run dates
      foreach ($run_dates as $date) {
        db_query('INSERT INTO {classifieds_print_schedule} (nid,run_date) VALUES(%d,"%s")', $nid, $date);
      }

      break; //end insert

    case 'update':

      //look up the old keyword
      $old_keyword = db_result(db_query('SELECT keyword FROM {classifieds_print_ads} WHERE nid=%d', $node->nid));

      //see if we need to add a keyword
      if ($node->keyword[1] == 1 && !empty($old_keyword)) {
        //overwrite the generated keyword with our old one
        $keyword = $old_keyword;
      }

      //perform the update
      db_query('UPDATE {classifieds_print_ads} SET byline="%s", text="%s", keyword="%s" WHERE nid=%d', $byline, $text, $keyword, $nid);

      //delete old run dates....
      db_query('DELETE FROM {classifieds_print_schedule} WHERE nid=%d', $nid);

      //save all new print schedule values
      foreach ($run_dates as $date) {
        db_query('INSERT INTO {classifieds_print_schedule} (nid,run_date) VALUES(%d,"%s")', $nid, $date);
      }

      //save all old printed schedules as well...
      if (isset($classified_data['publish']['prev_runs'])) {
        foreach ($classified_data['publish']['prev_runs'] as $date) {
          db_query('INSERT INTO {classifieds_print_schedule} (nid,run_date) VALUES(%d,"%s")', $nid, $date);
        }
      }

      break; //end update
  }
}

/**
 * Generate keyword
 */
function classifieds_publish_generate_keyword() {

  $length = 3; //A 3 digit keyword

  $keyword = _classifieds_publish_generate_random_string($length);

  $is_valid = FALSE;

  while ($is_valid == FALSE) {

    //check if this keyword is good or not
    $sql = "SELECT keyword FROM {classifieds_print_ads} AS a WHERE a.keyword LIKE '%s'";
    $data = db_fetch_object(db_query($sql, $keyword));

    if (!$data) { //not found in the database, keyword is ok
      $is_valid = TRUE;
    }
    else {
      $keyword = _classifieds_publish_generate_random_string($length);
    }
  }

  return $keyword;
}

/**
 * generate random string
 */
function _classifieds_publish_generate_random_string($length) {

  $length = 3;
  $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $string = '';

  for ($p = 0; $p < $length; $p++) {
    $string .= $characters[mt_rand(0, strlen($characters))];
  }

  return $string;
}

/**
 * publish_generate_preview- This function constructs the print ad previewd
 * in step 3.
 */
function classifieds_publish_generate_preview($form, $form_state=NULL) {

  //pull out variables
  if (!is_array($form['print_ad'])) { //we were given a raw $_POST array from js
    $classified_data = classifieds_get_data($form['classified_id']);

    $print_ad = $form['print_ad'];
    $byline = $classified_data['publish']['byline'];
  }
  else { //this is a normal drupal form, page load
    $classified_data = classifieds_get_data($form['classified_id']['#value']);

    $print_ad = $form['print_ad']['#default_value'];

    //sort through form and pull out fields
    $form_array = $form['#parameters'][1]['storage']['values'][1];
    $fields = array(); //we will store our fields in here
    foreach ($form_array as $name => $data) {
      if (substr($name, 0, 6) == 'field_') {
        if (isset($data[0]['value'])) {
          $fields[$name] = $data[0]['value'];
        }
      }
    }

    //add in location to the fields
    $fields['location'] = $form_state['storage']['values'][1]['city'];

    //set the byline then save it...
    $byline = classifieds_publish_generate_byline($classified_data['parent_tid'], $classified_data['child_tid'], $fields);

    //save the byline in the session var
    $classified_data['publish']['byline'] = $byline;

    //save
    classifieds_save_data($form['classified_id']['#value'], $classified_data);
  }

  //set chars per line now that we have the classified data array
  $chars_per_line = $classified_data['publish']['chars_per_line'];


  //add html to the byline
  if ($byline != '') {
    $byline = '<strong>' . substr($byline, 0, $chars_per_line - 1) . '</strong><br />';
  }

  //wrap up print ad from user
  $new_print_ad = wordwrap($print_ad, $chars_per_line, "<br />\n", TRUE);

  //attache the byline
  $new_print_ad = $byline . $new_print_ad;

  //return byline (if set) and user entered print ad
  return $new_print_ad;
}

/**
 * 
 */
function _classifieds_publish_get_period() {
  //get run ad schedule
  $run_ad = variable_get('classifieds_publish_schedule', NULL);

  //get the number of days
  switch ($run_ad) {
    case 1:
      $days = 1;
      break;
    case 2;
      $days = 7;
      break;
    case 3;
      $days = 14;
      break;
  }

  //return number of days
  return $days;
}

/**
 * This function constructs the schedule preview in step 3
 */
function classifieds_publish_generate_preview_days($post) {

  //define variables
  $run_ad = $post['run_ad'];

  //see if the run ad is blank
  $count = 0;
  if (isset($run_ad)) {
    foreach ($run_ad as $data) {
      if ($data != 0) {
        $count++;
      }
    }
  }

  //figure out sunday...
  if ($count == 0) { //they choose not to run the print ad
    $display = '<div id="classifieds-calendar-none"><em>Are you sure?</em></div>';

    return $display;
  }
  else {

    //get period
    $days = _classifieds_publish_get_period();
    $delivery_day = variable_get('classifieds_publish_schedule_delivery_day', NULL);

    //
    $display = '';

    //build calendar for each 
    foreach ($run_ad as $key => $run_date) {

      //filter out unchecked values
      if ($key == $run_date) {

        //convert sunday to a unix timestamp
        $explode_start_day = explode('-', $run_date);
        $month = $explode_start_day[0];
        $day = $explode_start_day[1];
        $year = $explode_start_day[2];
        $start_day = strtotime($year . '-' . $month . '-' . $day . ' 00:00:00');

        //setup vars to help build the date array loop
        $date_array = array();
        $row_count = 0;
        $j = 1 + $delivery_day; //adjust days for the delivery day

        for ($k = 0; $k < $delivery_day; $k++) {
          $date_array[$row_count][] = ''; //this wont work if the delivery day is over 7....
        }

        //now we need to make a list of the dates... with div tags....
        for ($i = 0; $i < $days; $i++) {
          $calc_month = date('M', mktime(0, 0, 0, date("m", $start_day), date("d", $start_day) + $i, date("Y", $start_day)));
          $calc_day = date('j', mktime(0, 0, 0, date("m", $start_day), date("d", $start_day) + $i, date("Y", $start_day)));
          $calc_day_text = date('D', mktime(0, 0, 0, date("m", $start_day), date("d", $start_day) + $i, date("Y", $start_day)));

          //add to calendar
          $date_array[$row_count][] = '<div class="classifieds-calendar-month">' . $calc_month . '</div>
                                      <div class="classifieds-calendar-day">' . $calc_day . '</div>';
          if ($j == 7) { //reset i if we hit the end of the week...
            $j = 0;
            $row_count++;
          }
          $j++;
        }

        //fill in empty table cells (if any) at the end
        end($date_array);         // move the internal pointer to the end of the array
        $key = key($date_array);

        //set some variables
        $no_of_columns = count($date_array[$key]);
        $empties = 7 - $no_of_columns;

        //add in the extra table cells at the end...
        for ($z = 0; $z < $empties; $z++) {
          $date_array[$key][] = '';
        }

        //header
        $header = array('Sun', 'Mon', 'Tues', 'Wed', 'Thu', 'Fri', 'Sat');

        //turn this into a table...
        $display .= theme('table', $header, $date_array);
      }
    }

    return $display;
  }
}

/**
 * publish_generate_byline - This will take input and generate a byline
 */
function classifieds_publish_generate_byline($parent_tid, $child_tid, $field_values) {

  //find the bylines who have the same parent id
  $sql = "SELECT id,child_tids,fields,append FROM {classifieds_bylines} WHERE parent_tid=%d";
  $result = db_query($sql, $parent_tid);
  $byline_array = array();

  //save all the bylines that match the parent
  while ($tmp = db_fetch_array($result)) {
    $byline_array[] = $tmp;
  }

  //create a array to hold valid results from database
  $gen_byline = array();

  //go through each byline array in database
  foreach ($byline_array as $byline) {
    $child_tids = unserialize($byline['child_tids']);
    $append = unserialize($byline['append']);

    //go through each child tid in each record
    foreach ($child_tids as $child) {

      //check if child tid is the one we are looking for...
      if ($child == $child_tid) {
        //get fields
        $fields = unserialize($byline['fields']);

        //
        foreach ($fields as $field) {
          $gen_byline[] = array(
            'id' => $field['id'],
            'weight' => $field['weight'],
            'field' => $field_values[$field['id']],
            'append' => $append['append-' . $field['id']],
          );
        }
        //end
        break;
      }
    }
  }

  //sort the byline via weight
  usort($gen_byline, "_classifieds_publish_compare_weights");

  $byline_string = '';

  $i = 0; //add counts and only show byline if all the fields were filled in
  foreach ($gen_byline as $byline) {
    if ($byline['field'] != '') {
      $byline_string .= $byline['field'] . $byline['append'] . ' ';
      $i++;
    }
  }

  //dpm($fields);
  //check to see if some fields are missing
  if ($i != count($gen_byline)) {
    return '';
  }
  else { //all the fields are here
    return trim($byline_string);
  }
}

/**
 * used with usort() to sort through weights
 */
function _classifieds_publish_compare_weights($a, $b) {
  if ($a["weight"] == $b["weight"])
    return 0;
  if ($a["weight"] > $b["weight"])
    return 1;
  return -1;
}

/**
 * load the keycode... used by the ad template
 */
function classifieds_publish_load_keycode($nid) {

  //query
  $sql = "SELECT * FROM {classifieds_print_ads} WHERE nid=%d";

  //retrieve data
  $print_ad = db_fetch_array(db_query($sql, $nid));

  //return the keycode
  return $print_ad['keyword'];
}

/**
 * format the date generated by run ad stuff
 */
function _classifieds_publish_create_run_string($date, $format='D, M j') {

  //define variables
  $period = _classifieds_publish_get_period();
  $explode = explode('-', $date); //in format 09-12-2011
  //create dates
  $start = date($format, mktime(0, 0, 0, $explode[0], $explode[1], $explode[2]));
  $end = date($format, mktime(0, 0, 0, $explode[0], $explode[1] + $period, $explode[2]));

  return array(
    'start' => $start,
    'end' => $end,
  );
}