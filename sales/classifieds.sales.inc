<?php
/**
 * @file
 * Sales - Main file containing sales logic - hence the name!
 */

/**
 * Responsible for sales functions
 */
class sales {

  /**
   * VARS
   */
  private $childTID;
  private $parentTID;

  /**
   * CONSTRUCTOR
   */
  public function __construct($childTID, $parentTID) {

    $this->childTID = $childTID;
    $this->parentTID = $parentTID;
  }

  /**
   * getSaleRules
   */
  public function getSaleRules() {
    //TODO: apply caching here...
    //holder array
    $rules = array();

    //populate the array with the defaults
    $this->getRulesByDefault($rules);

    //now look for all sales rules that apply to the parent tid - this will overwrite defaults
    $this->getRulesByTID($rules, $this->parentTID);

    //now look for all sale rules that apply to the child tid - this will overwrite parents
    $this->getRulesByTID($rules, $this->childTID);

    //since we have all our final rules...
    //look up all the sales rules information and add it into the arrays
    foreach ($rules as $key => $value) {
      $table = 'classifieds_sales_' . $key;
      $find_rule = db_fetch_array(db_query("SELECT * FROM {" . $table . "} WHERE sale_id=%d", $value));
      $rules[$key] = $find_rule;
    }

    //return
    return $rules;
  }

  //TODO...
  //uc node checkout
  //going to get called on node validate (thats where we double check pricing)
  //going to get called on node save...
  //maybe some ad summary functionality?
  /////*****HELPER FUNCTIONS*******///////

  /**
   * These are functions for helping building an array of sales rules, given a tid and pid.
   */
  private function getRulesByDefault(&$rules) {
    $sql = "SELECT * FROM {classifieds_rules2cat} AS a JOIN {classifieds_sale_rules} AS b ON a.sid=b.sid WHERE a.tid = 0"; //AND b.apply_to=%d
    $result = db_query($sql);

    while (($data = db_fetch_object($result)) !== FALSE) {
      $rules[$data->sale_type] = $data->sale_id;
    }
  }

  /**
   * can be used for either main or sub tids
   */
  private function getRulesByTID(&$rules, $tid) {
    $sql = "SELECT * FROM {classifieds_rules2cat} AS a JOIN {classifieds_sale_rules} AS b ON a.sid=b.sid WHERE a.tid = %d";
    $result = db_query($sql, $tid);

    while (($data = db_fetch_object($result)) !== FALSE) {
      $rules[$data->sale_type] = $data->sale_id;
    }
  }

}

/**
 * saves the neccassary stuff after an ad is created...
 */
function classifieds_sales_create_ad_save($node, $case='insert') {

  //vars to be saved in the database...
  $nid = $node->nid;
  $seller = $node->seller;
  $price = $node->price;
  $duration = $node->duration;

  $address = $node->address;
  $city = $node->city;
  $state = $node->state;

  //save info
  switch ($case) {
    case 'insert':
      //vars to be saved for online ads (THIS SHOULD BE IN CORE... not sales)
      db_query('INSERT INTO {classifieds_online_ads} (nid,seller,price,duration,address,city,state) VALUES(%d,%d,%f,%d,"%s","%s","%s")', $nid, $seller, $price, $duration, $address, $city, $state);
      break;

    case 'update':

      db_query('UPDATE {classifieds_online_ads} SET seller=%d, price=%f, duration=%d, address="%s", city="%s", state="%s" WHERE nid= %d', $seller, $price, $duration, $address, $city, $state, $nid);
      break;
  }
}

/**
 * Hijack the cck 'add more items' button
 *
 * This is a modified copy of content_add_more_js()
 */
function classifieds_add_more_js($type_name_url, $field_name) {

  //I need this to load it
  module_load_include('inc', 'content', 'includes/content.node_form');

  //COPIED
  $type = content_types($type_name_url);
  $field = content_fields($field_name, $type['type']);

  if (($field['multiple'] != 1) || empty($_POST['form_build_id'])) {
    // Invalid request.
    drupal_json(array('data' => ''));
    exit;
  }

  // Retrieve the cached form.
  $form_state = array('submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  if (!$form) {
    // Invalid form_build_id.
    drupal_json(array('data' => ''));
    exit;
  }

  // We don't simply return a new empty widget to append to existing ones, because
  // - ahah.js won't simply let us add a new row to a table
  // - attaching the 'draggable' behavior won't be easy
  // So we resort to rebuilding the whole table of widgets including the existing ones,
  // which makes us jump through a few hoops.
  // The form that we get from the cache is unbuilt. We need to build it so that
  // _value callbacks can be executed and $form_state['values'] populated.
  // We only want to affect $form_state['values'], not the $form itself
  // (built forms aren't supposed to enter the cache) nor the rest of $form_data,
  // so we use copies of $form and $form_data.
  $form_copy = $form;
  $form_state_copy = $form_state;
  $form_copy['#post'] = array();
  form_builder($_POST['form_id'], $form_copy, $form_state_copy);
  // Just grab the data we need.
  $form_state['values'] = $form_state_copy['values'];
  // Reset cached ids, so that they don't affect the actual form we output.
  form_clean_id(NULL, TRUE);

  // Sort the $form_state['values'] we just built *and* the incoming $_POST data
  // according to d-n-d reordering.
  unset($form_state['values'][$field_name][$field['field_name'] . '_add_more']);
  foreach ($_POST[$field_name] as $delta => $item) {
    $form_state['values'][$field_name][$delta]['_weight'] = $item['_weight'];
  }
  $form_state['values'][$field_name] = _content_sort_items($field, $form_state['values'][$field_name]);
  $_POST[$field_name] = _content_sort_items($field, $_POST[$field_name]);

  // Build our new form element for the whole field, asking for one more element.
  $form_state['item_count'] = array($field_name => count($_POST[$field_name]) + 1);
  $form_element = content_field_form($form, $form_state, $field);
  // Let other modules alter it.
  // We pass an empty array as hook_form_alter's usual 'form_state' parameter,
  // instead of $form_state (for reasons we may never remember).
  // However, this argument is still expected to be passed by-reference
  // (and PHP5.3 will throw an error if it isn't.) This leads to:
  $data = &$form_element;
  $empty_form_state = array();
  $data['__drupal_alter_by_ref'] = array(&$empty_form_state);
  drupal_alter('form', $data, 'content_add_more_js');

  // Add the new element at the right place in the (original, unbuilt) form.
  if (module_exists('fieldgroup') && ($group_name = _fieldgroup_field_get_group($type['type'], $field_name))) {
    $form[$group_name][$field_name] = $form_element[$field_name];
  }
  else {
    $form[$field_name] = $form_element[$field_name];
  }

  // Save the new definition of the form.
  $form_state['values'] = array();
  form_set_cache($form_build_id, $form, $form_state);

  // Build the new form against the incoming $_POST values so that we can
  // render the new element.
  $delta = max(array_keys($_POST[$field_name])) + 1;
  $_POST[$field_name][$delta]['_weight'] = $delta;
  $form_state = array('submitted' => FALSE);
  $form += array(
    '#post' => $_POST,
    '#programmed' => FALSE,
  );
  $form = form_builder($_POST['form_id'], $form, $form_state);

  // Render the new output.
  $field_form = (!empty($group_name)) ? $form[$group_name][$field_name] : $form[$field_name];
  // We add a div around the new content to receive the ahah effect.
  $field_form[$delta]['#prefix'] = '<div class="ahah-new-content">' . (isset($field_form[$delta]['#prefix']) ? $field_form[$delta]['#prefix'] : '');
  $field_form[$delta]['#suffix'] = (isset($field_form[$delta]['#suffix']) ? $field_form[$delta]['#suffix'] : '') . '</div>';
  // Prevent duplicate wrapper.
  unset($field_form['#prefix'], $field_form['#suffix']);

  // If a newly inserted widget contains AHAH behaviors, they normally won't
  // work because AHAH doesn't know about those - it just attaches to the exact
  // form elements that were initially specified in the Drupal.settings object.
  // The new ones didn't exist then, so we need to update Drupal.settings
  // by ourselves in order to let AHAH know about those new form elements.
  $javascript = drupal_add_js(NULL, NULL);
  $output_js = isset($javascript['setting']) ? '<script type="text/javascript">jQuery.extend(Drupal.settings, ' . drupal_to_js(call_user_func_array('array_merge_recursive', $javascript['setting'])) . ');</script>' : '';

  /*   * ***
   * CV FLIER HIJACK
   */

  //count the number of images
  $count = 0;
  foreach ($form_state['values']['field_image'] as $data) {
    if ($data['fid'] > 0) {
      $count++;
    }
  }

  classifieds_ad_summary_update_images($form['classified_id']['#value'], $count);

  $output_js .= '<script type="text/javascript">
    Drupal.behaviors.classifiedimages = function() {
     $("#classifieds-price-wrapper").html(' . drupal_to_js(classifieds_ad_summary_update()) . ');
     }
    </script>';

  /////


  $output = theme('status_messages') . drupal_render($field_form) . $output_js;

  // Using drupal_json() breaks filefield's file upload, because the jQuery
  // Form plugin handles file uploads in a way that is not compatible with
  // 'text/javascript' response type.
  $GLOBALS['devel_shutdown'] = FALSE;
  print drupal_to_js(array('status' => TRUE, 'data' => $output));
  exit;
}

/**
 * Hijack the filefield upload button
 */
function classifieds_filefield_js($type_name, $field_name, $delta) {

  $field = content_fields($field_name, $type_name);

  // Immediately disable devel shutdown functions so that it doesn't botch our
  // JSON output.
  $GLOBALS['devel_shutdown'] = FALSE;

  if (empty($field) || empty($_POST['form_build_id'])) {
    // Invalid request.
    drupal_set_message(t('An unrecoverable error occurred. The uploaded file likely exceeded the maximum file size (@size) that this server supports.', array('@size' => format_size(file_upload_max_size()))), 'error');
    print drupal_to_js(array('data' => theme('status_messages')));
    exit;
  }

  // Build the new form.
  $form_state = array('submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);

  if (!$form) {
    // Invalid form_build_id.
    drupal_set_message(t('An unrecoverable error occurred. This form was missing from the server cache. Try reloading the page and submitting again.'), 'error');
    print drupal_to_js(array('data' => theme('status_messages')));
    exit;
  }

  // Build the form. This calls the file field's #value_callback function and
  // saves the uploaded file. Since this form is already marked as cached
  // (the #cache property is TRUE), the cache is updated automatically and we
  // don't need to call form_set_cache().
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form['#post'] = $_POST;
  $form = form_builder($form_id, $form, $form_state);

  // Update the cached form with the new element at the right place in the form.
  if (module_exists('fieldgroup') && ($group_name = _fieldgroup_field_get_group($type_name, $field_name))) {
    if (isset($form['#multigroups']) && isset($form['#multigroups'][$group_name][$field_name])) {
      $form_element = $form[$group_name][$delta][$field_name];
    }
    else {
      $form_element = $form[$group_name][$field_name][$delta];
    }
  }
  else {
    $form_element = $form[$field_name][$delta];
  }

  if (isset($form_element['_weight'])) {
    unset($form_element['_weight']);
  }

  $output = drupal_render($form_element);

  // AHAH is not being nice to us and doesn't know the "other" button (that is,
  // either "Upload" or "Delete") yet. Which in turn causes it not to attach
  // AHAH behaviours after replacing the element. So we need to tell it first.
  // Loop through the JS settings and find the settings needed for our buttons.
  $javascript = drupal_add_js(NULL, NULL);
  $filefield_ahah_settings = array();
  if (isset($javascript['setting'])) {
    foreach ($javascript['setting'] as $settings) {
      if (isset($settings['ahah'])) {
        foreach ($settings['ahah'] as $id => $ahah_settings) {
          if (strpos($id, 'filefield-upload') || strpos($id, 'filefield-remove')) {
            $filefield_ahah_settings[$id] = $ahah_settings;
          }
        }
      }
    }
  }


  /**
   * CV CLASSIFIEDS HIJACKS THIS HERE
   * If this is a classifieds node type form... send this
   * over to the ad_summary stuff, so we can build the Ad
   * Summary block
   */
  if (isset($form['field_image']) && isset($form['c_cat'])) { //this is definately our create ad form
    //require the sales file and call up the image function to update the ad summary block
    $blank = 0;
    foreach ($form_state['values']['field_image'] as $data) {
      if ($data['fid'] == 0) {
        $blank++;
      }
    }

    //update our images for the ad_summary
    classifieds_ad_summary_update_images($form['classified_id']['#value'], count($form_state['values']['field_image']) - $blank);

    //update our ad summary
    $output .= '<script type="text/javascript">
    Drupal.behaviors.classifiedimages = function() {
     $("#classifieds-price-wrapper").html(' . drupal_to_js(classifieds_ad_summary_update()) . ');
     }
    </script>';
  }
  //END HIJACK
  // Add the AHAH settings needed for our new buttons.
  if (!empty($filefield_ahah_settings)) {
    $output .= '<script type="text/javascript">jQuery.extend(Drupal.settings.ahah, ' . drupal_to_js($filefield_ahah_settings) . ');</script>';
  }

  $output = theme('status_messages') . $output;

  // For some reason, file uploads don't like drupal_json() with its manual
  // setting of the text/javascript HTTP header. So use this one instead.
  print drupal_to_js(array('status' => TRUE, 'data' => $output));
  exit;
}

/**
 * General function that updates all the pricing when called up by ajax. This
 * will call up all the update functions, so that items are updated correctly.
 */
function classifieds_ad_summary_update() {

  //get the form state somehow...
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  form_get_cache($form_build_id, $form_state);

  //get the _POST info
  $post = $_POST;

  $classified_id = $_POST['classified_id'];

  //update seller type pricing
  classifieds_ad_summary_update_seller($classified_id, $form_state, $post);

  //update image pricing - Dont do this here, it interfares with
  //the ad_summary updating
  //classifieds_ad_summary_update_images($form_state, $post);
  //update print ad - sets print multiples needed below
  classifieds_ad_summary_update_periodical($classified_id, $form_state, $post);

  //update number of print lines (print preview)
  classifieds_ad_summary_update_lines($classified_id, $form_state, $post);

  //update keywords entered
  classifieds_ad_summary_update_keyword($classified_id, $form_state, $post);

  //If print is not being run, make sure print attributes go away
  $classified_data = classifieds_get_data($classified_id);
  if (!isset($classified_data['ad_summary']['print']['price'])) {
    unset($classified_data['ad_summary']['print']);
  }
  classifieds_save_data($classified_id, $classified_data);

  //output formated ad_summary
  return classifieds_ad_summary_theme($classified_id);
}

/**
 * create rates page
 */
function classifieds_sales_rates_form($form_state) {

  //set title
  drupal_set_title('Classified Rates');

  //set div tags for ahah
  $form['open_div'] = array(
    '#value' => '<div id="classifieds-rates-ahah-wrapper">',
  );

  //seller type
  $form['seller'] = array(
    '#title' => t('Type of Seller'),
    '#type' => 'select',
    '#description' => t('Select whether you are listing this as an individual or for an business entity.'),
    '#options' => array(
      1 => t('Private Party'),
      2 => t('Commercial'),
    ),
    '#multiple' => FALSE,
    '#default_value' => $form_state['post']['seller'],
  );

  //format
  $form['format'] = array(
    '#title' => t('Type of Ad'),
    '#type' => 'select',
    //'#description' => t('Select whether .'),
    '#options' => array(
      1 => t('Online'),
      2 => t('Published'),
    ),
    '#multiple' => FALSE,
    '#default_value' => $form_state['post']['format'],
  );

  //build a list of parent terms
  $sql = "SELECT * FROM {classifieds_node_per_cat} JOIN {term_data} ON classifieds_node_per_cat.tid = term_data.tid ORDER BY weight";
  $result = db_query($sql);
  $options[0] = '  --SELECT CATEGORY--  ';

  if (isset($form_state['post']['category'])) {
    unset($options[0]);
  }

  while ($data = db_fetch_object($result)) {
    $options[$data->tid] = $data->name;
  }

  //list categories
  $form['category'] = array(
    '#title' => 'Category',
    '#type' => 'select',
    '#default_value' => $form_state['post']['category'],
    '#options' => $options,
    '#ahah' => array(
      'path' => 'classifieds/js/rates/subcat',
      'wrapper' => 'classifieds-rates-ahah-wrapper',
    ),
  );

  $sql = "SELECT * FROM {term_hierarchy} JOIN {term_data} ON term_hierarchy.tid = term_data.tid WHERE
term_hierarchy.parent = %d ORDER BY term_data.weight";

  //set default
  $opt['select'] = '- SELECT SUB-CAT -';

  if (isset($form_state['post']['category'])) {
    $result = db_query($sql, $form_state['post']['category']);
    while (($data = db_fetch_object($result)) !== FALSE) {
      $opt[$data->tid] = $data->name;
    }
  }

  //fill in the form stuff now... or else the form wont load its ahah
  $form['subcat'] = array(
    '#type' => 'select',
    '#options' => $opt,
    '#title' => 'Sub-Category',
    //'#disabled' => TRUE,
    '#default_value' => $form_state['post']['subcat'],
  );

  //dpm($form_state);

  if (isset($form_state['post']['sub_cat_valid']) && $form_state['post']['sub_cat_valid'] == TRUE) {
    $form['subcat']['#disabled'] = FALSE;
  }


  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Calculate'),
    '#ahah' => array(
      'path' => 'classifieds/js/rates/button',
      'wrapper' => 'classifieds-rates-ahah-wrapper',
    ),
    '#disabled' => TRUE,
  );

  if (count($form_state['post']) != 0) {
    $form['submit']['#disabled'] = FALSE;
  }


  $form['close_div'] = array(
    '#value' => '</div>',
  );


  //div tag to display quote
  $form['quote'] = array(
    '#value' => '<br /><br /><div id="classifieds-calc-rate-quote"></div>',
  );

  return $form;
}

/**
 * Handles AHAH for classified rates
 *
 */
function classifieds_rates_js($id) {

  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];

  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);

  $form['#post'] = $_POST;
  $form['#redirect'] = FALSE;
  $form['#programmed'] = FALSE;
  $form_state['post'] = $_POST;

  // Prevents _form_builder_ie_cleanup() from incorrectly assigning the
  // first button in the form as the clicked button.
  // Wim Leers' AHAH Helper Module has more in-depth information.
  // @see the ahah_helper project
  $form_state['submitted'] = TRUE;

  drupal_process_form($form_id, $form, $form_state);

  // Once drupal_rebuild_form is called, we no longer have access to
  // $form_state['values'], so we merge it into $form_state['storage'].
  if (isset($form_state['values'])) {
    if (!isset($form_state['storage'])) {
      $form_state['storage'] = array();
    }
    $storage = $form_state['storage'];
    $values = $form_state['values'];
    $form_state['storage'] = array_smart_merge($storage, $values);
  }

  // Rebuild the form.
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);



  //see if this is a button click
  if ($id == 'button') {
    if ($form_state['post']['subcat'] == 'select') {
      form_set_error('subcat', 'You must select a sub-category.');
    }
    else { //no errors
      //build js to update classifieds-rates-quote div
      $output .= '<script type="text/javascript">
                  Drupal.behaviors.classifiedRates = function() {
                   $("#classifieds-calc-rate-quote").html(' . drupal_to_js(classifieds_sales_calculate_quote($form_state['post'])) . ');
                   }
                  </script>';
    }
  }

  $output .= drupal_render($form);

  // Get the JS settings so we can merge them.
  $javascript = drupal_add_js(NULL, NULL, 'header');
  $settings = call_user_func_array('array_merge_recursive', $javascript['setting']);

  drupal_json(array(
    'status' => TRUE,
    'data' => theme('status_messages') . $output,
    'settings' => array('ahah' => $settings['ahah']),
  ));
}

/**
 * this helper function checks to make sure that a node api
 * that has been submitted is priced correctly by the ad_summary.
 * this is done in case someone turns off the javascript
 * and ad summary can't update
 *
 * This is manuall being done...
 */
function classifieds_sales_calculate_form($form) {

  $classified_data = classifieds_get_data($form['classified_id']['#value']);

  //determine seller type
  if ($form['seller']['#value'] == 0) {
    $seller = 'private';
  }
  else {
    $seller = 'commercial';
  }

  //get sales rules
  module_load_include('inc', 'classifieds', '/sales/classifieds.sales');
  $sales = new sales($classified_data['child_tid'], $classified_data['parent_tid']);
  $sale_rules = $sales->getSaleRules();


  //determine number of images
  $no_of_images = 0;

  $no_of_image_spots = count($form['field_image']) - 18;

  for ($i = 0; $i < $no_of_image_spots; $i++) {
    if ($form['field_image'][$i]['fid']['#value'] != 0) {
      $no_of_images++;
    }
  }

  if ($no_of_images > $sale_rules['images']['free']) {
    $extra_images = $no_of_images - $sale_rules['images']['free'];
  }
  else {
    $extra_images = 0;
  }

  //determine number of lines
  $chars_per_line = variable_get('classifieds_publish_chars_per_line', NULL);
  $no_of_lines = count(explode("\n", wordwrap($form['print_ad']['#value'], $chars_per_line)));

  if ($no_of_lines > $sale_rules['lines']['free_lines']) {
    $extra_lines = $no_of_lines - $sale_rules['lines']['free_lines'];
  }
  else {
    $extra_lines = 0;
  }

  //determine if there is a keycode
  if ($form['keyword']['#value'][1] == 1) {
    $keycode_count = 1;
  }
  else { //no keycode
    $keycode_count = 0;
  }

  //determine number of scheduled print ad runs
  $print_runs = count($form['run_ad']['#value']);

  //figure out online values and attributes
  $online_base = $sale_rules['base']['price'] + $sale_rules['seller'][$seller];
  ; //seller + base
  $images = $sale_rules['images']['price'] * $extra_images; //$sale_rules['images']['free']
  //figure out print values
  $print_base = $sale_rules['print_base']['price'] + $sale_rules['print_seller'][$seller];
  $lines = $sale_rules['lines']['price'] * $extra_lines;
  $keycode = $sale_rules['keyword']['price'] * $keycode_count;

  //determine calculations
  $online_ad = $online_base + $images;
  $print_ad = ($print_base + $lines + $keycode) * $print_runs;

  //final price
  $price = $online_ad + $print_ad;

  //DEBUGGING
  /* dpm('PRICING');
    dpm('online ad '.$online_ad);
    dpm('images '.$images);

    dpm('print ad '.$print_ad);
    dpm('lines '.$lines);
    dpm('keycode '.$keycode);

    dpm('total '.$price); */

  //return
  return $price;
}

/**
 * this function builds the rates for the classified rates form.
 * it uses the tids given, plus online/publish and type of seller.
 */
function classifieds_sales_calculate_quote($post) {

  //pull up sale rules
  $sale_rules = classifieds_sales_get_rules($post['subcat'], $post['category']);

  //get seller
  if ($post['seller'] == 1) {
    $seller = 'private';
  }
  else {
    $seller = 'commercial';
  }

  //place holder for attributes
  $extras = array();

  //online or publish
  if ($post['format'] == 1) { //Online ad
    //get the costs
    $price = $sale_rules['base']['price'] + $sale_rules['seller'][$seller];

    //list online ad attributes: images
    $extras[] = 'You are allowed ' . $sale_rules['images']['free'] . ' FREE photos, each additional photo is <strong>' . _classifieds_sales_format_price($sale_rules['images']['price']) . '</strong>.';
  }
  else { //Publish ad
    //get the costs
    $price = $sale_rules['print_base']['price'] + $sale_rules['print_seller'][$seller];

    //list print ad attributes: lines, keyword
    $extras[] = 'You are allowed ' . $sale_rules['lines']['free_lines'] . ' free lines. Each additional line is <strong>' . _classifieds_sales_format_price($sale_rules['lines']['price']) . '</strong>.';
    if ($sale_rules['keyword']['hide'] != 1) {
      $extras[] = 'A keycode that links your print ad to your online ad can be purchased for <strong>' . _classifieds_sales_format_price($sale_rules['keyword']['price']) . '</strong>';
    }
  }

  $price = _classifieds_sales_format_price($price);

  if ($price == 'FREE') {
    $string = 'This ad will be <strong>FREE</strong>.';
  }
  else {
    $string = 'This ad will cost <strong>' . $price . '</strong>.';
  }

  //add in extras
  foreach ($extras as $add_string) {
    $string .= '<br />' . $add_string;
  }

  return $string;
}

/**
 * helper function that formats a price
 */
function _classifieds_sales_format_price($price) {

  if ($price == 0) {
    $price = 'FREE';
  }
  else {
    $price = '$' . number_format($price, 2);
  }

  return $price;
}

/**
 * Returns (and possibly creates) a the classifieds product node
 */
function _classifieds_sales_get_uc_product() {
  $nid = variable_get('classifieds_product_nid', '');

  if (empty($nid)) {
    // Check to see if a classifieds product already exists.
    $nid = db_result(db_query("SELECT nid FROM {node} WHERE
    title='Classified Ad' AND type='product'"));
    if (!$nid) {

      //creating a bare node
      $node = new stdClass();

      //insert product node data
      $node->nid = NULL;
      $node->vid = NULL;
      $node->uid = 1;
      //$node->created = 1314826973;
      $node->type = 'product';
      $node->language = NULL;
      $node->changed = NULL;
      $node->title = 'Classified Ad';
      $node->teaser_js = NULL;
      $node->teaser_include = 1;
      $node->body = NULL;
      $node->format = 1;
      $node->model = 'CLASSIFIED';
      $node->list_price = 0;
      $node->cost = 0;
      $node->sell_price = 0;
      $node->shippable = 0;
      $node->weight = 0;
      $node->weight_units = 'lb';
      $node->length_units = 'in';
      $node->dim_length = NULL;
      $node->dim_width = NULL;
      $node->dim_height = NULL;
      $node->pkg_qty = 1;
      $node->default_qty = 0;
      $node->ordering = 0;
      $node->revision = 0;
      $node->log = NULL;
      //$node->name = 'mburns';
      $node->date = NULL;
      $node->status = 1;
      $node->promote = 1;
      $node->sticky = 0;
      $node->op = 'Save';
      $node->submit = 'Save';
      $node->preview = 'Preview';
      $node->save_continue = 'Save and continue';
      //$node->form_build_id = 'form-237b19c752690e0813a47a2dea105d9f';
      //$node->form_token = '3521d0a1de9d7879b6cf71825f8841b9';
      $node->form_id = 'product_node_form';
      $node->comment = 2;
      /* $node->menu = array(
        $node->mlid = 0,
        $node->module = menu,
        $node->hidden = 0,
        $node->has_children = 0,
        $node->customized = 0,
        $node->options = Array ( ),
        $node->expanded = 0,
        $node->parent_depth_limit = 8,
        $node->link_title = NULL,
        $node->parent = 'primary-links:0',
        $node->weight = 0,
        ); */
      //$node->path = NULL;
      $node->pathauto_perform_alias = 1;

      //save it and give it the rest of the attributes
      node_save($node);

      //save the new nid
      $nid = $node->nid;
    }

    variable_set('classifieds_product_nid', $nid);
  }

  return $nid;
}

/**
 * Returns (and possibly creates) a the classifieds product node
 */
function _classifieds_sales_get_uc_product_edit() {
  $nid = variable_get('classifieds_product_edit_nid', '');

  if (empty($nid)) {
    // Check to see if a classifieds product already exists.
    $nid = db_result(db_query("SELECT nid FROM {node} WHERE
    title='Classified Ad Edit' AND type='product'"));
    if (!$nid) {

      //creating a bare node
      $node = new stdClass();

      //insert product node data
      $node->nid = NULL;
      $node->vid = NULL;
      $node->uid = 1;
      //$node->created = 1314826973;
      $node->type = 'product';
      $node->language = NULL;
      $node->changed = NULL;
      $node->title = 'Classified Ad Edit';
      $node->teaser_js = NULL;
      $node->teaser_include = 1;
      $node->body = NULL;
      $node->format = 1;
      $node->model = 'CLASSIFIED-EDIT';
      $node->list_price = 0;
      $node->cost = 0;
      $node->sell_price = 0;
      $node->shippable = 0;
      $node->weight = 0;
      $node->weight_units = 'lb';
      $node->length_units = 'in';
      $node->dim_length = NULL;
      $node->dim_width = NULL;
      $node->dim_height = NULL;
      $node->pkg_qty = 1;
      $node->default_qty = 0;
      $node->ordering = 0;
      $node->revision = 0;
      $node->log = NULL;
      //$node->name = 'mburns';
      $node->date = NULL;
      $node->status = 1;
      $node->promote = 1;
      $node->sticky = 0;
      $node->op = 'Save';
      $node->submit = 'Save';
      $node->preview = 'Preview';
      $node->save_continue = 'Save and continue';
      //$node->form_build_id = 'form-237b19c752690e0813a47a2dea105d9f';
      //$node->form_token = '3521d0a1de9d7879b6cf71825f8841b9';
      $node->form_id = 'product_node_form';
      $node->comment = 2;
      /* $node->menu = array(
        $node->mlid = 0,
        $node->module = menu,
        $node->hidden = 0,
        $node->has_children = 0,
        $node->customized = 0,
        $node->options = Array ( ),
        $node->expanded = 0,
        $node->parent_depth_limit = 8,
        $node->link_title = NULL,
        $node->parent = 'primary-links:0',
        $node->weight = 0,
        ); */
      //$node->path = NULL;
      $node->pathauto_perform_alias = 1;

      //save it and give it the rest of the attributes
      node_save($node);

      //save the new nid
      $nid = $node->nid;
    }

    variable_set('classifieds_product_edit_nid', $nid);
  }

  return $nid;
}