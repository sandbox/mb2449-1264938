<?php

/**
 * @file
 * Contains all the functions needed to build the ad summary on
 * the create ad page.  This might get turned into OOP...
 */

/**
 * ad summary first run
 * called up on the first run by the ad summary block
 * this will build the first ad summary and save it...
 * the tid is the sub tid
 */
function classifieds_ad_summary_first_run($classified_id) {

  //get classified data
  $classified_data = classifieds_get_data($classified_id);

  //check to see if this is the first run
  if (!isset($classified_data['ad_summary'])) {
    //check the url to see if this is an edit form... and it's the first run
    if (is_numeric(arg(1)) && arg(0) == 'node' && arg(2) == 'edit') {
      //given nid, lookup ad_summary - get latest ad summary whether paid for or not
      $sql = "SELECT * FROM {classifieds_sales} AS a WHERE a.nid=%d ORDER BY a.timestamp DESC";
      $db = db_fetch_object(db_query($sql, check_plain(arg(1))));

      $classified_data['ad_summary'] = unserialize($db->ad_summary);

      //save classified data
      classifieds_save_data($classified_id, $classified_data);
    }
    else { //this is a regular create ad form
      //forge values
      $post['seller'] = 0; //private party ad
      $form_state['storage']['step'] = 1;

      //run update seller type, which is the option loaded on the front page
      classifieds_ad_summary_update_seller($classified_id, $form_state, $post);
    }
  }
}

/**
 * ad_summary_update_images - this works with our filefield hijack to
 * update the price of images when they are deleted / uploaded.
 */
function classifieds_ad_summary_update_images($classified_id, $no_of_images) {

  //load classified data
  $classified_data = classifieds_get_data($classified_id);

  //set sale rules
  $sale_rules = $classified_data['sale_rules'];

  //
  if ($no_of_images > $classified_data['sale_rules']['images']['free']) { // then we need to start charging
    $extra = $no_of_images - $classified_data['sale_rules']['images']['free'];
    $charge = number_format($extra * $classified_data['sale_rules']['images']['price'], 2);
    $cost_per_image = number_format($classified_data['sale_rules']['images']['price'], 2);

    //build the ad_summary
    $classified_data['ad_summary']['online']['children'] = array('images' => array(
        'data' => 'Extra Photos:  <strong>+$' . $charge . '</strong><br /><em>(' . $extra . ' @ $' . $cost_per_image . ')</em>',
        'price' => $charge,
        ));
  }
  else { //delete last extra photo if none...
    unset($classified_data['ad_summary']['online']['children']['images']);
  }

  //save the data
  classifieds_save_data($classified_id, $classified_data);
}

/**
 * this gets called when step 1 or 3 is loaded, so that the person can get a published ad!
 */
function classifieds_ad_summary_update_periodical($classified_id, $form_state, $post) {

  //load classified data array
  $classified_data = classifieds_get_data($classified_id);

  //define variables we need
  if ($form_state['storage']['step'] == 3) {

    //check to see if we have old runs we need to combine...
    if (count($classified_data['publish']['prev_runs']) > 0) {
      foreach ($classified_data['publish']['prev_runs'] as $date) {
        $post['run_ad'][$date] = $date;
      }
    }

    $run_ad = $post['run_ad'];
    $seller = $form_state['storage']['values'][1]['seller'];
  }
  elseif ($form_state['storage']['step'] == 1) {
    //$run_ad = $form_state['storage']['values'][3]['run_ad'];
    $seller = $post['seller'];

    $run_ad = array();
    if (isset($classified_data['ad_summary']['print']['runs'])) {
      foreach ($classified_data['ad_summary']['print']['runs'] as $run) {
        $run_ad[$run['date']] = $run['date'];
      }
    }
  }

  //add in sale rules
  $sale_rules = $classified_data['sale_rules'];

  //only update periodical if we are step 3
  if ($form_state['storage']['step'] == 3 || $form_state['storage']['step'] == 1) {

    if (empty($run_ad)) {//remove the whole print category
      unset($classified_data['ad_summary']['print']);
    }
    else { //add a print category
      if ($seller == 0) { //private ad
        $seller_price = $sale_rules['print_seller']['private'];
      }
      else { //commercial
        $seller_price = $sale_rules['print_seller']['commercial'];
      }

      $price = $sale_rules['print_base']['price'] + $seller_price;

      if ($price == 0) {
        $price_text = 'FREE';
      }
      else {
        $price_text = '$' . $price;
      }

      //save any children
      if (isset($classified_data['ad_summary']['print']['children'])) {
        $children = $classified_data['ad_summary']['print']['children'];
      }


      //count up number of run ads
      $print_mult = count($run_ad);
      if ($print_mult > 1) {
        $print_mult_string = ' <em>(' . _classifieds_sales_format_price($price) . ' x ' . $print_mult . ')</em>';
      }

      //call up print func
      $classified_data['ad_summary']['print'] = array(
        'data' => 'Print: <strong>' . _classifieds_sales_format_price($price * $print_mult) . '</strong>' . $print_mult_string,
        'price' => $price * $print_mult,
        'mult' => $print_mult,
      );

      //call up publish module
      module_load_include('inc', 'classifieds', '/publish/classifieds_publish');

      //build print running array
      if (count($run_ad) > 0) {
        foreach ($run_ad as $key => $value) {
          if ($key == $value) {

            $date = _classifieds_publish_create_run_string($key, 'M j');

            $classified_data['ad_summary']['print']['runs'][] = array(
              'date' => $key,
              'data' => '<em>(Running ' . $date['start'] . ' -> ' . $date['end'] . ')</em>',
            );
          }
        }
      }

      if (isset($children)) {
        $classified_data['ad_summary']['print']['children'] = $children;
      }
    }

    //save any changes made to the ad summary in classified data array
    classifieds_save_data($classified_id, $classified_data);
  }
}

/**
 * Used to update the ad summary when some one is typing
 * in the text box
 */
function classifieds_ad_summary_update_lines($classified_id, $form_state, $post) {

  dpm('updating lines!');

  //this is always getting called, only update if on step 3 though
  if ($form_state['storage']['step'] == 3) {

    //load classified data
    $classified_data = classifieds_get_data($classified_id);

    //define variables
    $print_ad = $post['print_ad'];
    $chars_per_line = $classified_data['publish']['chars_per_line'];

    //define sale rules
    $sale_rules = $classified_data['sale_rules'];

    //define vars to be used in ad summary
    $no_of_lines = count(explode("\n", wordwrap($print_ad, $chars_per_line)));
    $extra = $no_of_lines - $sale_rules['lines']['free_lines'];
    $charge = $extra * $sale_rules['lines']['price'];
    $cost_per_line = number_format($sale_rules['lines']['price'], 2);

    //count up number of run ads
    $print_mult = $classified_data['ad_summary']['print']['mult'];
    if ($print_mult > 1) {
      $print_mult_string = ' <em>(' . _classifieds_sales_format_price($charge) . ' x ' . $print_mult . ')</em>';
    }

    if ($extra > 0) {
      //build the ad_summary
      $classified_data['ad_summary']['print']['children']['lines'] = array(
        'data' => 'Extra Lines: <strong>+' . _classifieds_sales_format_price($charge * $print_mult) . '</strong>' . $print_mult_string
        . '<br /><em>(' . $extra . ' @ $' . $cost_per_line . ')</em>',
        'price' => $charge * $print_mult,
      );
    }
    else {
      //get rid of it, its not needed
      unset($classified_data['ad_summary']['print']['children']['lines']);
    }

    //save classified_data
    classifieds_save_data($classified_id, $classified_data);
  }
}

/**
 * Used for updating the keyword field
 */
function classifieds_ad_summary_update_keyword($classified_id, $form_state, $post) {

  //this is always getting called, only update if on step 3 though
  if ($form_state['storage']['step'] == 3) {

    //define keyword
    $keyword = $post['keyword'];

    //load classiifed data
    $classified_data = classifieds_get_data($classified_id);

    //define sale rules
    $sale_rules = $classified_data['sale_rules'];

    //define vars to be used in ad summary
    //$charge =  number_format($sale_rules['keyword']['price'],2);
    $charge = $sale_rules['keyword']['price'];


    //count up number of run ads
    $print_mult = $classified_data['ad_summary']['print']['mult'];
    if ($print_mult > 1) {
      $print_mult_string = ' <em>(' . _classifieds_sales_format_price($charge) . ' x ' . $print_mult . ')</em>';
    }



    if ($keyword[1] == 1) { //they want it...
      //build the ad_summary
      $classified_data['ad_summary']['print']['children']['keyword'] = array(
        'data' => 'Keycode: <strong>+' . _classifieds_sales_format_price($charge * $print_mult) . '</strong>' . $print_mult_string,
        'price' => $charge * $print_mult,
      );
    }
    else { //remove it...
      unset($classified_data['ad_summary']['print']['children']['keyword']);
    }

    //save any changes made to the summary
    classifieds_save_data($classified_id, $classified_data);
  }
}

/**
 * ad_summary_update_seller - Called by the general ad_summary_update
 * function. Determines seller type pricing.
 */
function classifieds_ad_summary_update_seller($classified_id, $form_state, $post) {

  $classified_data = classifieds_get_data($classified_id);

  //add in sale rules
  $sale_rules = $classified_data['sale_rules'];

  //only update seller type if we are on step one
  if ($form_state['storage']['step'] == 1) {
    //add in seller type
    //$seller_type = $form_state['storage']['values'][1]['seller']; -this works when seller isnt there...
    $seller_type = $post['seller'];

    //check seller type
    if ($seller_type == 0) { //private party
      $price = $classified_data['sale_rules']['base']['price'] + $classified_data['sale_rules']['seller']['private'];
    }
    else { //commercial
      $price = $classified_data['sale_rules']['base']['price'] + $classified_data['sale_rules']['seller']['commercial'];
    }

    //save any children
    if (isset($classified_data['ad_summary']['online']['children'])) {
      $children = $classified_data['ad_summary']['online']['children'];
    }

    //build ad summary
    $classified_data['ad_summary']['online'] = array(
      'data' => 'Online: <strong>' . _classifieds_sales_format_price($price) . '</strong>',
      'price' => $price,
    );

    if (isset($children)) {
      $classified_data['ad_summary']['online']['children'] = $children;
    }

    //save any changes made to the summary
    classifieds_save_data($classified_id, $classified_data);
  }
}

/**
 * makes the list pretty
 * called by _ad_summary_update to output the prices to the user
 * also gets called by the ad summary block
 *
 * this is responsible for calculating the total
 */
function classifieds_ad_summary_theme($classified_id) {

  //get classified data array
  $classified_data = classifieds_get_data($classified_id);

  //load ad summary
  $ad_summary = $classified_data['ad_summary'];

  //set price
  $price = 0;

  //build list, and set prices accordingly
  $list = '<ul class="parent">';
  $list .= '<li>' . $ad_summary['online']['data'] . '</li>';
  $price += $ad_summary['online']['price'];

  //show online extra imgaes
  if (isset($ad_summary['online']['children']['images'])) {
    $list .= '<ul class="child"><li>' . $ad_summary['online']['children']['images']['data'] . '</li></ul>';
    $price += $ad_summary['online']['children']['images']['price'];
  }

  //show print price, and schedule if greater then 1
  if (isset($ad_summary['print']['data'])) {
    $list .= '<li>' . $ad_summary['print']['data'];

    $list .= '</li>';
    $price += $ad_summary['print']['price'];

    $runs = count($ad_summary['print']['runs']);
    if ($runs > 1) {
      $list .= '<ul class="child"><li><em>Scheduled Runs: ' . $runs . '</em></li></ul>';
    }
  }

  //show print lines
  if (isset($ad_summary['print']['children']['lines'])) {
    $list .= '<ul class="child"><li>' . $ad_summary['print']['children']['lines']['data'] . '</li></ul>';
    $price += $ad_summary['print']['children']['lines']['price'];
  }

  //show print keycode
  if (isset($ad_summary['print']['children']['keyword'])) {
    $list .= '<ul class="child"><li>' . $ad_summary['print']['children']['keyword']['data'] . '</li></ul>';
    $price += $ad_summary['print']['children']['keyword']['price'];
  }

  //check to see if this ad is being edited, if so- save the last ad summary data for comparison
  //THIS GETS RUN ONLY ONCE
  if (isset($classified_data['edit']) && is_numeric(arg(1)) && _classifieds_has_completed_sale_record(arg(1))
      && !isset($classified_data['last_ad_summary'])) {
    //get latest sales history
    $sales_history = _classifieds_get_sale_records(arg(1));

    //scan through sales_history to find the latest paid for ad_summary
    //0 is the newest, i+1 being the highest
    for ($i = 0; $i < count($sales_history); $i++) {
      if ($sales_history[$i]->edit == 0) {
        break;
      }
    }

    //find the last ad_summary that was paid for
    $last_ad_summary = unserialize($sales_history[$i]->ad_summary);

    //save data
    $classified_data['last_ad_summary'] = $last_ad_summary;
  }


  //this is an edited item that has already been payed for once....
  if (isset($classified_data['last_ad_summary'])) {

    //compare this ad summary to saved ad summary
    $paid = $classified_data['last_ad_summary']['total'];
    $new_total = $price - $paid;

    //no one gets money back!
    if ($new_total < 0) {
      $new_total = 0;
    }

    //build list items
    $list .= '<li><span class="subtotal"><em>Sub-Total:</em> ' . _classifieds_sales_format_price($price) . '</span></li>';
    $list .= '<li><span class="subtotal"><em>Paid:</em> -' . _classifieds_sales_format_price($paid) . '</span></li>';
    $list .= '<li><span class="total"><em>New Total:</em> ' . _classifieds_sales_format_price($new_total) . '</span></li>';
    $list .= '</ul>';

    //save total values for later.
    $classified_data['ad_summary']['new_total'] = $new_total;
    $classified_data['ad_summary']['paid'] = $paid;
  }
  else { //show the regular total screen
    //this is a normal new ad
    $list .= '<li><span class="total"><em>Total:</em> ' . _classifieds_sales_format_price($price) . '</span></li>';
    $list .= '</ul>';
  }

  //save price for later
  $classified_data['ad_summary']['total'] = $price;
  classifieds_save_data($classified_id, $classified_data);

  //return the themed list...
  return $list;
}
