      Drupal.behaviors.classifiedPrintAd = function() {
        var delay = (function(){
          var timer = 0;
          return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
          };
        })();

        $('#edit-print-ad').keyup(function() {
            
            delay(function(){
              $.ajax({
                type: 'POST',
                url: $('#edit-preview-url').val(),
                data: $('#node-form').serializeArray(),
                success: function(data) {
                  var result = Drupal.parseJson(data);
                  $('#classifieds-preview-wrapper').html(result.data);
                }
              });

            }, 700 );
        });


     }