<?php

/**
 * @file
 * Sales Admin is responsible for creating the admin stuff needed for
 * changing sales rules, etc
 */

/**
 * page call back
 *
 * Publish page under menu
 */
function classifieds_admin_sales_page() {

  //Print a list of all default rules
  $output .= '<strong>Default Rules</strong><br />';

  //build header
  $headers = array(
    //array('data' => t('UID'), 'field' => 'uid', 'sort'=> 'desc'),
    array('data' => t('Sale Rule'), 'field' => 'name'),
    array('data' => t('Rule Type'), 'field' => 'sale_type'),
    array('data' => '') //'colspan' => 2
  );

  //build sql
  $sql = "SELECT a.sid,name,sale_type FROM {classifieds_sale_rules} AS a JOIN {classifieds_rules2cat} AS b ON a.sid = b.sid  WHERE b.tid=0";
  $sql .= tablesort_sql($headers);

  //run query
  $result = db_query($sql);

  //set up data array
  $data = array();
  $i = 1;

  //your basic while loop to get the data
  while ($tmp = db_fetch_array($result)) {

    //add links...
    $tmp['name'] = l($tmp['name'], "admin/classifieds/sales/rules/" . $tmp['sale_type'] . '/' . $tmp['sid']);

    //save the data
    $data[$i] = $tmp;

    //we dont want to show these fields
    unset($data[$i]['sid']);

    //advance the counter
    $i++;
  }

  //save to output
  $output .= theme('table', $headers, $data);


  //Print a list of all category rules
  $output .= '<strong>Category Rules</strong><br />';

  //build header
  $headers = array(
    //array('data' => t('UID'), 'field' => 'uid', 'sort'=> 'desc'),
    array('data' => t('Sale Rule'), 'field' => 'name'),
    array('data' => t('Rule Type'), 'field' => 'sale_type'),
    array('data' => 'Categories'),
      //array('data' => '') //'colspan' => 2
  );

  //build sql
  $sql = "SELECT a.sid,name,sale_type FROM {classifieds_sale_rules} AS a WHERE a.sid NOT IN (SELECT sid FROM {classifieds_rules2cat} WHERE tid =0)";
  $sql .= tablesort_sql($headers);

  //run query
  $result = db_query($sql);

  //set up data array
  $data = array();
  $i = 1;

  //your basic while loop to get the data
  while ($tmp = db_fetch_array($result)) {

    $main_string = ''; //do this or the categories accumulate
    $sub_string = '';

    //Find all the main categories associated with this rule
    $main_cats = db_query("SELECT * FROM {classifieds_rules2cat} AS a JOIN {term_data} AS b ON a.tid=b.tid WHERE a.sid=%d AND a.tid IN (SELECT tid FROM {classifieds_node_per_cat})", $tmp['sid']);
    while (($data_main = db_fetch_object($main_cats)) !== FALSE) {
      $main_string .= $data_main->name . ', ';
    }

    //Find all the sub categories associated with this rule
    $sub_cats = db_query("SELECT * FROM {classifieds_rules2cat} AS a JOIN {term_data} AS b ON a.tid=b.tid WHERE a.sid=%d AND a.tid NOT IN (SELECT tid FROM {classifieds_node_per_cat})", $tmp['sid']);
    while (($data_sub = db_fetch_object($sub_cats)) !== FALSE) {
      $sub_string .= $data_sub->name . ', ';
    }

    $cat_string = '<strong><em>Main Categories:</em></strong><br /> ' . substr($main_string, 0, -2) . '<br />';
    $cat_string .='<strong><em>Sub Cateogries:</em></strong><br /> ' . substr($sub_string, 0, -2);

    //add links...
    $tmp['categories'] = $cat_string;
    $tmp['name'] = l($tmp['name'], "admin/classifieds/sales/rules/" . $tmp['sale_type'] . '/' . $tmp['sid']);

    //save the data
    $data[$i] = $tmp;

    //we dont want to show these fields
    unset($data[$i]['sid']);

    //advance the counter
    $i++;
  }

  //save to output
  $output .= theme('table', $headers, $data);

  //return
  return $output;
}

/**
 * Add Sales Rule Form
 */
function classifieds_sales_add_rule_form($form_state, $sale_rule) {

  //dpm($form_state);
  // create a delete variable in case the form is new
  if (!isset($form_state['storage']['delete'])) {
    $form_state['storage']['delete'] = FALSE;
  }

  // if the delete button has been clicked, we must show the delete confirmation.
  if ($form_state['storage']['delete'] == TRUE && is_numeric(arg(5))) {
    //the form is being deleted
    //get the attrib name
    $sale = db_fetch_object(db_query("SELECT * FROM {classifieds_sale_rules} WHERE sid=%d", arg(5)));

    //DELETE FORM...

    $form['question'] = array(
      '#title' => t('Are you sure?'),
      '#type' => 'item',
      '#description' => t('This action will delete @name and can not be undone.', array('@name' => $sale->name)),
    );

    $form['sid'] = array(
      '#type' => 'hidden',
      '#value' => arg(5),
    );

    $form['sale_type'] = array(
      '#type' => 'hidden',
      '#value' => $sale->sale_type,
    );

    $form['sale_id'] = array(
      '#type' => 'hidden',
      '#value' => $sale->sale_id,
    );

    $form['submit_delete_confirm'] = array(
      '#type' => 'submit',
      '#value' => t('Confirm'),
    );
  }
  else { //show the regular form
    //Is this an edit form?
    $edit_form = FALSE;

    //determine if the argument has been set...
    if (is_numeric(arg(5))) {

      //this is an edit form.... so modify the form where appropriate.
      $edit_form = TRUE;

      //Get basic default values
      $sql = "SELECT * FROM {classifieds_sale_rules} AS a WHERE a.sid = %d";
      $result = db_query($sql, arg(5));
      $data = db_fetch_object($result); //this var will hold the default values...
      //save default values
      $sid = $data->sid;
      $name = $data->name;
      $rule_type = $data->apply_to;

      //Loop through category values
      $sql = "SELECT * FROM {classifieds_rules2cat} AS a WHERE a.sid = %d";

      $result = db_query($sql, $sid);

      //start default value
      $cat_options = array();

      while (($data = db_fetch_object($result)) !== FALSE) {
        if ($data->tid == 0) {
          $default = 1; //set form value
        }
        else {
          $cat_options[$data->tid] = $data->tid;
        }
      }
    }

    //define attribute name for personal use
    $form['name'] = array(
      '#title' => t('Name'),
      '#type' => 'textfield',
      '#description' => t('Enter a human-readable rule name for administration purposes.'),
      '#default_value' => $name,
      '#maxlength' => 40,
      '#required' => TRUE,
      '#size' => 30,
    );

    //figure out if this sale rule is for online ads, or published ads
    module_load_include('inc', 'classifieds', 'sales/sales.' . $sale_rule);
    if (call_user_func('classifieds_sales_' . $sale_rule . '_is_online')) {
      //this is an online ad
      $online = 1;
    }
    else {
      $online = 0;
    }

    //show hidden online field
    $form['online'] = array(
      '#type' => 'hidden',
      '#value' => $online,
    );


    $form['apply_to'] = array(
      '#type' => 'fieldset',
      '#title' => t('Apply rule'),
      '#description' => 'Please choose at least one of these options.',
      '#collapsible' => TRUE,
    );

    $form['apply_to']['default'] = array(
      //'#title' => t('Special conditions'),
      '#type' => 'checkboxes',
      '#options' => array(1 => 'Default'),
      '#default_value' => array($default),
      '#description' => (t('Applies to all categories, unless overridden. <em>Note: There can only be one default per rule per rule type. Also, if this is checked, any categories associated with this rule will be deleted.</em>')),
    );

    $form['apply_to']['fields'] = array(
      '#type' => 'fieldset',
      '#title' => t('Select categories'),
      '#description' => t('Select what categories this attribute should be applied to.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['apply_to']['fields']['list'] = array(
      '#type' => 'select',
      //'#title' => t('Select which categories to apply this attribute to'),
      '#multiple' => TRUE,
      '#default_value' => $cat_options,
      '#size' => 9,
      '#options' => _classifieds_generate_cat_options(),
      '#description' => 'Press CTRL to select multiple fields.'
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add Rule'),
      '#validate' => array('classifieds_sales_add_rule_form_validate_custom'), //only validate if we are submitting
      '#weight' => 100,
    );

    if ($edit_form == TRUE) {
      //modify the submit button
      $form['submit']['#value'] = 'Save Rule';

      //add an edit button...
      $form['submit_delete'] = array(
        '#type' => 'submit',
        '#value' => t('Delete Rule'),
        '#weight' => 105,
      );

      //include a hidden field
      $form['update'] = array(
        '#type' => 'hidden',
        '#value' => $sid,
      );
    }

    //given the attribute type, include the proper file
    require_once(dirname(__FILE__) . '/sales/sales.' . $sale_rule . '.inc');

    //given the attribute type, run the proper function to
    //include it's specific form attributes
    call_user_func_array('classifieds_sales_' . $sale_rule . '_add_form', array(&$form));

    //insert a hidden field here... so we know what type of form this is...
    $form['type'] = array(
      '#type' => 'hidden',
      '#value' => $sale_rule,
    );
  }

  return $form;
}

/**
 * Validate my attribute...
 */
function classifieds_sales_add_rule_form_validate_custom($form, $form_state) {

  //figure out the important variables we need to pass on
  $sale_rule = $form['type']['#value']; //base, images, etc...
  $rule_type = $form['rule_type']['#value']; //online or print
  //check to make sure there isnt a default for this attribute type already...
  if ($form['apply_to']['default']['#value'][1] &&
      _classifieds_sales_check_default($sale_rule, $form['update']['#value']) == TRUE) {

    $publication = _classifieds_sales_get_publication_name($data->apply_to);

    form_set_error('default', t('There is already a default set by this rule type for ' . $publication . ' ads.'));
  }

  //check to make sure this attribute is a default or has a category selected
  if (empty($form['apply_to']['default']['#value']) &&
      empty($form['apply_to']['fields']['list']['#value'])) {

    form_set_error('default', t('This sale rule must be applied to a category, or marked as default.'));
  }

  //if categories are selected, we must make sure that each category only has one
  //sale rule associated with it
  if (!empty($form['apply_to']['fields']['list']['#value'])) {

    //loop through the values
    foreach ($form['apply_to']['fields']['list']['#value'] as $key => $value) {

      //the key is the tid
      // see if this tid already has a attribute of this type assigned to it
      $sql = "SELECT * FROM {classifieds_rules2cat} AS a JOIN {classifieds_sale_rules} AS b ON a.sid=b.sid WHERE a.tid = %d AND b.sale_type = '%s'";
      $result = db_query($sql, $key, $sale_rule);
      $data = db_fetch_object($result);

      if (!empty($data) && $data->sid != arg(5)) { //arg 4 is the sid
        //find category name
        $cat = db_fetch_object(db_query("SELECT name FROM {term_data} WHERE tid=%d", $data->tid));

        $publication = _classifieds_sales_get_publication_name($data->apply_to);

        //this category has an attribute of this type, so set a form error...
        $cat_error .= 'The category "' . $cat->name . '" already has a sale rule of this type assigned to it (<a href="' . url('admin/classifieds/sales/rules/' . $sale_rule . '/' . $data->sid) . '">' . $data->name . '</a>) for ' . $publication . ' ads.<br />';
      }
    }

    //now that we ran through all the cats selected, see if it has built an error message
    if (isset($cat_error)) {
      form_set_error('list', $cat_error);
    }
  }


  //VALIDATE ANY FIELDS THE RULES ADDED...
  //given the attribute type, include the proper file
  require_once(dirname(__FILE__) . '/sales/sales.' . $sale_rule . '.inc');

  //given the attribute type, run the proper function to
  //include it's specific form attributes
  call_user_func('classifieds_sales_' . $sale_rule . '_add_form_validate', $form, $form_state);
}

/**
 * Add Sales Rule Form Submit
 */
function classifieds_sales_add_rule_form_submit($form, &$form_state) {

  //check to see if the delete button was clicked
  if ($form_state['clicked_button']['#value'] == $form_state['values']['submit_delete']) {

    //set the variables, so the form rebuilds a confirmation message
    $form_state['rebuild'] = TRUE;
    $form_state['storage']['delete'] = TRUE;
  }

  //check to see if the confirm delete button was clicked
  elseif ($form_state['clicked_button']['#value'] == $form_state['values']['submit_delete_confirm']) {

    //delete the item
    $sale_type = $form_state['values']['sale_type'];

    //call attribute function delete
    require_once(dirname(__FILE__) . '/sales/sales.' . $sale_type . '.inc');
    call_user_func('classifieds_sales_' . $sale_type . '_delete', $form_state['values']['sale_id']);

    //delete call categories associated with this attribute
    $sql = "DELETE FROM {classifieds_rules2cat} WHERE sid = %d";
    db_query($sql, $form_state['values']['sid']);

    //finally, delete the attribute itself
    $sql = "DELETE FROM {classifieds_sale_rules} WHERE sid = %d";
    db_query($sql, $form_state['values']['sid']);

    //message
    drupal_set_message('Sale Rule has been deleted.');

    //redirect the user
    $form_state['redirect'] = 'admin/classifieds/sales';

    //delete this so drupal knows we need to submit the form
    unset($form_state['storage']);
  }

  //user this one for the submit button...
  elseif ($form_state['clicked_button']['#value'] == $form_state['values']['submit']) {

    //do normal save here...
    drupal_set_message('Sale Rule Saved.');

    //we must save the selected attribute first
    $sale_rule = $form['type']['#value'];

    //given the attribute type, include the proper file
    require_once(dirname(__FILE__) . '/sales/sales.' . $sale_rule . '.inc');

    //given the attribute type, run the proper function to
    //include it's specific form attributes
    $sale_id = call_user_func('classifieds_sales_' . $sale_rule . '_add_form_submit', $form, $form_state);

    //now we must save the general attribute data
    //CHECK TO SEE IF THIS IS AN INSERT OR AND UPDATE(EDIT) using the update hidden field containt the aid....
    if (is_numeric($form['update']['#value'])) {

      // this is an edit form so update
      db_query("UPDATE {classifieds_sale_rules} SET name='%s' WHERE sid = %d", $form['name']['#value'], $form['update']['#value']);

      $sid = $form['update']['#value'];
    }
    else {
      // this is a new form
      db_query('INSERT INTO {classifieds_sale_rules} (sale_id,sale_type,name,online) VALUES(%d,"%s","%s",%d)', $sale_id, $sale_rule, $form['name']['#value'], $form['online']['#value']);

      //figure out the id number for the last record saved
      $sid = db_last_insert_id('classifieds_sales', 'sid');
    }

    //if default has been checked, or this attribute already exists and is being edited, then we must
    //delete all old categorys associated with this aid
    if (is_numeric($form['update']['#value']) || $form['apply_to']['default']['#value'][0] == '1') {
      //delete all categorys associated with the aid
      db_query('DELETE FROM {classifieds_rules2cat} WHERE sid=%d', $sid);
    }

    //finally we must save the category data
    if ($form['apply_to']['default']['#value'][1]) {
      //default is checked... only one record to save
      db_query('INSERT INTO {classifieds_rules2cat} (sid,tid) VALUES(%d,%d)', $sid, 0);
    }
    else {
      //default is not checked.... sort through the fields...
      //loop through all the fields that are checked....
      foreach ($form['apply_to']['fields']['list']['#value'] as $key => $value) {

        //the key and the value are both the tid that this value is to be applied to....
        db_query('INSERT INTO {classifieds_rules2cat} (sid,tid) VALUES(%d,%d)', $sid, $value);
      }
    }

    //We must do this or the form will rebuild instead of refreshing.
    unset($form_state['storage']);
  }
}

/**
 * Returns true of false whether a attribute type already
 * has a default value or not.
 */
function _classifieds_sales_check_default($sale_rule, $sid) {

  if (substr($sale_rule, 0, 6) != 'print_') { //print stuff has it's own check defaults...
    //build sql statment to query for a default (tid=0) already saved in our tables
    $sql = "SELECT * FROM {classifieds_rules2cat} AS a JOIN {classifieds_sale_rules} AS b ON a.sid = b.sid WHERE
    a.tid = 0 AND b.sale_type ='%s'";

    // run query
    $result = db_query($sql, $sale_rule);

    //execute query
    $data = db_fetch_object($result);

    //return
    if ($data == FALSE) {
      //default has not been set
      return FALSE;
    }
    elseif ($data->sid == $sid) {
      //we are editing the default... so dont trigger the warning
      return FALSE;
    }
    else {
      //default has already been set
      return TRUE;
    }
  }
}
