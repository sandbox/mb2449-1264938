<?php

/**
 * @file
 * Sale rule: Print Ad (Base Price)
 */
function classifieds_sales_print_base_add_form(&$form) {

  //check to see if the update field is here... if so modify the form.
  if (is_numeric(arg(5))) {
    //arg(5) for is an sid... use the sid to select your sale_id
    //look up days
    $sql = "SELECT * FROM {classifieds_sales_print_base} AS a JOIN {classifieds_sale_rules} as b ON a.sale_id=b.sale_id WHERE b.sid = %d";
    $result = db_query($sql, arg(5));
    $data = db_fetch_object($result); //this var will hold the default values...
    //set hidden field
    $form['update_base'] = array(
      '#type' => 'hidden',
      '#value' => $data->sale_id,
    );
  }

  //
  $form['price'] = array(
    '#title' => t('Price'),
    '#type' => 'textfield',
    '#description' => t('Enter the starting price to insert an ad. Enter 0 for FREE, or a price (decimals allowed).'),
    '#default_value' => isset($data->price) ? $data->price : 0,
    '#maxlength' => 6,
    '#required' => TRUE,
    '#field_prefix' => '$',
    '#size' => 4,
  );
}

/**
 * Validate the elements added to the form
 */
function classifieds_sales_print_base_add_form_validate($form, $form_state) {

  //make sure that this number is numeric
  if (!is_numeric($form_state['values']['price'])) {
    form_set_error('price', t('"Price" must be a number.'));
  }

  //make sure this number is within an acceptable range
  if ($form_state['values']['price'] < 0 || $form_state['values']['price'] > 100) {
    form_set_error('price', t('"Price" must be $0 (FREE) or greater, but less then $100'));
  }
}

/**
 * Submit the elements added to the form
 *
 * this has to save the form information, and return the id
 */
function classifieds_sales_print_base_add_form_submit($form, $form_state) {

  //drupal_set_message(dpm($form));
  //check to see if we should insert or update(for edit forms)
  if (is_numeric($form['update_base']['#value'])) {

    //insert into database
    db_query('UPDATE {classifieds_sales_print_base} SET price=%f WHERE sale_id=%d', $form['price']['#value'], $form['update_base']['#value']);

    //return id
    $new_id = $form['update_base']['#value'];
  }
  else {
    //insert into database
    db_query('INSERT INTO {classifieds_sales_print_base} (price) VALUES(%f)', $form['price']['#value']);

    //get id
    $new_id = db_last_insert_id('classifieds_sales_print_base', 'sale_id');
  }

  //return attrib_id
  return $new_id;
}

/**
 * Delete a base sale rule
 */
function classifieds_sales_print_base_delete($sale_id) {
  $sql = "DELETE FROM {classifieds_sales_print_base} WHERE sale_id = %d";
  db_query($sql, $sale_id);
}

function classifieds_sales_print_base_is_online() {
  return FALSE;
}