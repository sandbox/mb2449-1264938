<?php
/**
 * @file
 * Cant remember what this does... something with views
 */
class classifieds_views_time_remaining_handler_field extends views_handler_field_date {

  function render($values) {
    $value = $values->classifieds_online_ads_expiration;
    $time_now = time();
    $delta = $value ? $value - $time_now : 0;
    return format_interval($delta);
  }

}