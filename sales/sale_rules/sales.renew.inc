<?php

/**
 * @file
 * Sale rule: Renew
 */
function classifieds_sales_renew_add_form(&$form) {

  //check to see if the update field is here... if so modify the form.
  if (is_numeric(arg(5))) {
    //arg(5) for is an sid... use the sid to select your sale_id
    //look up days
    $sql = "SELECT * FROM {classifieds_sales_renew} AS a JOIN {classifieds_sale_rules} as b ON a.sale_id=b.sale_id WHERE b.sid = %d";
    $result = db_query($sql, arg(5));
    $data = db_fetch_object($result); //this var will hold the default values...
    //set hidden field
    $form['update_renew'] = array(
      '#type' => 'hidden',
      '#value' => $data->sale_id,
    );
  }


  $form['percent'] = array(
    '#title' => t('Percent of Normal Rate'),
    '#type' => 'textfield',
    '#description' => t('Enter the percentage amount of the normal ad inseration rate (1=Normal price, .75=25% discount, 0=free renewal).'),
    '#default_value' => isset($data->percent) ? $data->percent : '1.00',
    '#maxlength' => 4,
    '#required' => TRUE,
    '#field_suffix' => '(0.00 -> 1.00)',
    '#size' => 3,
  );
}

/**
 * Validate the elements added to the form
 */
function classifieds_sales_renew_add_form_validate($form, $form_state) {

  //make sure that this number is numeric
  if (!is_numeric($form_state['values']['percent'])) {
    form_set_error('percent', t('"Percent of Normal Rate" must be a number.'));
  }

  //make sure this number is within an acceptable range
  if ($form_state['values']['percent'] < 0 || $form_state['values']['percent'] > 1.00) {
    form_set_error('percent', t('"Percent of Normal Rate" must be between 0.00 and 1.00'));
  }
}

/**
 * Submit the elements added to the form
 *
 * this has to save the form information, and return the id
 */
function classifieds_sales_renew_add_form_submit($form, $form_state) {

  //drupal_set_message(dpm($form));
  //check to see if we should insert or update(for edit forms)
  if (is_numeric($form['update_renew']['#value'])) {

    //insert into database
    db_query('UPDATE {classifieds_sales_renew} SET percent=%f WHERE sale_id=%d', $form['percent']['#value'], $form['update_renew']['#value']);

    //return id
    $new_id = $form['update_renew']['#value'];
  }
  else {
    //insert into database
    db_query('INSERT INTO {classifieds_sales_renew} (percent) VALUES(%f)', $form['percent']['#value']);

    //get id
    $new_id = db_last_insert_id('classifieds_sales_renew', 'sale_id');
  }

  //return attrib_id
  return $new_id;
}

/**
 * Delete a base sale rule
 */
function classifieds_sales_renew_delete($sale_id) {
  $sql = "DELETE FROM {classifieds_sales_renew} WHERE sale_id = %d";
  db_query($sql, $sale_id);
}

/**
 * standard functions that define what the rule affects (or is it effects?)
 */
function classifieds_sales_renew_is_online() {
  return TRUE;
}