<?php

/**
 * @file
 * This is the reply form that gets called by the view ad 
 * template file
 */

/**
 * Classifieds reply form that is displayed on nodes
 * 
 * TODO: maybe move this somewhere more suitable...?
 */
function classifieds_reply_form($form_state, $nid, $title) {

  //save the nid so we know who to send the email too...
  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $nid,
  );

  $form['title'] = array(
    '#type' => 'hidden',
    '#value' => $title,
  );

  $form['email'] = array(
    '#title' => t('Your E-mail'),
    '#type' => 'textfield',
    '#maxlength' => 60,
    '#default_value' => $form_state['values']['email'],
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['message'] = array(
    '#title' => t('Message'),
    '#type' => 'textarea',
    '#default_value' => $form_state['values']['message'],
    '#cols' => 40,
    '#rows' => 3,
    '#resizable' => FALSE,
    '#required' => TRUE,
  );

  $form['copy'] = array(
    '#type' => 'checkboxes',
    '#options' => array('copy' => 'Send me a copy'),
  );

  if (isset($form_state['values']['copy'])) {
    $form['copy']['#default_value'] = $form_state['values']['copy'];
  }

  $form['captcha'] = array(
    '#type' => 'captcha',
    '#captcha_type' => 'image_captcha/Image',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
    '#ahah' => array(
      'event' => 'click',
      'path' => 'classifieds/reply',
      'wrapper' => 'classifieds-reply'
    )
  );

  return $form;
}

/**
 * Classifieds reply form that is displayed on nodes
 * Validation Handler
 */
function classifieds_reply_form_validate($form, &$form_state) {

  //check to make sure we have a valid email...
  if (!valid_email_address($form_state['values']['email'])) {
    form_set_error('mail', t('You must enter a valid e-mail address.'));
  }
}

/**
 * This function actually sends out the email, when a user
 * clicks send on a classified node reply form, and returns
 * an success string...
 */
function classifieds_reply_send_email($form_state) {

  //debug
  //drupal_set_message(dpm($form_state));
  //pull out vars form form_state
  $email_buyer = $form_state['values']['email'];
  $message = $form_state['values']['message'];
  $title = $form_state['values']['title'];

  //look up the ad owner's email
  $nid = $form_state['values']['nid'];
  $sql = "SELECT field_email_email FROM {content_field_email} AS a WHERE a.nid = %d";
  $email_seller = db_result(db_query($sql, $nid));

  //build parameters
  $params = array(
    'message' => $message,
    'replyto' => $email_buyer,
    'title' => $title,
  );

  //send the e-mail out
  drupal_mail('classifieds', 'reply', $email_seller, language_default(), $params);

  //send out a copy?
  if ($form_state['values']['copy']['copy'] != FALSE) {
    drupal_mail('classifieds', 'reply', $email_buyer, language_default(), $params);
  }
}
