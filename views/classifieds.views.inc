<?php

/**
 * This was originally based off the classifieds views file
 * for the ed_classifieds...
 *
 *
 * @file
 * Simple text-based classified ads module.
 * Autho: Michael Curry, Exodus Development, Inc.  * exodusdev@gmail.com
 * for more information, please visit http://exodusdev.com/drupal/modules/classified.module
 */
/* ================= views.module integration ===================== */

function classifieds_views_handlers() {

  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'classifieds') . '/views/handlers',
    ),
    'handlers' => array(
      // field handlers
      'classifieds_views_time_remaining_handler_field' => array(
        'parent' => 'views_handler_field_date'
      ),
      'classifieds_views_parent_category_handler_field' => array(
        'parent' => 'views_handler_field'
      ),
      'classifieds_views_child_category_handler_field' => array(
        'parent' => 'views_handler_field'
      ),
      'classifieds_views_seller_handler_field' => array(
        'parent' => 'views_handler_field'
      ),
      'classifieds_views_price_handler_field' => array(
        'parent' => 'views_handler_field'
      ),
      //filter handlers
      'classifieds_views_handler_filter_seller' => array(
        'parent' => 'views_handler_filter'
      ),
      'classifieds_views_handler_filter_price' => array(
        'parent' => 'views_handler_filter'
      ),
      'classifieds_views_handler_filter_third_cat' => array(
        'parent' => 'views_handler_filter'
      ),
    ),
  );
}

/*
 * Implements hook_views_data()
 * No need to make it a base, doesn't have sufficient first class data
 * as yet, really just dependent upon Node.
 */

function classifieds_views_data() {
  //setup copied over
  $name = 'Classified Ad';
  $parms = array('@name' => $name, '!name' => $name, '%name' => $name);

  //copy and pasted 
  $parms['!modulename'] = 'classifieds';
  $data = array();
  // All classified ads are in a single table group
  $data['classifieds_online_ads']['table']['group'] = t('Classified Ads');
  // Everything else is in a Node
  $data['classifieds_online_ads']['table']['join'] = array(
    // Tell Views that we can join with node 
    // ed_classifieds used vid(for version id, for node revisions)
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid'
      ));
  // 'expires_on'
  $data['classifieds_online_ads']['expiration'] = array(
    'title' => t('Expires On'),
    // The help that appears on the UI
    'help' => t('Classified ad expiry date.'),
    // Information for displaying
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE
    ),
    // Information for accepting as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_date',
      'name field' => 'title',
      'numeric' => TRUE,
    ),
    // Information for accepting as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_date'
    ),
    // Information for sorting
    'sort' => array(
      'handler' => 'views_handler_sort_date'
    )
  );
  // 'expiry_remaining' time remaining is an alias of 'expires_on'
  // but displayed differently
  $data['classifieds_online_ads']['expiry_remaining'] = array(
    'real field' => 'expiration',
    'title' => t('Expiry time remaining'),
    // The help that appears on the UI
    'help' => t('Time remaining until expiry.'),
    // Information for displaying
    'field' => array(
      'handler' => 'classifieds_views_time_remaining_handler_field',
      'click sortable' => TRUE
    ),
    // Information for accepting as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_date',
      'name field' => 'title',
      'numeric' => TRUE,
    ),
    // Information for accepting as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_date'
    ),
    // Information for sorting
    'sort' => array(
      'handler' => 'views_handler_sort_date'
    )
  );

  // Classified Ad: Address
  $data['classifieds_online_ads']['address'] = array(
    'title' => t('Address'),
    // The help that appears on the UI
    'help' => t('The street address for the classified ad'),
    // Information for displaying
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE
    ),
    // Information for accepting as an argument
    'argument' => array(
      'handler' => 'views_handler_argument',
      'name field' => 'title',
      'numeric' => TRUE,
    ),
    // Information for accepting as a filter
    'filter' => array(
      'handler' => 'views_handler_filter'
    ),
    // Information for sorting
    'sort' => array(
      'handler' => 'views_handler_sort'
    )
  );

  // Classified Ad: City
  $data['classifieds_online_ads']['city'] = array(
    'title' => t('City'),
    // The help that appears on the UI
    'help' => t('The city for the classified ad'),
    // Information for displaying
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE
    ),
    // Information for accepting as an argument
    'argument' => array(
      'handler' => 'views_handler_argument',
      'name field' => 'title',
      'numeric' => TRUE,
    ),
    // Information for accepting as a filter
    'filter' => array(
      'handler' => 'views_handler_filter'
    ),
    // Information for sorting
    'sort' => array(
      'handler' => 'views_handler_sort'
    )
  );

  // Classified Ad: State
  $data['classifieds_online_ads']['state'] = array(
    'title' => t('State'),
    // The help that appears on the UI
    'help' => t('The state for the classified ad'),
    // Information for displaying
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE
    ),
    // Information for accepting as an argument
    'argument' => array(
      'handler' => 'views_handler_argument',
      'name field' => 'title',
      'numeric' => TRUE,
    ),
    // Information for accepting as a filter
    'filter' => array(
      'handler' => 'views_handler_filter'
    ),
    // Information for sorting
    'sort' => array(
      'handler' => 'views_handler_sort'
    )
  );
  //Classified Ad: Parent Category
  $data['classifieds_online_ads']['parent'] = array(
    'field' => array(
      'real field' => 'nid',
      'title' => t('Parent Category'),
      'help' => t('The parent taxonomy term name associated with this ad.'),
      'handler' => 'classifieds_views_parent_category_handler_field',
    ),
  );
  //Classified Ad: Child Category
  $data['classifieds_online_ads']['child'] = array(
    'field' => array(
      'real field' => 'nid',
      'title' => t('Child Category'),
      'help' => t('The child taxonomy term name associated with this ad.'),
      'handler' => 'classifieds_views_child_category_handler_field',
    ),
  );

  //Classified Ad: Price
  $data['classifieds_online_ads']['price'] = array(
    'title' => t('Price'),
    // The help that appears on the UI
    'help' => t('Classified ad price.'),
    // Information for displaying
    'field' => array(
      'handler' => 'classifieds_views_price_handler_field',
      'click sortable' => TRUE
    ),
    // Information for accepting as an argument
    'argument' => array(
      'handler' => 'views_handler_argument',
      'name field' => 'title',
      'numeric' => TRUE,
    ),
    // Information for accepting as a filter
    'filter' => array(
      'handler' => 'classifieds_views_handler_filter_price',
    ),
    // Information for sorting
    'sort' => array(
      'handler' => 'views_handler_sort'
    )
  );

  //Classified Ad: Seller
  $data['classifieds_online_ads']['seller'] = array(
    'title' => t('Seller'),
    'real field' => 'nid',
    // The help that appears on the UI
    'help' => t('Classified ad seller.'),
    // Information for displaying
    'field' => array(
      'handler' => 'classifieds_views_seller_handler_field',
      'click sortable' => TRUE
    ),
    // Information for accepting as an argument
    'argument' => array(
      'handler' => 'views_handler_argument',
      'name field' => 'seller',
      'numeric' => TRUE,
    ),
    // Information for accepting as a filter
    'filter' => array(
      'handler' => 'classifieds_views_handler_filter_seller'
    ),
    // Information for sorting
    'sort' => array(
      'handler' => 'views_handler_sort'
    )
  );


  //Classified Ad: Third Category
  $data['classifieds_online_ads']['third_cat'] = array(
    'title' => t('Third Category'),
    'real field' => 'nid',
    // The help that appears on the UI
    'help' => t('Classified ad third category.'),
    // Information for displaying
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE
    ),
    // Information for accepting as an argument
    'argument' => array(
      'handler' => 'views_handler_argument',
      'name field' => 'seller',
      'numeric' => TRUE,
    ),
    // Information for accepting as a filter
    'filter' => array(
      'handler' => 'classifieds_views_handler_filter_third_cat'
    ),
    // Information for sorting
    'sort' => array(
      'handler' => 'views_handler_sort'
    )
  );
  return $data;
}

/* ================= views.module integration END  ===================== */
