<?php

/**
 * @file
 * Contains the logic for building the detail fields in the create ad form
 */
/*
 * Thic object is responsible for building, holding, and returning what
 * detail fields are to be shown on the create ad page
 */

class detailFields {

  /**
   * VARS
   */
  private $childTID;
  private $parentTID;
  private $defaultValues;

  /**
   * CONSTRUCTOR
   */
  public function __construct($child_tid, $parent_tid, $defaultValues) {

    $this->childTID = $child_tid;
    $this->parentTID = $parent_tid;
    $this->defaultValues = $defaultValues;
  }

  /**
   * buildRules - Retrieves rules from database and assembles the final
   * set of rules that will apply to this category
   */
  private function getDetailFields($third_cat=NULL) {

    //TODO: enable caching at this spot....
    //build array to hold rules
    $rules = array();
    $chosen_rule = array();

    //find all rules that match the parent_tid
    $sql = "SELECT * FROM {classifieds_node_field} AS a WHERE a.parent_tid = %d";
    $result = db_query($sql, $this->parentTID);

    while (($data = db_fetch_object($result)) !== FALSE) {
      $rules[$data->id] = $data;
    }


    //Determine if we use the third cat or regular child
    if (isset($third_cat)) {

      $key = $third_cat;

      foreach ($rules as $id => $data) {

        //find the rule...
        $child_tids = unserialize($data->child_tids);

        if (in_array($key, $child_tids)) {
          $chosen_rule = $data;
          break;
        }
      }
    }


    //look for child tid rule if third cat isnt applicable, or there is no third cat fields filled in
    if (empty($chosen_rule)) {

      $key = $this->childTID;

      foreach ($rules as $id => $data) {

        //find the rule...
        $child_tids = unserialize($data->child_tids);

        if (in_array($key, $child_tids)) {
          $chosen_rule = $data;
          break;
        }
      }
    }

    //return fields
    return unserialize($chosen_rule->fields);
  }

  /**
   * Modify form variable according to what fields should be shown
   * @param type $form 
   */
  public function modifyAd(&$form, &$form_state) {

    //figure out if this has three cats, or two...
    $child_tree = taxonomy_get_tree(_classifieds_get_vid(), $this->childTID, -1, 1);

    //if we have a 3rd cat
    if (count($child_tree) > 0) { //we have 3 cats
      //see if we have a value for the third tid
      if (isset($form_state['values']['field_sub_cat_menu'])) {
        $third_cat = $form_state['values']['field_sub_cat_menu'];
      }

      //figure out what fields to show
      $fields = $this->getDetailFields($third_cat);

      //hide extra fields
      $this->hideDetailFields($form, $fields, $third_cat);

      //build third cat selector
      $this->build3rdSelect($form, $form_state);
    }
    else { //we have 2 cats
      //figure out what fields to show
      $fields = $this->getDetailFields();

      //hide extra fields
      $this->hideDetailFields($form, $fields);
    }
  }

  ////////////////////////HELPER FUNCS/////////////////////////////

  /**
   * This builds the selector drop down for a third category
   */
  private function build3rdSelect(&$form, &$form_state) {

    //figure out what the children are...
    $children = taxonomy_get_children($this->childTID);

    //build drop down options
    $options = array();

    //include the none option
    $options[$tid] = '- None -';

    foreach ($children as $c) {
      $options[$c->tid] = $c->name;
    }

    //load default values...
    if (isset($form_state['values']['field_sub_cat_menu'])) {
      $default_value = $form_state['values']['field_sub_cat_menu'];
    }


    //build drop down
    $form['field_sub_cat_menu'] = array(
      '#title' => t('Type of ' . $form['c_sub_cat']['#description']),
      '#type' => 'select',
      '#options' => $options,
      '#multiple' => FALSE,
      '#default_value' => isset($default_value) ? $default_value : '',
      '#ahah' => array(
        'event' => 'change',
        'path' => 'classifieds/adjust_details_js',
        'wrapper' => 'classifieds-details-wrapper',
      //'effect' => 'fade',
      ),
      '#required' => TRUE,
    );

    //add default value if this is an edit form
    if ($form_state['values']['first_edit_load'] == TRUE) {
      $form['field_sub_cat_menu']['#default_value'] = $form['#node']->field_sub_cat_menu;
    }

    //load details default values
    //classifieds_create_ad_details_default($form, $form_state);     
    $this->fillInDefaults($form);
  }

  /**
   * Hide the fields
   */
  private function hideDetailFields(&$form, $fields) {

    //only perform this action if there are fields to hide!
    if (!empty($fields)) {

      //find forbidden fields...
      module_load_include('inc', 'classifieds', 'includes/classifieds.admin');
      $forbidden_fields = classifieds_admin_node_fields_get_forbidden();

      //go through form items and ditch the fields we dont want
      foreach ($form as $name => $data) {

        if (substr($name, 0, 6) == "field_") {

          if (!in_array($name, $fields) && !in_array($name, $forbidden_fields)) {
            //$form[$name]['#access'] = FALSE;
            unset($form[$name]);
          }
        }
      }
    }
  }

  //THIS ISNT BEING USED YET.... MUST REFACTOR THIS BIT OF CODE STILL...
  private function fillInDefaults(&$form) {

    $default_value = $this->defaultValues;

    //find out what fields are forbidden
    module_load_include('inc', 'classifieds', 'includes/classifieds.admin');
    $forbidden_fields = classifieds_admin_node_fields_get_forbidden();

    //fields whose default values are set using arrays
    $detail_type_defaults = array('optionwidgets_buttons', 'optionwidgets_select', 'select');

    //go through the whole form, and add the default value
    //for all the checkboxes... for some reason this is broke
    //maybe its the BETTER VIEWS module?
    foreach ($form as $key => $data) {

      if (substr($key, 0, 6) == 'field_'
          && !in_array($key, $forbidden_fields)
      ) {
        if (isset($default_value[$key])) {
          if (in_array($form[$key]['#type'], $detail_type_defaults)) {
            $form[$key]['#default_value'] = $default_value[$key];
          }
          else {
            $form[$key][0]['#default_value']['value'] = $default_value[$key][0]['value'];
          }
        }
      }
    }
  }

}
