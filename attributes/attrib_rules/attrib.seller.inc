<?php
/**
 * @file
 * Image Attribute Item
 */
class sellerAttribute extends itemAttribute {

  /**
   * buildAd
   */
  public function buildAd(&$form, $form_state) {

    //refactor snippet
    if (!isset($this->attributes['seller'])) {
      //no image field attribute set, so hide the field_image element.
      //unset($form['field_image']);
      dpm('no seller stuff... dont do build Ad');
    }


    //look up the attribs properties
    $data = $this->getData($this->attributes[$this->name]);

    //check to see if we can show this
    if ($data->hide != 1) {

      //save default value 
      $default_value = $form_state['storage']['values'][1];

      //turn field_seller into a normal radio form item
      $form['seller'] = array(
        '#type' => 'radios',
        '#title' => t('For Sale By'),
        '#options' => array(
          0 => t('Private Party'),
          1 => $data->title,
        ),
        '#default_value' => isset($default_value['seller']) ? $default_value['seller'] : 0,
        '#ahah' => array(
          'event' => 'change',
          'path' => 'classifieds/js/step1',
          'wrapper' => 'classifieds-price-wrapper2',
          'effect' => 'fade',
        ),
      );

      //check to see if this is an edit form
      if ($this->isFirstEdit) {
        $form['seller']['#default_value'] = $form['#node']->seller;
      }
    }
  }

  /**
   * searchFilter -
   * modifieds the search filter block according to images....
   */
  public function searchFilter(&$form, $form_state) {

    //look up the attribs properties
    $data = $this->getData($this->attributes[$this->name]);

    //change 'commericial' name for seller type drop down
    $form['seller']['#options']['commercial'] = $data->title;

    //check to see if we hide this
    if ($data->hide == 1) {
      //unset field_seller
      $form['seller']['#disabled'] = TRUE;
      //unset($form['seller']);
    }
  }

  ///////////////HELPER FUNCTIONS///////////////////////////
}

////////////////ADMIN FUNCTIONS////////////////////////

/**
 * The elements that this attribute will add to the
 * create attribute admin form.
 */
function classifieds_attrib_seller_form() {

  //check to see if the update field is here... if so modify the form.
  if (is_numeric(arg(5))) {
    //arg(4) for is an aid... use the aid to select your attrib_id
    //look up days
    $sql = "SELECT * FROM {classifieds_attrib_seller} AS a JOIN {classifieds_attributes} as b ON a.attrib_id=b.attrib_id WHERE b.aid = %d";

    $result = db_query($sql, arg(5));

    $data = db_fetch_object($result); //this var will hold the default values...

    $hide = $data->hide;
    $title = $data->title;

    //drupal_set_message('hide is'.$hide);
    //set hidden field
    $form['update_seller'] = array(
      '#type' => 'hidden',
      '#value' => $data->attrib_id,
    );
  }
//drupal_set_message(dpm($form));
  //drupal_set_message('the attrib id'.$data->attrib_id);

  $form['seller'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#description' => t('Enter the title to be display for a commercial entity, i.e.: Dealer,Realtor,Commercial'),
    '#default_value' => $title,
    '#maxlength' => 25,
    '#required' => TRUE,
    '#size' => 20,
  );

  $form['seller_hide'] = array(
    '#type' => 'checkboxes',
    '#default_value' => array($hide),
    '#options' => array(1 => 'Do not display the "For Sale By" field'),
  );
  return $form;
}

/**
 * Validate the elements added to the form
 */
function classifieds_attrib_seller_form_validate($form, $form_state) {
  //no validaiton needed
}

/**
 * Submit the elements added to the form
 *
 * this has to save the form information, and return the id
 */
function classifieds_attrib_seller_form_submit($form, $form_state) {

  //make sure the hide value has a value
  if (!isset($form['seller_hide']['#value'][1])) {
    $form['seller_hide']['#value'][1] = 0;
  }

  //check to see if we should insert or update(for edit forms)
  if (is_numeric($form['update_seller']['#value'])) {

    //insert into database
    db_query('UPDATE {classifieds_attrib_seller} SET title="%s", hide=%d WHERE attrib_id=%d', $form['seller']['#value'], $form['seller_hide']['#value'][1], $form['update_seller']['#value']);

    //return id
    $new_id = $form['update_seller']['#value'];
  }
  else {
    drupal_set_message($form['seller_hide']['#value']);

    //insert into database
    db_query('INSERT INTO {classifieds_attrib_seller} (title,hide) VALUES(%d,"%s")', $form['seller']['#value'], $form['seller_hide']['#value'][1]);

    //get id
    $new_id = db_last_insert_id('classifieds_attrib_seller', 'attrib_id');
  }

  //return attrib_id
  return $new_id;
}

/**
 * print out information for a seller attribute given a attrib_id
 */
function _classifieds_attrib_seller_print($attrib_id) {

  //lookup the number of days...
  $sql = "SELECT * FROM {classifieds_attrib_seller} AS a WHERE a.attrib_id = %d";

  $result = db_query($sql, $attrib_id);

  $data = db_fetch_object($result);

  //format results
  $output = array(
    'name' => 'Seller',
    'desc' => '<li><em>Field Title:</em> ' . $data->title . '</li>',
  );

  if ($data->hide == 1) {
    $output['desc'] .= '<li><em>Hidden:</em> YES</li>';
  }
  else {
    $output['desc'] .= '<li><em>Hidden:</em> No</li>';
  }

  //return
  return $output;
}

/**
 * deletes this attibute type
 */
function classifieds_attrib_seller_delete($attrib_id) {

  $sql = "DELETE FROM {classifieds_attrib_seller} WHERE attrib_id = %d";

  db_query($sql, $attrib_id);
}

/**
 * builds a list of values for type of seller - 
 * we have to do it this backwards way since optionswidget doesnt
 * build the options to the seller type form until after this runs...
 * so our create ad form commercial seller title wont match unless 
 * it works this way
 */
//I THINK THIS IS GARBAGE!
/*function classifieds_attrib_seller_build_values(){
  
  //check to see if a session variable has been set
  if($_SESSION['classifieds_seller_title']){
    
    $com = $_SESSION['classifieds_seller_title'];
    
    unset($_SESSION['classifieds_seller_title']);
    
  } else {
    $com = 'Commercial';
  }
  
  $values = array(0 => 'Private Party', 1 => $com);
  
  return $values;
  
}*/


/**
 * Given a seller type (0=private party 1=commericial) and a tid,
 * This function will figure out what the commericial label should
 * be (Commericial, Dealer, etc)
 */
/*function classifieds_attrib_seller_find_commercial_label($parent_tid,$child_tid){

    //get the attributes...
    $attributes = classifieds_attrib_get_attributes($parent_tid, $child_tid);

    $label = $attributes['seller']['title'];

    return $label;

}*/