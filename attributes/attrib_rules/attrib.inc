<?php
/**
 * @file
 * This is a abstract class is used to define attribute types that 
 * can access certain functions
 */

abstract class itemAttribute {

  /**
   * VARS
   */
  protected $name; //holds the name of the item
  protected $attributes; //holdes the attributes...
  //vars from build ad- read only, used for form building purposes
  protected $classifiedData; //read only copy of the classifiedData...
  protected $defaultValues;
  protected $isFirstEdit;
  protected $isFirstLoad;

  /**
   * FORCED FUNCTIONS
   */
  abstract protected function buildAd(&$form, $form_state);

  abstract protected function searchFilter(&$form, $form_state);

  /**
   * Constructor 
   * @param type $attributes 
   */
  public function __construct(&$attributes, $classifiedData, $defaultValues=NULL, $isFirstEdit=NULL, $isFirstLoad=NULL) {

    //load input attributes array
    $this->attributes = $attributes;

    //load name
    $this->name = str_replace('Attribute', '', get_class($this));

    //load build ad stuff
    $this->classifiedData = $classifiedData;
    $this->defaultValues = $defaultValues;
    $this->isFirstEdit = $isFirstEdit;
    $this->isFirstLoad = $isFirstLoad;
  }

  /**
   * getData() 
   *  Loads the data from the table
   */
  protected function getData($id) {

    $sql = "SELECT * FROM {classifieds_attrib_" . $this->name . "} WHERE attrib_id = %d";
    $result = db_query($sql, $id);

    $data = db_fetch_object($result);

    return $data;
  }

}
