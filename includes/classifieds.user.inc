<?php

/**
 * @file
 * Contains the users control panel
 */

/**
 * Page callback - For 'classifieds' tab under /user menu
 *
 * NOTE: $uid does not come through, unless your an admin
 */
function classifieds_user_page($uid) {

  //get user id
  global $user;

  //add a colspan...$row[] = array('data' => 'b', 'colspan' => 2);
  $headers = array(
    //array('data' => t('UID'), 'field' => 'uid', 'sort'=> 'desc'),
    //array('data' => t('NID'), 'field' => 'nid', 'sort'=> 'desc'),
    array('data' => t('Pub. Date'), 'field' => 'created', 'sort' => 'desc'),
    //array('data' => t('Exp. Date'), 'field' => 'created', 'sort'=> 'desc'),
    array('data' => t('Title'), 'field' => 'title'),
    array('data' => t('Last Modified'), 'field' => 'title'),
    //array('data' => t('Expires On'), 'field' => 'title'),
    array('data' => 'Links') //'colspan' => 2
  );



  //we need to only select the nodes created by the user...
  //and only of the content types that are classified ads
  $sql = "SELECT uid,nid,created,title,changed FROM {node} AS a WHERE a.uid = %d AND a.type IN (SELECT type FROM {classifieds_node_per_cat})";
  $sql .= tablesort_sql($headers);

  //run query
  $result = db_query($sql, $user->uid);


  //Only show table if the user has create ads
  if ($result) {

    //set up data array
    $data = ''; //reset data var
    $i = 1;

    //your basic while loop to get the data
    while ($tmp = db_fetch_array($result)) {

      //format the data...
      $tmp['created'] = format_date($tmp['created'], 'custom', 'm/d/Y', '', "en");
      ;

      //add links...
      //= 'something'; //node/59/edit?destination=admin%2Fcontent%2Fnode
      $tmp['link_edit'] = l(t('Edit'), "admin/settings/data/edit/$id") .
          '&nbsp;&nbsp;' .
          l(t('Remove'), "admin/settings/data/remove/$id");


      //save the data
      $data[$i] = $tmp;

      //we dont want to show these fields
      unset($data[$i]['uid']);
      unset($data[$i]['nid']);

      //advance the counter
      $i++;
    }
  }

  //save to output
  $output = theme('table', $headers, $data);

  //PAGINATION
  //$limit = 10;
  //generate a paged query
  //$result = pager_query($sql, $limit);
  //return
  return $output;
}

