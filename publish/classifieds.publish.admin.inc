<?php

/**
 * @file
 * Classifieds Publish: Admin
 * This file contains the administrative functions required to modify
 * publishing settings in the admin panel.
 */

/**
 * page call back
 *
 * Publish page under menu
 */
function classifieds_admin_publish_page() {

  //Print a list of all default attributes
  $output .= '<strong>PUBLISH PAGE</strong><br />';

  return $output;
}

/**
 * This function actually builds the xml file to be downloaded...
 */
function _classifieds_publish_export($date) {

  //add the delivery day on top of it....
  $explode_date = explode('-', $date);
  $date_formatted = date("m_d_y", mktime(0, 0, 0, $explode_date[0], $explode_date[1], $explode_date[2]));

  //numbering variables
  $alphabet = 'A B C D E F G H I J K L M N O P Q R S T U V W X Y Z';
  $alpha_array = explode(' ', $alphabet);

  //set counter
  $i = 0; //counts the number of categories
  //header stuff
  drupal_set_header('Content-type: text/xml');
  drupal_set_header('Content-Disposition: attachment; filename="' . variable_get('classifieds_publish_name', NULL) . '-' . $date_formatted . '.xml"');

  //create array to hold all processing
  $parent_array = array();

  //beging outputting xml...
  echo '<?xml version="1.0" encoding="UTF-8"?>';

  //load parent categories
  $sql = "SELECT * FROM {term_data} AS a JOIN {term_hierarchy} AS b ON a.tid=b.tid WHERE a.vid=%d AND b.parent=0 ORDER BY a.weight";

  //run query
  $result = db_query($sql, _classifieds_get_vid());
  while ($tmp = db_fetch_array($result)) { //loop through parent cats
    //create array to hold the child categories
    $sub_cat = array();

    //reset sub_cat counter
    $j = 1;

    //load all of the child categories then....
    $sql_child = "SELECT * FROM {term_data} AS a JOIN {term_hierarchy} AS b ON a.tid=b.tid WHERE a.vid=%d AND b.parent=%d ORDER BY a.weight";
    $result_child = db_query($sql_child, _classifieds_get_vid(), $tmp['tid']);
    while ($tmp2 = db_fetch_array($result_child)) { //loop through
      $print_ad_sql = '
        FROM {classifieds_print_ads} AS a
        LEFT JOIN {classifieds_print_schedule} AS b ON a.nid=b.nid
        LEFT JOIN {term_node} AS c on a.nid=c.nid
        LEFT JOIN {classifieds_online_ads} AS d on a.nid=d.nid
        WHERE b.run_date LIKE "%s" AND c.tid=%d AND d.censor >= 1';

      //count the number of ads in this child cat
      $ad_count_sql = "SELECT COUNT(*) " . $print_ad_sql;
      $ad_count = db_result(db_query($ad_count_sql, $date, $tmp2['tid']));

      //only add this the subcats array... if there are ads
      if ($ad_count > 0) {
        //create array to hold all this categories ads
        $ad_array = array();

        //load all of the classified print ads that are associated with the current $tmp2['tid']
        $sql3 = "SELECT * " . $print_ad_sql;
        $result3 = db_query($sql3, $date, $tmp2['tid']);
        while ($tmp3 = db_fetch_array($result3)) {

          //get ad info
          $ad_info = array(
            'keycode' => $tmp3['keyword'],
            'byline' => check_plain($tmp3['byline']),
            'text' => check_plain($tmp3['text']),
          );

          //add the ad string to the array
          $ad_array[] = $ad_info;
        }

        //sort the ad array....
        usort($ad_array, '_classifieds_publish_sort_bylines');


        //store sub cat stuff in $sub_cat array
        $sub_cat[] = array(
          'subcat' => check_plain($tmp2['name']) . ' (' . $ad_count . ')',
          'subkey' => $alpha_array[$i] . $j,
          'ads' => $ad_array,
        );

        //increase subcat counter
        $j++;
      }
    }

    //save the  data in the parent array
    $parent_array[] = array(
      'parent' => $tmp['name'],
      'catkey' => $alpha_array[$i],
      'sub_cat' => $sub_cat,
    );

    //advance the category counter
    $i++;
  }


  //start output
  //display the classified ad directory
  //create string to hold information
  $output = '';

  //keep track of number of tcols
  $trows = 0;

  //loop through the parents...
  foreach ($parent_array as $parent) {

    //output the category name
    $output .= '<catkey aid:table="cell" aid:crows="1" aid:ccols="1" aid:ccolwidth="25">' . $parent['catkey'] . '</catkey>';
    $output .= '<category aid:table="cell" aid:crows="1" aid:ccols="1" aid:ccolwidth="145">' . check_plain($parent['parent']) . '</category>';
    $trows++;

    //print out values here- parent cat
    if (count($parent['sub_cat']) > 0) { //we have at least 1 child cat with ads
      //print out subcats
      foreach ($parent['sub_cat'] as $data) {

        //print out sub_cat
        $output .= '<subkey aid:table="cell" aid:crows="1" aid:ccols="1" aid:ccolwidth="25">' . $data['subkey'] . '</subkey>';
        $output .= '<subcat aid:table="cell" aid:crows="1" aid:ccols="1" aid:ccolwidth="145">' . check_plain($data['subcat']) . '</subcat>';
        $trows++;
      }
    }
  }

  //calculate number of tcols...
  echo '<Root>';

  echo '<classifiedDirectory>';
  echo '<classifiedDirectoryTable xmlns:aid="http://ns.adobe.com/AdobeInDesign/4.0/" aid:table="table" aid:trows="' . $trows . '" aid:tcols="2">';

  //print content
  echo $output;

  //include closing tag
  echo '</classifiedDirectoryTable>';
  echo '</classifiedDirectory>';


  //display the classified ads
  //create string to hold information
  $output = '';

  //keep track of number of tcols
  $trows = 0;

  //loop through the parents...
  foreach ($parent_array as $parent) {

    //output the category name
    $output .= '<catkeyHeader aid:table="cell" aid:crows="1" aid:ccols="1" aid:ccolwidth="170">' . check_plain($parent['catkey']) . '</catkeyHeader>';
    $output .= '<categoryHeader aid:table="cell" aid:crows="1" aid:ccols="1" aid:ccolwidth="170">' . check_plain($parent['parent']) . '</categoryHeader>';
    $trows = $trows + 2;

    //print out values here- parent cat
    if (count($parent['sub_cat']) > 0) { //we have at least 1 child cat with ads
      //print out subcats
      foreach ($parent['sub_cat'] as $data) {

        //print out sub_cat
        $output .= '<subkeyHeader aid:table="cell" aid:crows="1" aid:ccols="1" aid:ccolwidth="170">' . $data['subkey'] . '</subkeyHeader>';
        $output .= '<subcatHeader aid:table="cell" aid:crows="1" aid:ccols="1" aid:ccolwidth="170">' . check_plain($data['subcat']) . '</subcatHeader>';
        $trows = $trows + 2;

        //loop through the ads
        foreach ($data['ads'] as $ads) {

          //build the ad, insert the byline, and keycode if they are set
          if ($ads['byline'] != '') {
            $output .= '<byline aid:table="cell" aid:crows="1" aid:ccols="1" aid:ccolwidth="170">' . $ads['byline'] . '</byline>';
            $trows++;
          }

          $output .= '<classified aid:table="cell" aid:crows="1" aid:ccols="1" aid:ccolwidth="170">' . $ads['text'] . '</classified>';
          $trows++;

          if ($ads['keycode'] != '') {
            $output .= '<keycode aid:table="cell" aid:crows="1" aid:ccols="1" aid:ccolwidth="170">' . $ads['keycode'] . '</keycode>';
            $trows++;
          }
        }
      }
    }
    else { //there are no ads for the whole category
      $output .= '<emptyCategory aid:table="cell" aid:crows="1" aid:ccols="1" aid:ccolwidth="170">Sorry, there are no ads posted in this category.</emptyCategory>';
      $trows++;
    }
  }

  //calculate number of tcols...
  echo '<classifiedAds>';
  echo '<classifiedAdsTable xmlns:aid="http://ns.adobe.com/AdobeInDesign/4.0/" aid:table="table" aid:trows="' . $trows . '" aid:tcols="1">';

  //print content
  echo $output;

  //include closing tag
  echo '</classifiedAdsTable>';
  echo '</classifiedAds>';


  //close root
  echo '</Root>';
}

/**
 * For sorting the bylines in xml output
 */
function _classifieds_publish_sort_bylines($a, $b) {

  //return
  $return = 0;

  //sort by byline
  $a = $a['byline'];
  $b = $b['byline'];

  //break up the bylines
  $a_array = explode(" ", $a);
  $b_array = explode(" ", $b);


  //check to see if we are dealing with empty bylines
  if ($a == '' || $b == '') { //we are dealing with empty bylines, so we push these to the back of the list
    if ($a == '' && $b != '') {
      $return = 1;
    }
    elseif ($a != '' | $b == '') {
      $return = -1;
    }
    //else they are both blank, and equal
  }
  else { //they both have bylines, proceed to sort
    //count up the array
    $count = count($a_array);

    //set i
    $i = 0;

    //cycle through bylines - this wills top at line a
    while ($i < $count) {
      //figure
      if (is_numeric($a_array[$i])) { //this is a number
        //vary return based on results
        if ($a_array[$i] > $b_array[$i]) {
          $return = -1;
          break;
        }
        elseif ($a_array[$i] < $b_array[$i]) {
          $return = 1;
          break;
        } //else its equal, so we keep going
      }
      else { //this is alphanumeric
        $strcmp = strcmp($a_array[$i], $b_array[$i]);

        if ($strcmp != 0) {
          $return = $strcmp;
          break;
        }
      }

      //increment
      $i++;
    }
  }

  return $return;
}

/**
 * helper funciton that builds due time
 */
function _classifieds_publish_get_due_time() {

  //get variables
  $hour = variable_get('classifieds_publish_schedule_hour', NULL) + 1;
  $min = variable_get('classifieds_publish_schedule_minutes', NULL);
  $mil = variable_get('classifieds_publish_schedule_mil', NULL);

  //build hour
  if ($mil == 2) {
    $hour = $hour + 12;
  }

  //build minutes
  $min = ($min - 1) * 15;
  if ($min == 0) {
    $min = '00';
  }

  //return
  return $hour . ':' . $min . ':00';
}

/**
 * Export form....
 */
function classifieds_publish_export_form($form_state) {

  //create period run dates
  module_load_include('inc', 'classifieds', '/publish/classifieds.publish');
  $publish = new publish();

  $schedule = $publish->buildScheduleDropDown(4, 3);

  //only print the export form if publish has been set up
  if (!empty($schedule['dates'])) {

    //put dates in order
    $schedule['dates'] = array_reverse($schedule['dates'], TRUE);

    //build form
    $form['period'] = array(
      '#title' => t('Period'),
      '#type' => 'select',
      '#options' => $schedule['dates'],
      '#default_value' => $schedule['key'],
      '#multiple' => FALSE,
    );

    $form['export'] = array(
      '#type' => 'submit',
      '#value' => t('Export'),
    );

    //return
    return $form;
  }
}

/**
 * submit handler
 */
function classifieds_publish_export_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#id'] == 'edit-export') {
    //find date
    $date = $form_state['values']['period'];

    //redirect to export url
    drupal_goto('admin/classifieds/publish/export/' . $date);
  }
}

/**
 * Publish Settings 
 *
 * @param int $form_state
 * @return string
 */
function classifieds_publish_settings($form_state) {

  //set the array variables for the forms
  $hour = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
  $minutes = array(1 => '00', 2 => '15', 3 => '30', 4 => '45');
  $mil = array(1 => 'AM', 2 => 'PM');

  //define attribute name for personal use
  $form['classifieds_publish_name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#description' => t('Enter the name of your publication'),
    '#default_value' => variable_get('classifieds_publish_name', 'My Publication'),
    '#maxlength' => 40,
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['classifieds_publish_desc'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#description' => t('This will be what is shown to customers, asking them if they want to advertise in the print edition.'),
    '#default_value' => variable_get('classifieds_publish_desc', ''),
    '#cols' => 80,
    '#rows' => 12,
    '#resizable' => FALSE,
  );

  $form['exclude'] = array(
    '#type' => 'fieldset',
    '#title' => t('Exclude categories'),
    '#description' => 'Select any categories that you do not want your publication to pick up.',
    '#collapsible' => TRUE,
  );

  $form['exclude']['fields']['classifieds_publish_exclude'] = array(
    '#type' => 'select',
    //'#title' => t('Select which categories to apply this attribute to'),
    '#multiple' => TRUE,
    '#default_value' => variable_get('classifieds_publish_exclude', ''),
    '#size' => 9,
    '#options' => _classifieds_generate_cat_options(),
    '#description' => 'Press CTRL to select multiple fields.'
  );

  $options = array(
    1 => t('Daily'),
    2 => t('Weekly'),
    3 => t('Bi-weekly'),
      //4 => t('Monthly'),
      //maybe add a custom option down here later...
  );

  $form['classifieds_publish_chars_per_line'] = array(
    '#title' => t('Characters per Line'),
    '#type' => 'textfield',
    '#description' => t('Enter how many characters per line your publication supports.'),
    '#default_value' => variable_get('classifieds_publish_chars_per_line', 25),
    '#size' => 3,
    '#required' => TRUE,
  );

  $form['classifieds_publish_schedule'] = array(
    '#title' => t('Publishing Schedule'),
    '#type' => 'select',
    '#description' => t('Pick how often your publication is printed.'),
    '#options' => $options,
    '#multiple' => FALSE,
    '#default_value' => variable_get('classifieds_publish_schedule', 1),
    '#ahah' => array(
      'event' => 'change',
      'path' => 'classifieds/js/publish_settings',
      'wrapper' => 'classifieds-publish-schedule-details',
      'method' => 'replace',
    )
  );

  //build part 2
  $form['schedule'] = array(
    '#type' => 'fieldset',
    '#title' => 'Schedule options',
    '#prefix' => '<div id="classifieds-publish-schedule-details">',
    '#suffix' => '</div>',
  );

  $form['schedule']['classifieds_publish_schedule_hour'] = array(
    '#title' => t('Due Time'),
    '#type' => 'select',
    '#options' => $hour,
    '#multiple' => FALSE,
    '#default_value' => variable_get('classifieds_publish_schedule_hour', ''),
  );

  $form['schedule']['classifieds_publish_schedule_minutes'] = array(
    '#type' => 'select',
    '#options' => $minutes,
    '#multiple' => FALSE,
    '#default_value' => variable_get('classifieds_publish_schedule_minutes', ''),
  );

  $form['schedule']['classifieds_publish_schedule_mil'] = array(
    '#type' => 'select',
    '#options' => $mil,
    '#multiple' => FALSE,
    '#default_value' => variable_get('classifieds_publish_schedule_mil', ''),
  );

  //generate date...

  $schedule_code = variable_get('classifieds_publish_schedule', NULL);

  if ($schedule_code == 2) {
    $number_of_days = 7;
  }
  elseif ($schedule_code == 3) {
    $number_of_days = 14;
  }

  //display the due date if its weekly or bi-weekly
  if ($schedule_code == 2 || $schedule_code == 3) {

    $days_of_week = array(1 => 'Sun', 2 => 'Mon', 3 => 'Tues', 4 => 'Wed', 5 => 'Thur', 6 => 'Fri', 7 => 'Sat');
    $day = array();

    for ($i = 1; $i <= $number_of_days; $i++) {

      if ($i > 7) {
        $j = $i - 7;
        $week = 'Week 2: ';
      }
      else {
        $j = $i;
        $week = 'Week 1: ';
      }

      $day[] = $week . $days_of_week[$j];
    }

    $form['schedule']['classifieds_publish_schedule_delivery_day'] = array(
      '#title' => t('Delivery Day'),
      '#type' => 'select',
      '#options' => $day,
      '#multiple' => FALSE,
      '#default_value' => variable_get('classifieds_publish_schedule_delivery_day', ''),
    );

    $form['schedule']['classifieds_publish_schedule_day'] = array(
      '#title' => t('Due Day'),
      '#type' => 'select',
      '#options' => $day,
      '#multiple' => FALSE,
      '#default_value' => variable_get('classifieds_publish_schedule_day', ''),
    );
  }

  /* $form['emails'] = array(
    '#title' => t('Editors'),
    '#type' => 'textarea',
    '#description' => t('The exported .xml will be e-mailed to the following recipients. One e-mail per line.'),
    '#default_value' => isset($default_value['emails']) ? $default_value['emails'] : '',
    '#cols' => 40,
    '#rows' => 6,
    '#resizable' => FALSE,
    //'#weight' => 15,
    );

    $form['autoex'] = array(
    //'#title' => t('Auto Export'),
    '#type' => 'checkboxes',
    '#options' => array(1=>'Auto-export classifieds.'),
    '#default_value' => isset($default_value['autoex']) ? array($default_value['autoex']) : '',
    ); */

  //return
  return system_settings_form($form);
}

/**
 * Publish Settings AHAH function
 */
function classifieds_js_publish_settings() {

  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];

  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);

  $form['#post'] = $_POST;
  $form['#redirect'] = FALSE;
  $form['#programmed'] = FALSE;
  $form_state['post'] = $_POST;

  // Prevents _form_builder_ie_cleanup() from incorrectly assigning the
  // first button in the form as the clicked button.
  // Wim Leers' AHAH Helper Module has more in-depth information.
  // @see the ahah_helper project
  $form_state['submitted'] = TRUE;

  //unset this or errors will be triggered...
  unset($form['name']);
  unset($form['chars_per_line']);

  drupal_process_form($form_id, $form, $form_state);

  // Rebuild the form.
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);

  //get rid of errors and messages
  unset($_SESSION['messages']);

  $output = $form['schedule'];

  // Get the JS settings so we can merge them.
  $javascript = drupal_add_js(NULL, NULL, 'header');
  $settings = call_user_func_array('array_merge_recursive', $javascript['setting']);

  drupal_json(array(
    'status' => TRUE,
    'data' => drupal_render($output),
    'settings' => array('ahah' => $settings['ahah']),
  ));
}

/**
 * Validates the add periodical form
 */
function classifieds_publish_settings_validate($form, &$form_state) {

  if (isset($form_state['values']['classifieds_publish_chars_per_line'])) { //needed because this isnt on every step
    //chars_per_line must be numeric
    if (!is_numeric($form_state['values']['classifieds_publish_chars_per_line'])) {

      form_set_error('classifieds_publish_chars_per_line', t('Characters per line must be a number.'));
    }

    //check to see that the number is reasonable also...
    if ($form_state['values']['classifieds_publish_chars_per_line'] < 1 || $form_state['values']['classifieds_publish_chars_per_line'] > 300) {

      form_set_error('classifieds_publish_chars_per_line', t('Characters per line must be between 1 and 300.'));
    }
  }
}

/*
 * Form for adding a byline
 */

function classifieds_publish_bylines_form($form_state) {

  //dpm($form_state);
  //this is an edit form...
  if (is_numeric(arg(5))) {

    //drupal_set_message('this is an edit form');
    //this is an edit form.... so modify the form where appropriate.
    $edit_form = TRUE;

    //Get basic default values
    $sql = "SELECT * FROM {classifieds_bylines} WHERE id = %d";

    $result = db_query($sql, arg(5));

    $data = db_fetch_object($result); //this var will hold the default values...
    //save default values
    //$data->id;

    $default_value['name'] = $data->name;

    //find these....
    //dpm($data);

    $default_value['category'] = $data->parent_tid;
    $default_value['sub_category'] = unserialize($data->child_tids);

    $fields = unserialize($data->fields);

    //pull out just the field tids...
    $field_names = array();
    foreach ($fields as $my_items) {
      $field_names[] = $my_items['id'];
    }

    $default_value['fields'] = $field_names;

    //remember weight order.... (step 3)
    $default_value['my_items'] = $fields;

    //go through appends
    $append = unserialize($data->append);

    //set the default values...
    foreach ($append as $key => $value) {
      $default_value[$key] = $value;
    }
  }
  else {

    $default_value = "";
  }

  // $form_state['storage']['step'] keeps track of what page we're on.
  if (!isset($form_state['storage']['step'])) {
    $form_state['storage']['step'] = 1;
  }

  //Don't lose our old data when returning to a page with data already typed in.
  //$default_value = '';
  if (isset($form_state['storage']['values'][$form_state['storage']['step']])) {
    $default_value = $form_state['storage']['values'][$form_state['storage']['step']];
  }


  switch ($form_state['storage']['step']) {
    case 1:

      //define attribute name for personal use
      $form['name'] = array(
        '#title' => t('Name'),
        '#type' => 'textfield',
        '#description' => t('Enter the name of your byline'),
        '#default_value' => isset($default_value['name']) ? $default_value['name'] : '',
        '#maxlength' => 40,
        '#required' => TRUE,
        '#size' => 30,
      );

      //create list of parent categories
      $parent_tids = _classifieds_get_parents();
      $parent_list = array();

      //go through tids and create names for them
      foreach ($parent_tids as $no => $tid) {
        $term = taxonomy_get_term($tid);
        $parent_list[$term->tid] = $term->name;
      }

      $form['category'] = array(
        '#type' => 'select',
        //'#title' => t('Select which categories to apply this attribute to'),
        '#multiple' => FALSE,
        '#default_value' => isset($default_value['category']) ? $default_value['category'] : '',
        '#options' => $parent_list,
        '#description' => 'Select what parent category to create a byline for.'
      );

      break;

    case 2:

      //find tid so we can look up what content type it is
      $tid = $form_state['storage']['values'][1]['category'];
      $node_type = _classifieds_get_cat_node($tid);


      //find out what subcategories this should apply to...
      $children = _classifieds_get_children($tid);

      //add names to the child tid list
      foreach ($children as $tid) {
        $term = taxonomy_get_term($tid);
        $children_list[$tid] = $term->name;
      }

      //add form
      $form['sub_category'] = array(
        '#type' => 'select',
        '#title' => t('Sub-categories'),
        '#multiple' => TRUE,
        '#size' => 5,
        '#default_value' => isset($default_value['sub_category']) ? $default_value['sub_category'] : '',
        '#options' => $children_list,
      );

      //find fields for this node type
      $fields = content_fields(); //get list of all extra cck fields
      $node_fields = array();

      //a list of forbidden cck fields
      $forbidden = array('field_image', 'field_phone', 'field_email');

      //add the location field to the list manuall-it is not showing up on node api load...
      $node_fields['location'] = 'location';

      //build list of fields
      foreach ($fields as $name => $data) {
        if ($data['type_name'] == $node_type && !in_array($name, $forbidden)) {
          $node_fields[$name] = $name;
        }
      }

      //cound fields
      if (count($node_fields) == 0) {
        $ghost_next = TRUE;
      }

      //build part 2
      $form['fields'] = array(
        '#title' => t('Fields to build byline out of'),
        '#type' => 'checkboxes',
        '#options' => $node_fields,
        '#default_value' => isset($default_value['fields']) ? $default_value['fields'] : array(),
      );

      break;

    case 3:

      //create list of fields
      //link tids to id
      //dpm($form_state);
      $fields = $form_state['storage']['values'][2]['fields'];

      $items = array();
      $i = 1;

      foreach ($fields as $name => $value) {
        if ($value != '0') {
          $items[] = array($name, $name, $i);
          $i++;
        }
      }

      //create dragable table
      $form['my_items'] = array();
      $form['my_items']['#tree'] = TRUE;

      foreach ($items AS $values) {

        $id = $values[0];
        $title = $values[1];

        //load default weights if it exists....
        if (isset($default_value['my_items'][$title])) {
          $weight = $default_value['my_items'][$title]['weight'];
        }
        else {
          $weight = $values[2];
        }


        $form['my_items'][$id] = array(
          'title' => array(
            '#type' => 'markup',
            '#value' => $title,
          ),
          'weight' => array(
            '#type' => 'weight',
            '#delta' => count($items),
            '#default_value' => $weight,
          ),
          'id' => array(
            '#type' => 'hidden',
            '#value' => $id,
          ),
        );
      }

      break;

    //last step
    case 4:

      //sort through items, and give the option to attach a prefix
      foreach ($form_state['storage']['values'][3]['my_items'] as $field_data) {
        //print out form text field
        $form['append-' . $field_data['id']] = array(
          '#title' => t('Append'),
          '#type' => 'textfield',
          '#field_prefix' => $field_data['id'] . ' ',
          '#default_value' => isset($default_value['append-' . $field_data['id']]) ? $default_value['append-' . $field_data['id']] : '',
          '#size' => 4,
          '#maxlength' => 15,
        );
      }

      break;
  }


  //Depending on what page we're on, show the appropriate buttons.
  if ($form_state['storage']['step'] > 1) {
    $form['previous'] = array(
      '#type' => 'submit',
      '#value' => t('<< Previous'),
    );
  }
  if ($form_state['storage']['step'] != 4) {

    $form['next'] = array(
      '#type' => 'submit',
      '#value' => t('Continue >>'),
    );

    if (isset($ghost_next)) {
      $form['next']['#disabled'] = TRUE;
      drupal_set_message('You must select a parent category with additional fields to continue.', 'error');
    }
  }
  else {

    $form['finish'] = array(
      '#type' => 'submit',
      '#value' => t('Add Byline'),
    );

    if ($edit_form == TRUE) {
      //modify the submit button
      $form['finish']['#value'] = 'Save Byline';

      //include a hidden field
      $form['update'] = array(
        '#type' => 'hidden',
        '#value' => $data->id,
      );
    }
  }


  //return
  return $form;
}

/**
 * Form validate for byline form
 */
function classifieds_publish_bylines_form_validate($form, &$form_state) {

  //dpm($form_state);

  if ($form_state['storage']['step'] == 2 && $form_state['clicked_button']['#id'] != 'edit-previous') {

    //make sure at least one field is selected
    $fields = $form_state['values']['fields'];

    $i = 0;

    foreach ($fields as $value) {
      if ($value != '0') {
        $i++;
      }
    }

    if ($i == 0) {
      form_set_error('fields', 'You must select a field.');
    }

    //make sure at least one sub-cat is selected
    if (count($form_state['values']['sub_category']) == 0) {
      form_set_error('sub_category', 'You must select at least one sub-category.');
    }

    //build list of child tid's already in database
    $sql = "SELECT id,child_tids FROM {classifieds_bylines} WHERE parent_tid=%d";
    $parent_tid = $form_state['storage']['values'][1]['category'];
    $result = db_query($sql, $parent_tid);
    $stored_child_tids = array(); //the array that holds all the tids that have been used in this cat
    //your basic while loop to get the data
    while ($tmp = db_fetch_array($result)) {
      $child_tids = unserialize($tmp['child_tids']);

      //dont put a tid in if we are editing, and it matches, or else we cant edit our own categories
      if ($tmp['id'] != arg(5)) {
        $stored_child_tids = array_merge($stored_child_tids, $child_tids);
      }
    }

    //create string to store errors
    $child_error_array = array();

    //make sure sub_category isnt already in the database
    foreach ($form_state['values']['sub_category'] as $tid) {
      if (in_array($tid, $stored_child_tids)) {
        $term = taxonomy_get_term($tid); //we inputted the tid
        $child_error_array[] = 'The sub-category "' . $term->name . '" already has a byline applied to it.';
      }
    }

    //show the sub cat errors if they exist
    if (count($child_error_array) > 0) {
      $child_string = '<ul>';
      foreach ($child_error_array as $string) {
        $child_string .= '<li>' . $string . '</li>';
      }
      $child_string .= '</ul>';
      form_set_error('sub_category', $child_string);
    }
  }
}

/**

 * Form submit for bylines
 */
function classifieds_publish_bylines_form_submit($form, &$form_state) {

  //Save the values for the current step into the storage array.
  $form_state['storage']['values'][$form_state['storage']['step']] = $form_state['values'];

  //Check the button that was clicked and change the step.
  if ($form_state['clicked_button']['#id'] == 'edit-previous') {
    $form_state['storage']['step']--;
  }
  elseif ($form_state['clicked_button']['#id'] == 'edit-next') {
    $form_state['storage']['step']++;
  }
  elseif ($form_state['clicked_button']['#id'] == 'edit-finish') {

    //set up variables
    $child_tids = serialize($form_state['storage']['values'][2]['sub_category']);
    $name = $form_state['storage']['values'][1]['name'];
    $fields = serialize($form_state['storage']['values'][3]['my_items']);
    $parent_tid = $form_state['storage']['values'][1]['category'];

    //dpm($form_state);

    $append = array();

    //run through step form values to save $append
    foreach ($form_state['storage']['values'][4] as $key => $data) {
      if (substr($key, 0, 7) == 'append-') {
        $append[$key] = $data;
      }
    }

    //serialze the array so we can save it in teh database
    $append = serialize($append);

    //update old entry, or create new entry
    if (isset($form_state['values']['update'])) {

      // this is an edit form
      db_query("UPDATE {classifieds_bylines} SET parent_tid=%d,child_tids='%s',name='%s',fields='%s',append='%s' WHERE id = %d", $parent_tid, $child_tids, $name, $fields, $append, $form_state['values']['update']);
    }
    else { //this is a new form
      db_query('INSERT INTO {classifieds_bylines} (parent_tid,child_tids,name,fields,append) VALUES(%d,"%s","%s","%s","%s")', $parent_tid, $child_tids, $name, $fields, $append);
    }

    //We must do this or the form will rebuild instead of refreshing.
    unset($form_state['storage']);

    //Go to this page after completing the form.
    $form_state['redirect'] = 'admin/classifieds/publish/bylines/list';

    drupal_set_message('Your byline has been saved.');
  }
}

/**
 * Returns the list of bylines
 */
function classifieds_publish_list_bylines() {

  //add a colspan...$row[] = array('data' => 'b', 'colspan' => 2);
  $headers = array(
    array('data' => t('Name'), 'field' => 'name'),
    array('data' => t('Parent Tid'), 'field' => 'parent_tid'),
    array('data' => t('Child Tids'), 'field' => 'child_tids'),
    array('data' => t('Fields'), 'field' => 'fields'),
    array('data' => ''), //'colspan' => 2
    array('data' => '')
  );

  //I WILL HAVE TO UPDATE THIS TO SHOW A STATUS....
  //i will need the duration attribute to show how many days this ad is suppose to go for....
  //the clock shouldnt start until it is published... so when its published, update the created date.
  //we need to only select the nodes created by the user...
  //and only of the content types that are classified ads
  $sql = "SELECT id,name,parent_tid,child_tids,fields FROM {classifieds_bylines}";
  $sql .= tablesort_sql($headers);

  //run query
  $result = db_query($sql);

  //set up data array
  $data = array();
  $i = 1;

  //your basic while loop to get the data
  while ($tmp = db_fetch_array($result)) {

    //add links...
    $tmp['link_edit'] = l(t('Edit'), "admin/classifieds/publish/bylines/add/" . $tmp['id']);
    $tmp['link_delete'] = l(t('Delete'), "admin/classifieds/publish/bylines/del/" . $tmp['id']);

    //format fields
    $fields = unserialize($tmp['fields']);
    $fields_string = '<ul>';

    //run through fields and pull out the names
    foreach ($fields as $name) {
      $fields_string .= '<li>' . $name['id'] . '</li>';
    }
    $fields_string .= '</ul>';

    //pull out child tids...
    $child_tids = unserialize($tmp['child_tids']);
    $child_string = '<ul>';
    foreach ($child_tids as $tid) {
      $term = taxonomy_get_term($tid);
      $child_string .= '<li>' . $term->name . '</li>';
    }
    $child_string .= '</ul>';

    //find parent tid's name
    $term = taxonomy_get_term($tmp['parent_tid']);

    //overwrite fields
    $tmp['fields'] = $fields_string;
    $tmp['child_tids'] = $child_string;
    $tmp['parent_tid'] = $term->name;

    //save the data
    $data[$i] = $tmp;

    //we dont want to show these fields
    unset($data[$i]['id']);

    //advance the counter
    $i++;
  }


  //save to output
  $output = theme('table', $headers, $data);

  //return
  return $output;
}

//not sure if we are using this...

function theme_classifieds_publish_bylines_form($form) {

  if (isset($form['my_items'])) {

    drupal_add_tabledrag('my_draggable_table', 'order', 'sibling', 'weight-group');

    $header = array('Title', 'Weight');

    //sort the items according to weight... this is needed for when
    //default_values are loaded or else they will be out of order
    // Obtain a list of columns
    foreach (element_children($form['my_items']) as $key => $row) {
      $weight_sort[$key] = $form['my_items'][$row]['weight']['#default_value'];
    }

    // Sort the data with volume descending, edition ascending
    // Add $data as the last parameter, to sort by the common key
    $my_items_array = element_children($form['my_items']);

    array_multisort($weight_sort, SORT_ASC, $my_items_array);

    //start to build the rows
    foreach ($my_items_array as $key) {

      $element = &$form['my_items'][$key];
      $element['weight']['#attributes']['class'] = 'weight-group';
      $element['itemid']['#attributes']['class'] = 'itemid';

      $row = array();
      $row[] = drupal_render($element['title']);
      $row[] = drupal_render($element['weight']) . drupal_render($element['id']);

      //Add a draggable class to every table row (<tr>)
      $rows[] = array('data' => $row, 'class' => 'draggable');
    }

    $output = theme('table', $header, $rows, array('id' => 'my_draggable_table'));

    $output .= drupal_render($form);

    return $output;
  }
}

/**
 * delete a byline
 */
function classifieds_publish_bylines_delete_form(&$form_state, $id) {

  //get the attrib name
  $byline = db_fetch_object(db_query("SELECT * FROM {classifieds_bylines} WHERE id=%d", $id));

  drupal_set_title(t('Are you sure you want to delete @title ?', array('@title' => $byline->name)));

  $form['question'] = array(
    '#type' => 'item',
    '#description' => t('This action cannot be undone.'),
  );

  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => arg(5),
  );

  $form['submit_delete_confirm'] = array(
    '#type' => 'submit',
    '#value' => t('Confirm'),
  );

  return $form;
}

/**
 * delete submit...
 */
function classifieds_publish_bylines_delete_form_submit($form, &$form_state) {

  //delete the item
  $sql = "DELETE FROM {classifieds_bylines} WHERE id = %d";
  db_query($sql, $form_state['values']['id']);

  //message
  drupal_set_message('Byline has been deleted.');

  //redirect the user
  $form_state['redirect'] = 'admin/classifieds/publish/bylines/list';
}
