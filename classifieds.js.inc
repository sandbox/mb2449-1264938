<?php

/**
 * @file
 * Contains all the ajax callback functions
 */

/**
 * This processes seller type changes on step 1
 */
function classifieds_step1_js() {

  //dpm($_POST);

  $output .= '<script type="text/javascript">
  Drupal.behaviors.classifiedimages = function() {
   $("#classifieds-price-wrapper").html(' . drupal_to_js(classifieds_ad_summary_update()) . ');
   }
  </script>';

  drupal_json(array('status' => TRUE, 'data' => $output));
}

/**
 * Called by jquery, this loads the days from whatever is selected
 * in create ad, step 3
 */
function classifieds_step3_js($id) {

  //include sales module, publish
  module_load_include('inc', 'classifieds', '/sales/classifieds.sales');
  module_load_include('inc', 'classifieds', '/publish/classifieds.publish');

  //define default output string, this will update ad summary
  $output .= '<script type="text/javascript">
  //Drupal.behaviors.classifiedAdSummary = function() {
   $("#classifieds-price-wrapper").html(' . drupal_to_js(classifieds_ad_summary_update()) . ');
   //}
  </script>';

  //take different action depending on what form element triggered
  switch ($id) {
    case 'days':
      //attach
      $output .= classifieds_publish_generate_preview_days($_POST);
      break;
    case 'keycode':
      //keycode doesnt update anything on step 3, just the ad summary
      break;
    case 'preview':
      $output .= classifieds_publish_generate_preview($_POST);
      break;
  }

  //return json
  drupal_json(array('status' => TRUE, 'data' => $output));
}

/**
 * rename this - JS Handler that re-adjusts the details section of
 * step 1 of the create ad form.
 */
function classifieds_adjust_details_js() {

  /**
   * AHAH FORM PROCESSING
   */
  //Include node.pages
  module_load_include('inc', 'node', 'node.pages');

  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];

  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);

  $form['#post'] = $_POST;
  $form['#redirect'] = FALSE;
  $form['#programmed'] = FALSE;
  $form_state['post'] = $_POST;

  // Prevents _form_builder_ie_cleanup() from incorrectly assigning the
  // first button in the form as the clicked button.
  // Wim Leers' AHAH Helper Module has more in-depth information.
  // @see the ahah_helper project
  //$form_state['submitted'] = TRUE;

  //Don't process submit handlers
  $form_state['rebuild'] = TRUE;

  //Process form
  //drupal_process_form($form_id, $form, $form_state); //THIS CAUSES ERROR-IM NOT SURE WHY... 
  
  // Rebuild the form
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);


  /**
   * RENDER OUTPUT
   */
  //Begin rendering output
  $output = drupal_render($form['field_sub_cat_menu']);

  //load function to build new tables...
  module_load_include('inc', 'classifieds', 'themes/classifieds.pages');

  $output .= _classifieds_build_details_table($form);

  //delete error message- the form will rebuilt, and validation will fail since
  //other fields arent filled in yet... so get rid of all error messages.
  unset($_SESSION['messages']['error']);


  /**
   * OUTPUT JSON
   */
  // Get the JS settings so we can merge them.
  $javascript = drupal_add_js(NULL, NULL, 'header');
  $settings = call_user_func_array('array_merge_recursive', $javascript['setting']);

  drupal_json(array(
    'status' => TRUE,
    'data' => theme('status_messages') . $output,
    'settings' => array('ahah' => $settings['ahah']),
  ));
}

/**
 * This proces the place an ad ahah block
 */
function classifieds_place_ad_js() {

  //include the place ad stuff
  module_load_include('inc', 'classifieds', 'includes/classifieds.place_ad');

  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];

  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);

  $form['#post'] = $_POST;
  $form['#redirect'] = FALSE;
  $form['#programmed'] = FALSE;
  $form_state['post'] = $_POST;

  // Prevents _form_builder_ie_cleanup() from incorrectly assigning the
  // first button in the form as the clicked button.
  // Wim Leers' AHAH Helper Module has more in-depth information.
  // @see the ahah_helper project
  $form_state['submitted'] = TRUE;

  drupal_process_form($form_id, $form, $form_state);

  // Once drupal_rebuild_form is called, we no longer have access to
  // $form_state['values'], so we merge it into $form_state['storage'].
  if (isset($form_state['values'])) {
    if (!isset($form_state['storage'])) {
      $form_state['storage'] = array();
    }
    $storage = $form_state['storage'];
    $values = $form_state['values'];
    $form_state['storage'] = array_merge($storage, $values);
  }

  // Rebuild the form.
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);

  //we need to set some variables and move the page
  if (isset($form_state['clicked_button'])) {

    //Set new place ad session var
    _classifieds_place_new_ad($form_state['storage']['content']['simple_select_child'], $form_state['storage']['content']['simple_select_parent']);

    //get name for place ad
    $term = taxonomy_get_term($form_state['storage']['content']['simple_select_parent']);
    $clean_name = strtolower(_classifieds_sanitize_url($term->name));

    //move to the proper place ad
    drupal_goto('classifieds/place/' . $clean_name);
  }
  //we need to send the form back so the user can continue
  else {
    $output = $form;

    // Get the JS settings so we can merge them.
    $javascript = drupal_add_js(NULL, NULL, 'header');
    $settings = call_user_func_array('array_merge_recursive', $javascript['setting']);

    drupal_json(array(
      'status' => TRUE,
      'data' => theme('status_messages') . drupal_render($output),
      'settings' => array('ahah' => $settings['ahah']),
    ));
  }
}

/*
 * Returns a list of cities for the autocomplete
 */

function classifieds_autocomplete_cities($string) {

  //retreive a list of cities
  $cities = variable_get('classifieds_list_of_cities', NULL);
  $cities = explode("\n", $cities);

  //filtered results
  $filtered_cities = array();

  //filter through the cities
  foreach ($cities as $city) {
    $string_length = strlen($string);

    $cut_city = substr($city, 0, $string_length);

    if (strcasecmp($string, $cut_city) == 0) {
      $filtered_cities[$city] = $city;
    }
  }

  drupal_json($filtered_cities);
}

/*
 * Returns a list of states for the autocomplete
 */

function classifieds_autocomplete_states($string) {

  //retreive a list of cities
  $states = 'Alabama
Alaska
Arizona
Arkansas
California
Colorado
Connecticut
Delaware
Florida
Georgia
Hawaii
Idaho
Illinois
Indiana
Iowa
Kansas
Kentucky
Louisiana
Maine
Maryland
Massachusetts
Michigan
Minnesota
Mississippi
Missouri
Montana
Nebraska
Nevada
New Hampshire
New Jersey
New Mexico
New York
North Carolina
North Dakota
Ohio
Oklahoma
Oregon
Pennsylvania
Rhode Island
South Carolina
South Dakota
Tennessee
Texas
Utah
Vermont
Virginia
Washington
West Virginia
Wisconsin
Wyoming';
  $states = explode("\n", $states);

  //filtered results
  $filtered_states = array();

  //filter through the cities
  foreach ($states as $state) {
    $string_length = strlen($string);

    $cut_state = substr($state, 0, $string_length);

    if (strcasecmp($string, $cut_state) == 0) {
      $filtered_states[$state] = $state;
    }
  }

  drupal_json($filtered_states);
}

/**
 * classfieds_ahah_reply - this renders the reply form
 * when submit is hit (reply form for individual ads)
 */
function classifieds_ahah_reply() {

  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];

  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);

  $form['#post'] = $_POST;
  $form['#redirect'] = FALSE;
  $form['#programmed'] = FALSE;
  $form_state['post'] = $_POST;

  // Prevents _form_builder_ie_cleanup() from incorrectly assigning the
  // first button in the form as the clicked button.
  // Wim Leers' AHAH Helper Module has more in-depth information.
  // @see the ahah_helper project
  $form_state['submitted'] = TRUE;

  drupal_process_form($form_id, $form, $form_state);

  // Once drupal_rebuild_form is called, we no longer have access to
  // $form_state['values'], so we merge it into $form_state['storage'].
  if (isset($form_state['values'])) {
    if (!isset($form_state['storage'])) {
      $form_state['storage'] = array();
    }
    $storage = $form_state['storage'];
    $values = $form_state['values'];
    $form_state['storage'] = array_smart_merge($storage, $values);
  }

  // Rebuild the form.
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);

  //Check for errors
  $errors = form_get_errors();
  if (count($errors) == 0) { //send the e-mail off
    $output = classifieds_reply_send_email($form_state);
  }
  else { //spit the form back out with errors
    $output = drupal_render($form);
  }

  // Get the JS settings so we can merge them.
  $javascript = drupal_add_js(NULL, NULL, 'header');
  $settings = call_user_func_array('array_merge_recursive', $javascript['setting']);

  drupal_json(array(
    'status' => TRUE,
    'data' => theme('status_messages') . $output,
    'settings' => array('ahah' => $settings['ahah']),
  ));
}
