<?php

/**
 * @file
 * This file contains the template pre processor functions
 */

/**
 * Process variables for aggregator-item.tpl.php.
 *
 * @see aggregator-item.tpl.php
 */
function template_preprocess_classifieds_create_ad(&$variables) {

  //include the collapsable javascript file
  drupal_add_js('misc/collapse.js');
  //include the js files for images
  drupal_add_js(drupal_get_path('module', 'classifieds') . '/attributes/attrib.images.js');
  //include the css file
  drupal_add_css(drupal_get_path('module', 'classifieds') . '/css/classifieds-create_ad.css');

  //get rid of breadcrumbs
  drupal_set_breadcrumb(array());


  //get form
  $form = &$variables['form'];

  //modify the location fields
  $form['group_contact']['field_location'][0]['city']['#required'] = TRUE;
  $form['group_contact']['field_location'][0]['province']['#required'] = TRUE;
  $form['group_contact']['field_location'][0]['province']['#title'] = 'State';

  //get rid of all path stuff
  unset($form['path']);

  //set step
  $variables['step'] = $form['step']['#value'];

  //set category vars
  $variables['category'] = $form['c_cat']['#description'];
  $variables['sub_category'] = $form['c_sub_cat']['#description'];
  drupal_render($form['c_cat']);
  drupal_render($form['c_sub_cat']);

  //set other vars
  $variables['buttons'] = drupal_render($form['buttons']);

  //set details and third category drop down
  if (isset($form['field_sub_cat_menu'])) {
    $variables['third_cat'] = drupal_render($form['field_sub_cat_menu']);
  }

  $variables['details'] = _classifieds_build_details_table($form, $form['field_sub_cat_menu']['#default_value']);

  //see if 'none' is selected
  if (isset($form['field_sub_cat_menu']) && $form['field_sub_cat_menu']['#default_value'] == NULL) {
    $variables['details'] = ''; //hide the details if this is the case
  }

  //set image text
  $variables['image_text'] = $form['images_text']['#description'] . '<br /><br />';
  drupal_render($form['images_text']);
}

/**
 * This is copied over from theme.inc in the themes folder in the views module.
 * It is the preprocessor stuff.
 * @param unknown_type $vars
 */
function template_preprocess_classifieds_filter_list(&$vars) {

  $form = &$vars['form'];

  //dpm($form);
  //get our search field
  $form['search']['#title'] = $form['#info']['filter-keys']['label'];
  $vars['search'] = drupal_render($form['search']);
  unset($form['#info']['filter-keys']['label']);

  //get our price field
  //$form['price']['min']['#title'] = $form['#info']['filter-field_price_value']['label'];
  //For some reason, we cant attach prefixes without the form looking fucked up 
  //(the prefix is above the label)... so i have to do it this ghetto ass way
  $vars['price_min'] = drupal_render($form['price']);
  $vars['price_min'] = str_replace('<input', '<span class="field-prefix">$</span><input', $vars['price_min']);

  $vars['price_max'] = drupal_render($form['price2']);
  $vars['price_max'] = str_replace('<input', '<span class="field-prefix">to $</span><input', $vars['price_max']);
  //unset($form['#info']['filter-field_price_value']['label']);
  //get our seller field
  //$form['seller']['#title'] = $form['#info']['filter-field_seller_value_many_to_one']['label'];
  $vars['seller'] = drupal_render($form['seller']);
  unset($form['#info']['filter-field_seller_value_many_to_one']['label']);

  //get our image field
  $vars['image'] = drupal_render($form['image']);
  unset($form['#info']['filter-field_image_list']['label']);

  //get our subcat if its there...
  if (isset($form['third_cat'])) {
    $vars['third_cat'] = drupal_render($form['third_cat']);
    unset($form['#info']['filter-third_cat']['label']);
  }

  //load the sub cats up
  $vars['sub_cats'] = classifieds_search_filter_build_sub_menu();

  // Put all single checkboxes together in the last spot.
  $checkboxes = '';

  if (!empty($form['q'])) {
    $vars['q'] = drupal_render($form['q']);
  }

  $vars['widgets'] = array();
  foreach ($form['#info'] as $id => $info) {
    // Set aside checkboxes.
    if (isset($form[$info['value']]['#type']) && $form[$info['value']]['#type'] == 'checkbox') {
      $checkboxes .= drupal_render($form[$info['value']]);
      continue;
    }
    $widget = new stdClass;
    // set up defaults so that there's always something there.
    $widget->label = $widget->operator = $widget->widget = NULL;

    $widget->id = $form[$info['value']]['#id'];
    if (!empty($info['label'])) {
      $widget->label = $info['label'];
    }
    if (!empty($info['operator'])) {
      $widget->operator = drupal_render($form[$info['operator']]);
    }
    $widget->widget = drupal_render($form[$info['value']]);
    $vars['widgets'][$id] = $widget;
  }

  // Wrap up all the checkboxes we set aside into a widget.
  if ($checkboxes) {
    $widget = new stdClass;
    // set up defaults so that there's always something there.
    $widget->label = $widget->operator = $widget->widget = NULL;
    $widget->widget = $checkboxes;
    $vars['widgets']['checkboxes'] = $widget;
  }

  // Don't render these:
  unset($form['form_id']);
  unset($form['form_build_id']);
  unset($form['form_token']);

  // This includes the submit button.
  $vars['button'] = drupal_render($form);
}

/**
 * Build Details Fields
 * Used in create-ad.tpl and adjust_fields_js
 */
function _classifieds_build_details_table(&$form) {

  //find out what fields are forbidden
  module_load_include('inc', 'classifieds', 'includes/classifieds.admin');
  $forbidden_fields = classifieds_admin_node_fields_get_forbidden();

  //count how many field items need to be included in the details fields...
  $detail_fields = array();
  $i = 1;
  $j = 0;
  $row_count = 0;
  $default_cols = 4; //number of cols the table should have
  //render the rest of the fields
  foreach ($form as $key => $value) {

    //if this is a field, we need to pull it out...
    if (substr($key, 0, 6) == 'field_' && !in_array($key, $forbidden_fields)) {

      //add this to the out put
      $detail_fields[$row_count][] = drupal_render($form[$key]);
      if ($i == $default_cols) {
        $i = 0;
        $row_count++;
      }

      $i++; //this is used for resetting the row
      $j++; //total counter
    }
  }

  //find the last item... and add a colspan too it...
  end($detail_fields);         // move the internal pointer to the end of the array
  $key = key($detail_fields);

  //set some variables
  $no_of_columns = count($detail_fields[$key]);
  $empties = $default_cols - $no_of_columns;

  //determine the col span
  if ($no_of_columns == 1) { //first case
    $colspan = $default_cols;
  }
  elseif ($no_of_columns == $default_cols) { //last case
    $colspan = 0;
  }
  else {
    $colspan = $empties + 1;
  }

  //modify the last entry
  if ($colspan > 0) {
    $detail_fields[$key][$no_of_columns - 1] = array('data' => $detail_fields[$key][$no_of_columns - 1], 'colspan' => $colspan);
  }

  //return
  $headers = NULL;

  if (count($detail_fields[0]) > 0) {
    return theme('table', $headers, $detail_fields);
  }
  else {
    return;
  }
}