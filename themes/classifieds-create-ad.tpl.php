<?php
// $Id: aggregator-item.tpl.php,v 1.1 2007/09/13 08:02:38 goba Exp $

/**
 * TODO: CREATE REAL DOCUMENTATION
 * @file aggregator-item.tpl.php
 * Default theme implementation to format an individual feed item for display
 * on the aggregator page.
 *
 * Available variables:
 * - $feed_url: URL to the originating feed item.
 * - $feed_title: Title of the feed item.
 * - $source_url: Link to the local source section.
 * - $source_title: Title of the remote source.
 * - $source_date: Date the feed was posted on the remote source.
 * - $content: Feed item content.
 * - $categories: Linked categories assigned to the feed.
 *
 * @see template_preprocess()
 * @see template_preprocess_aggregator_item()
 */
?>

<div id="classifieds">

  <table id="title">
    <tr><td colspan="2" class="cat"><?php print $category.' » '.$sub_category; ?></td></tr>
    <tr><td><?php print drupal_render($form['title']);?></td><td><?php print drupal_render($form['price']);?></td></tr>
  </table>

  <?php 
    //SWITCH BY STEPS...
    switch ($step) {
        case 1: //STEP 1

          //Set Title
          drupal_set_title('Create Your Ad');

          //pull out some fields first
          $for_sale_by = drupal_render($form['seller']);
          $description = drupal_render($form['body_field']);
          $images =  drupal_render($form['field_image']);

          //hide this if there are no details
          if(isset($third_cat) || $details != ''){
        ?>
              
        <div id="form_details">
          <fieldset class="group-story collapsible"><legend>Details</legend>
             <div class="description">Set your ad apart by filling in some additional details. </div>
             <div id="classifieds-details-wrapper">
                <?php
                  //print the third category select if it's there
                  if(isset($third_cat)){
                    print $third_cat;
                  }

                  print $details;

                ?>
              </div>
          </fieldset>
        </div>

        <?php
          }
        ?>

        <div id="description"><?php print $description;?></div>

        <div id="form_photos">
          <fieldset class="group-story collapsible"><legend>Photos</legend>
             <div class="description"><?php print $image_text; ?></div>
                <?php print $images; ?>
          </fieldset>
        </div>

      <?php print drupal_render($form);  ?>

      <div id="for-sale-by"><?php print $for_sale_by;?></div>
      <div class="clear"></div>    
          
          <?php 
            //END STEP 1
            break;
            
        case 2:

          //Set Title
          drupal_set_title('Preview Your Ad');

          //not much going on here, just spit out the form...
          print drupal_render($form);
          
          //END STEP 2
          break;
        case 3:

          //Set Title
          drupal_set_title('Publish Your Ad');

          //build the header text
          $header_text = '<div id="classifieds-print-header">';
          $header_text .= '<img src="/sites/all/themes/hiroshige-custom/newspaper.png" width="100" height="100" id="classifieds-print-paper"></img>';
          $header_text .= $form['description']['#description'];
          $header_text .= ' <em>'.$form['ad_rate']['#description'].'</em>';
          $header_text .= '</div>';
          drupal_render($form['description']);
          drupal_render($form['ad_rate']);
          
          //render form elements
          $online_ad = $form['online_ad']['#description'];
          drupal_render($form['online_ad']);
          $input_box = drupal_render($form['print_ad']);
          $preview_box = drupal_render($form['preview']);
          $run_select = drupal_render($form['run_ad']);
          $keyword = drupal_render($form['keyword']);
          $preview_days = drupal_render($form['preview_days']);
          $prev_runs = drupal_render($form['run_ad_disabled']);

          //print the headers...
          print $header_text;
          ?>

          <table>
            <tr>
              <td width="50%">
                <?php 
                  print $input_box;
                ?>
              </td>
              <td width="50%">
                <?php 
                  print $preview_box;
                  print $keyword;
                ?>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <div id="form_online_ad_text">
                  <fieldset class="group-story collapsible"><legend>Full Ad Text</legend> 
                     <div class="description"><?php print $online_ad; ?></div>
                  </fieldset>
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <?php
                  print $prev_runs;
                  print $run_select;
                ?>
              </td>
              <td>
                <?php 
                  
                  print $preview_days;
                ?>
              </td>
            </tr>
          </table>
          
          <?php 
          print drupal_render($form);
          
          //END STEP 3
          break;
    }
  ?>

  <div id="buttons"><?php print $buttons;?></div>

</div>