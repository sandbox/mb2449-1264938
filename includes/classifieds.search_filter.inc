<?php

/**
 * @file
 * This is responsible for modifying the views search filter
 */

/**
 * SearchField Form - This lets the admin associate what view
 * filter fields should be associated with each category...
 */
function classifieds_admin_search_fields_form($form_state) {

  // create a delete variable in case the form is new
  if (!isset($form_state['storage']['delete'])) {
    $form_state['storage']['delete'] = FALSE;
  }

  // if the delete button has been clicked, we must show the delete confirmation.
  if ($form_state['storage']['delete'] == TRUE && is_numeric(arg(4))) {

    //the form is being deleted
    //get the attrib name
    $rule = db_fetch_object(db_query("SELECT * FROM {classifieds_search_filter} WHERE sid=%d", arg(4)));

    $form['question'] = array(
      '#title' => t('Are you sure?'),
      '#type' => 'item',
      '#description' => t('This action will delete "@name" and can not be undone.', array('@name' => $rule->name)),
    );

    $form['sid'] = array(
      '#type' => 'hidden',
      '#value' => arg(4),
    );

    $form['submit_delete_confirm'] = array(
      '#type' => 'submit',
      '#value' => t('Confirm'),
    );
  }
  else { //show the regular form
    //Is this an edit form?
    $edit_form = FALSE;

    //determine if the argument has been set...
    if (is_numeric(arg(4))) {

      //this is an edit form.... so modify the form where appropriate.
      $edit_form = TRUE;

      //Get basic default values
      $sql = "SELECT * FROM {classifieds_search_filter} WHERE sid = %d";

      $result = db_query($sql, arg(4));

      $data = db_fetch_object($result); //this var will hold the default values...
      //save default values
      $sid = $data->sid;
      $name = $data->name;
      $filters_default = unserialize($data->filters);

      //Loop through category values
      $sql = "SELECT * FROM {classifieds_search2cat} WHERE sid = %d";

      $result = db_query($sql, $sid);

      //start default value
      $cat_options = array();

      while (($data = db_fetch_object($result)) !== FALSE) {
        if ($data->tid == 0) {
          $default = 1; //set form value
        }
        else {
          $cat_options[$data->tid] = $data->tid;
        }
      }
    }

    //create the form
    $form['name'] = array(
      '#title' => t('Name'),
      '#type' => 'textfield',
      '#description' => t('Enter a human-readable name for admin purposes.'),
      '#default_value' => $name,
      '#maxlength' => 40,
      '#required' => FALSE,
      '#size' => 30,
    );

    $form['apply_to'] = array(
      '#type' => 'fieldset',
      '#title' => t('Apply attribute'),
      '#description' => 'Please choose at least one of these options.',
      '#collapsible' => TRUE,
    );

    $form['apply_to']['fields'] = array(
      '#type' => 'fieldset',
      '#title' => t('Select categories'),
      '#description' => t('Select what categories this attribute should be applied to.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['apply_to']['fields']['list'] = array(
      '#type' => 'select',
      //'#title' => t('Select which categories to apply this attribute to'),
      '#multiple' => TRUE,
      '#default_value' => $cat_options,
      '#size' => 9,
      '#options' => _classifieds_generate_cat_options(),
      '#description' => 'Press CTRL to select multiple fields.'
    );

    //generate list of filters
    $view = views_get_view('classifieds_default');
    $filters = $view->display['default']->display_options['filters'];


    //dpm($filters);
    //define fields that are default or that shouldn't be controlled.
    $forbidden_options = classifieds_search_filter_get_forbidden();

    $filter_options = array();
    foreach ($filters as $name => $data) {
      if (!in_array($name, $forbidden_options)) {
        $filter_options[$data['expose']['identifier']] = $data['expose']['label'] . ' (' . $data['expose']['identifier'] . ')';
      }
    }

    //make sure this is an empty array if it wasnt set
    if (!isset($filters_default)) {
      $filters_default = array();
    }

    $form['filters'] = array(
      '#title' => t('Filters'),
      '#type' => 'checkboxes',
      '#default_value' => $filters_default,
      '#description' => (t('Please select any extra search filters that you would like to show for the above categories.')),
      '#options' => $filter_options,
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save Search Filter'),
      '#validate' => array('classifieds_admin_search_fields_form_validate_custom'), //only validate if we are submitting
      '#weight' => 100,
    );

    if ($edit_form == TRUE) {
      //modify the submit button
      $form['submit']['#value'] = 'Save Search Filter';

      //add an edit button...
      $form['submit_delete'] = array(
        '#type' => 'submit',
        '#value' => t('Delete Search Filter'),
        '#weight' => 105,
      );

      //include a hidden field
      $form['update'] = array(
        '#type' => 'hidden',
        '#value' => $sid,
      );
    }
  }

  return $form;
}

/**
 * SearchField Form - This validates the form...
 */
function classifieds_admin_search_fields_form_validate_custom($form, $form_state) {

  //check to make sure that name is filled in
  if (!$form['name']['#value']) {
    form_set_error('name', t('Please enter a name for this rule.'));
  }

  //check to make sure this attribute is a default or has a category selected
  if (empty($form['apply_to']['fields']['list']['#value'])) {

    form_set_error('default', t('This rule must be applied to a category.'));
  }

  //if categories are selected, we must make sure that each category only has one
  //attribute type associated wit hit
  //loop through the values
  foreach ($form['apply_to']['fields']['list']['#value'] as $key => $value) {

    //the key is the tid
    // see if this tid already has a attribute of this type assigned to it
    $sql = "SELECT * FROM {classifieds_search2cat} AS a JOIN {classifieds_search_filter} AS b ON a.sid=b.sid WHERE a.tid = %d";

    $result = db_query($sql, $key);

    $data = db_fetch_object($result);

    if (!empty($data) && $data->sid != arg(4)) { //arg 4 is the sid
      //find category name
      $cat = db_fetch_object(db_query("SELECT name FROM {term_data} WHERE tid=%d", $data->tid));

      //this category has an attribute of this type, so set a form error...
      $cat_error .= 'The category "' . $cat->name . '" already has a attribute of this type assigned to it (<a href="' . url('admin/classifieds/search/' . $data->sid) . '">' . $data->name . '</a>).<br />';
    }
  }

  //now that we ran through all the cats selected, see if it has built an error message
  if (isset($cat_error)) {
    form_set_error('list', $cat_error);
  }
}

/**
 *
 */
function classifieds_admin_search_fields_form_submit($form, &$form_state) {

  //check to see if the delete button was clicked
  if ($form_state['clicked_button']['#value'] == $form_state['values']['submit_delete']) {

    //set the variables, so the form rebuilds a confirmation message
    $form_state['rebuild'] = TRUE;
    $form_state['storage']['delete'] = TRUE;
  }

  //check to see if the confirm delete button was clicked
  elseif ($form_state['clicked_button']['#value'] == $form_state['values']['submit_delete_confirm']) {
    //delete the item

    $sid = $form_state['values']['sid'];

    //finally, delete the attribute itself
    $sql = "DELETE FROM {classifieds_search_filter} WHERE sid = %d";
    db_query($sql, $sid);

    //message
    drupal_set_message('Rule has been deleted.');

    //redirect the user
    $form_state['redirect'] = 'admin/classifieds/search';

    //delete this so drupal knows we need to submit the form
    unset($form_state['storage']);
  }

  //user this one for the submit button...
  elseif ($form_state['clicked_button']['#value'] == $form_state['values']['submit']) {

    //do normal save here...
    drupal_set_message('Rule saved.');

    //variables to insert
    $filters = serialize($form['filters']['#value']);
    $name = $form['name']['#value'];


    //CHECK TO SEE IF THIS IS AN INSERT OR AND UPDATE(EDIT) using the update hidden field containt the aid....
    if (is_numeric($form['update']['#value'])) {
      // this is an edit form so update
      db_query("UPDATE {classifieds_search_filter} SET name='%s',filters='%s' WHERE sid = %d", $name, $filters, $form['update']['#value']);

      $sid = $form['update']['#value'];
    }
    else {
      //this is a new form
      db_query('INSERT INTO {classifieds_search_filter} (name,filters) VALUES("%s","%s")', $name, $filters);

      //figure out the id number for the last record saved
      $sid = db_last_insert_id('classifieds_search_filter', 'sid');
    }

    //if this rule already exists and is being edited, then we must
    //delete all old categorys associated with this aid
    if (is_numeric($form['update']['#value'])) {
      //delete all categorys associated with the aid
      db_query('DELETE FROM {classifieds_search2cat} WHERE sid=%d', $sid);
    }

    //loop through all the fields that are checked....
    foreach ($form['apply_to']['fields']['list']['#value'] as $key => $value) {
      //the key and the value are both the tid that this value is to be applied to....
      db_query('INSERT INTO {classifieds_search2cat} (sid,tid) VALUES(%d,%d)', $sid, $value);
    }

    //We must do this or the form will rebuild instead of refreshing.
    unset($form_state['storage']);
  }
}

/**
 * This displays a list of search filters
 */
function classifieds_admin_search_fields_list() {

  //build header
  $headers = array(
    //array('data' => t('UID'), 'field' => 'uid', 'sort'=> 'desc'),
    array('data' => t('Name'), 'field' => 'name'),
    //array('data' => t('Rule Type'), 'field' => 'sale_type'),
    //array('data' => t('Applies to'), 'field' => 'apply_to'),
    array('data' => 'Filters Shown'),
    array('data' => 'Categories') //'colspan' => 2
  );

  //build sql
  $sql = "SELECT a.sid,name,filters FROM {classifieds_search_filter} AS a";
  $sql .= tablesort_sql($headers);

  //run query
  $result = db_query($sql);

  //set up data array
  $data = array();
  $i = 1;

  //your basic while loop to get the data
  while ($tmp = db_fetch_array($result)) {

    $main_string = ''; //do this or the categories accumulate
    $sub_string = '';

    //Find all the main categories associated with this rule
    $main_cats = db_query("SELECT * FROM {classifieds_search2cat} AS a JOIN {term_data} AS b ON a.tid=b.tid WHERE a.sid=%d AND a.tid IN (SELECT tid FROM {classifieds_node_per_cat})", $tmp['sid']);
    while (($data_main = db_fetch_object($main_cats)) !== FALSE) {
      $main_string .= $data_main->name . ', ';
    }

    //Find all the sub categories associated with this rule
    $sub_cats = db_query("SELECT * FROM {classifieds_search2cat} AS a JOIN {term_data} AS b ON a.tid=b.tid WHERE a.sid=%d AND a.tid NOT IN (SELECT tid FROM {classifieds_node_per_cat})", $tmp['sid']);
    while (($data_sub = db_fetch_object($sub_cats)) !== FALSE) {
      $sub_string .= $data_sub->name . ', ';
    }

    $cat_string = '<strong><em>Main Categories:</em></strong><br /> ' . substr($main_string, 0, -2) . '<br />';
    $cat_string .='<strong><em>Sub Cateogries:</em></strong><br /> ' . substr($sub_string, 0, -2);

    //add links...
    $tmp['categories'] = $cat_string;
    $tmp['name'] = l($tmp['name'], "admin/classifieds/search/add/" . $tmp['sid']);


    //go through filters and organize them...
    //drupal_set_message(dpm(unserialize($tmp['filters'])));

    $filter_string = ''; //reset the string
    $filters = unserialize($tmp['filters']);
    foreach ($filters as $name) {
      $filter_string .= '<li>' . $name . '</li>';
    }

    $tmp['filters'] = $filter_string;

    //save the data
    $data[$i] = $tmp;

    //we dont want to show these fields
    unset($data[$i]['sid']);

    //advance the counter
    $i++;
  }

  //save to output
  $output = theme('table', $headers, $data);

  //return
  return $output;
}

/**
 * This function will determine what search filters to show/hide based on
 * stored input. Since users can add additional exposed fields to the view,
 * they need a way to select which ones are shown for what fields.
 */
function classifieds_search_filter_main(&$form, $parent_tid, $child_tid) {

  //define filters for the parent_tid
  if (isset($parent_tid)) {
    $sql = "SELECT filters FROM {classifieds_search2cat} AS a JOIN {classifieds_search_filter} AS b ON a.sid=b.sid WHERE a.tid=%d";
    $result = db_query($sql, $parent_tid);
    $filters = unserialize(db_result($result));
  }

  //get child filters
  if (isset($child_tid)) {
    $sql = "SELECT filters FROM {classifieds_search2cat} AS a JOIN {classifieds_search_filter} AS b ON a.sid=b.sid WHERE a.tid=%d";
    $result = db_query($sql, $child_tid);
    $filters = unserialize(db_result($result));
  }

  //figure out what filter to show, based on what came back
  if (empty($filters)) {
    $filters = array();
  }

  //generate list of default filters
  $view = views_get_view('classifieds_default');
  $default_filters = $view->display['default']->display_options['filters'];

  //dpm($filters);

  $forbidden_options = classifieds_search_filter_get_forbidden();
  $default_filter_array = array();

  foreach ($default_filters as $name => $data) {
    //dont add in the forbidden fields....
    if (!in_array($name, $forbidden_options)) {
      $default_filter_array[] = $data['expose']['identifier'];
    }
  }

  //dpm($default_filter_array);
  //dpm($filters);
  //loop through form and hide all fields that arent suppose to be shown
  foreach ($form as $key => $value) {
    if (in_array($key, $default_filter_array) && !in_array($key, $filters)) {
      //dpm($form['#info']);
      $form[$key]['#access'] = FALSE; //unset might cause the view to break if no arguments are inputed
      //unset($form['#info']['filter-'.$key]);
    }
  }
}

/**
 * This function spits out a list of fields that the search filter should
 * never have access to
 */
function classifieds_search_filter_get_forbidden() {
  return array('vid', 'status', 'field_image_list', 'keys', 'seller', 'price', 'third_cat');
}

/**
 * This function returns an array menu listing of sub categories
 * given a tid
 */
function classifieds_search_filter_build_sub_menu() {

  //figure out what arguments are set in the url
  $arg1 = arg(1);
  $arg2 = arg(2);

  //find the classifieds vid
  $vid = _classifieds_get_vid();

  //create blank array
  $items = array();
  $i = 0;

  if (isset($arg2)) {
    //get tid
    $tid = _classifieds_get_term_by_name($arg2);

    //see if this term has chilren
    $taxonomy = taxonomy_get_tree($vid, $tid, -1, 1);

    foreach ($taxonomy as $id => $data) {
      $url = '/classifieds/' . $arg1 . '/' . $arg2 . '/' . _classifieds_sanitize_url($data->name);
      $items[$i] = array('data' => '<a href="' . $url . '">' . $data->name . '</a>', 'class' => 'leaf');
      $i++;
    }
  }
  elseif (isset($arg1)) {
    //get tid
    $tid = _classifieds_get_term_by_name($arg1);

    //see if this term has chilren
    $taxonomy = taxonomy_get_tree($vid, $tid, -1, 1);

    foreach ($taxonomy as $id => $data) {
      $url = '/classifieds/' . $arg1 . '/' . classifieds_sanitize_url($data->name);
      $items[$i] = array('data' => '<a href="' . $url . '">' . $data->name . '</a>', 'class' => 'leaf');
      $i++;
    }
  }
  else {

    //this is a search, display the parents...
    $taxonomy = taxonomy_get_tree($vid, 0, -1, 1);

    foreach ($taxonomy as $id => $data) {
      $url = '/classifieds/' . classifieds_sanitize_url($data->name);
      $items[$i] = array('data' => '<a href="' . $url . '">' . $data->name . '</a>', 'class' => 'leaf');
      $i++;
    }
  }

  //create the list
  $title = NULL;
  $type = 'ul';
  $attributes = array(
    'class' => 'sub-cat',
  );

  //build an item list, then convert it to a menu
  if (count($items) > 0) {
    $item_list = theme('item_list', $items, $title, $type, $attributes);
  }
  else {
    $item_list = NULL;
  }

  //return
  return $item_list;
}