<?php
// $Id: views-exposed-form.tpl.php,v 1.4.4.1 2009/11/18 20:37:58 merlinofchaos Exp $
/**
 * @file views-exposed-form.tpl.php
 *
 * This template handles the layout of the views exposed filter form.
 *
 * Variables available:
 * - $widgets: An array of exposed form widgets. Each widget contains:
 * - $widget->label: The visible label to print. May be optional.
 * - $widget->operator: The operator for the widget. May be optional.
 * - $widget->widget: The widget itself.
 * - $button: The submit button for the form.
 *
 * @ingroup views_templates
 */
?>

<?php if (!empty($q)): ?>
  <?php
    // This ensures that, if clean URLs are off, the 'q' is added first so that
    // it shows up first in the URL.
    print $q;
  ?>
<?php endif; ?>
<div id="classifieds-filter-list">

  <fieldset id="searchfieldset" style="background-color:#fff;">
    <legend id="searchlegend">Narrow your search...</legend>

    <table id="title">
      <tr>
        <td><?php print $search;?></td><td><?php print $price_min; ?></td>
        <td><label>&nbsp;</label><?php print $price_max; ?></td><td><?php print $seller; ?></td>
        <td><label>&nbsp;</label><?php print $image; ?></td><td style="padding-top:35px;"><?php print $button;?></td>
      </tr>
      <tr><td colspan="6"><hr /></td></tr>
      
      <tr><td colspan="6">
            <?php 
            foreach($widgets as $id => $widget):
            ?>
          <div id="field-block-<?php print str_replace('_', '-', substr($id, 7)); ?>">
                <?php if (!empty($widget->widget)): ?>
                  <label for="<?php print $widget->id; ?>"><?php print $widget->label.':'; ?></label>
                <?php endif; ?>
                <?php print str_replace('And:',' to&nbsp;',$widget->widget); ?>

          </div>
            <?php endforeach; ?>
      </td></tr>

    <?php if (!empty($sub_cats) || isset($third_cat)){ ?>
      <tr><td colspan="6"><hr /></td></tr>
      <tr><td colspan="6"><div style="margin-top:-1em;padding-bottom:1em;"><strong>Sub-categories:</strong></div>
        <?php
            if(isset($third_cat)){
              print $third_cat;
            } else {
              print $sub_cats;
            }
        ?>
      </td></tr>
    <?php } ?>
      
    </table>
 
  </fieldset>

</div>