<?php

/**
 * @file
 * Attributes responsible for building attributes, and modifying 
 * buildAd() form and searchFilter() form
 */
class attributes {

  /**
   * VARS
   */
  private $childTID;
  private $parentTID;
  //vars from build ad... necassary when attributes modify the form data...
  protected $classifiedData; //read only copy of the classifiedData...
  protected $defaultValues;
  protected $isFirstEdit;
  protected $isFirstLoad;

  /**
   * CONSTRUCTOR
   * @param type $form
   * @param type $form_state
   */
  public function __construct($classifiedData, $defaultValues=NULL, $isFirstEdit=NULL, $isFirstLoad=NULL) {

    $this->childTID = $classifiedData['child_tid'];
    $this->parentTID = $classifiedData['parent_tid'];

    $this->classifiedData = $classifiedData;
    $this->defaultValues = $defaultValues;
    $this->isFirstEdit = $isFirstEdit;
    $this->isFirstLoad = $isFirstLoad;
  }

  /**
   * buildAd - 
   * this is responsible for modifying the create your ad form
   * according to the attributes definitions
   */
  public function buildAd(&$form, $form_state) {

    //build array to hold attribute types and aids
    $attributes = $this->buildAttributes();

    //include the abstract class all attributes are based on
    module_load_include('inc', 'classifieds', '/attributes/attrib_rules/attrib');

    //loop through our $attribute array, and call our functions to modify the form
    foreach ($attributes as $type => $attrib_data) {

      //include the file
      module_load_include('inc', 'classifieds', '/attributes/attrib_rules/attrib.' . $type);

      //build attribute
      $object_name = $type . 'Attribute';

      //have the attribute modify the form
      $attribute = new $object_name($attributes, $this->classifiedData, $this->defaultValues, $this->isFirstEdit, $this->isFirstLoad);

      $attribute->buildAd($form, $form_state);
    }


    //THIS MAY NOT BE GETTING SAVED... WE MAY NEED TO PASS IN CLASSIFIED DATA FROM BUILD AD!
    //retrieve sesssion data, add attributes, and save
    $classified_data = classifieds_get_data($form['classified_id']['#value']);
    $classified_data['attributes'] = $attributes;
    classifieds_save_data($form['classified_id']['#value'], $classified_data);
  }

  /**
   * searchFilter
   */
  public function searchFilter(&$form, $form_state) {

    //include the css file
    drupal_add_css(drupal_get_path('module', 'classifieds') . '/css/classifieds-list.css');

    //set the theme
    $form['#theme'] = classifieds_filter_list;

    //modify price attributes
    $form['price']['min']['#size'] = 4;
    $form['price']['max']['#size'] = 4;
    $form['price']['max']['#title'] = '';

    //modify images
    //NOTE: You need the 'better exposed filters' module for this to work right
    $form['image']['#theme'] = 'select_as_checkboxes';
    $form['image']['#options'] = array('All' => '<Any>', 1 => 'Has photo');

    //modify submit button
    $form['submit']['#value'] = 'Find';

    // Build Breadcrumbs
    $breadcrumb = array();
    $breadcrumb[] = l('Home', '<front>');
    $breadcrumb[] = l('Classifieds', 'classifieds');


    //create attribute holding array
    $attributes = array();

    //load default attributes
    $this->getAttributesByDefault($attributes);


    //We need to figure out what (if any) main and sub category is being displayed
    //First off, is this a parent cat, or a is a parent AND child selected
    if (arg(1)) { //THIS IS A PARENT OR PARENT/CHILD LISTING
      //load parent term info - THIS SEQUENCE WILL NEED MODIFIED
      $parent_tid = _classifieds_get_term_by_name(arg(1));

      $parent = taxonomy_get_term($parent_tid);

      //set title
      drupal_set_title($parent->name);

      //load this parent categories attributes
      $this->getAttributesByTID($attributes, $this->parentTID);

      //check to see if this parent term has a child also
      if (arg(2)) {
        //get the child term information
        $child_tid = _classifieds_get_term_by_name(arg(2));

        $child = taxonomy_get_term($child_tid);

        //set parent breadcrumb
        $breadcrumb[] = l($parent->name, 'classifieds/' . _classifieds_sanitize_url($parent->name));

        //set child breadcrumb
        //$breadcrumb[] = l($child->name, 'classifieds/'._classifieds_sanitize_url($parent->name)
        //.'/'._classifieds_sanitize_url($child->name));
        //set title
        drupal_set_title($child->name);

        //load the parent categories attributes, then the childs
        $this->getAttributesByTID($attributes, $this->childTID);
      }
    }

    // Set Breadcrumbs
    drupal_set_breadcrumb($breadcrumb);


    //include the abstract class all attributes are based on
    module_load_include('inc', 'classifieds', '/attributes/attrib_rules/attrib');

    //load the attribute and allow them to modify the search filter as needed
    foreach ($attributes as $type => $attrib_id) {
      //include the file
      module_load_include('inc', 'classifieds', '/attributes/attrib_rules/attrib.' . $type);

      //build attribute
      $object_name = $type . 'Attribute';

      //have the attribute modify the form
      $attrib_rule = new $object_name($attributes, $this->classifiedData);

      //modify the filter
      $attrib_rule->searchFilter($form, $form_state);
    }

    //load the search filter function that determines what filters to hide and show
    //based on tid
    module_load_include('inc', 'classifieds', 'includes/classifieds.search_filter');
    classifieds_search_filter_main($form, $this->parentTID, $this->childTID);
  }

  //////////////////HELPER FUNCTIONS///////////////////////////

  private function buildAttributes() {

    //TODO: this is where caching would take place...
    //build array to hold attribute types and aids
    $attributes = array();

    //populate the array with the defaults
    $this->getAttributesByDefault($attributes);

    //now look for all attributes that apply to the parent tid - this will overwrite defaults
    $this->getAttributesByTID($attributes, $this->parentTID);

    //now look for all attributes that apply to the child tid - this will overwrite parents
    $this->getAttributesByTID($attributes, $this->childTID);

    //write a loop to look up the attributes information, and store it in array form
    //now that we now which ones are selected....
    foreach ($attributes as $key => $value) {
      $table = 'classifieds_attrib_' . $key;
      $find_attrib = db_fetch_array(db_query("SELECT * FROM {" . $table . "} WHERE attrib_id=%d", $value));
      $attributes[$key] = $find_attrib;
    }

    //dpm($attributes);
    //return
    return $attributes;
  }

  /**
   * gets all default attributes
   */
  private function getAttributesByDefault(&$attributes) {
    $sql = "SELECT * FROM {classifieds_attrib2cat} AS a JOIN {classifieds_attributes} AS b ON a.aid=b.aid WHERE a.tid = 0";
    $result = db_query($sql);

    while (($data = db_fetch_object($result)) !== FALSE) {
      $attributes[$data->attrib_type] = $data->attrib_id;
    }
  }

  /**
   * gets all attributes that match a tid
   */
  private function getAttributesByTID(&$attributes, $tid) {
    $sql = "SELECT * FROM {classifieds_attrib2cat} AS a JOIN {classifieds_attributes} AS b ON a.aid=b.aid WHERE a.tid = %d";
    $result = db_query($sql, $tid);

    while (($data = db_fetch_object($result)) !== FALSE) {
      $attributes[$data->attrib_type] = $data->attrib_id;
    }
  }

}
