<?php
/**
 * @file
 * Price Attribute Item
 */
class priceAttribute extends itemAttribute {

  /**
   * buildAd
   */
  public function buildAd(&$form, $form_state) {

    //look up the attribs properties
    $data = $this->getData($this->attributes[$this->name]);

    //check to see if we hide this
    if ($data->hide != 1) {

      //save default value 
      $default_value = $form_state['storage']['values'][1];

      //create price field
      $form['price'] = array(
        '#title' => $data->title,
        '#type' => 'textfield',
        '#field_prefix' => t('$'),
        '#default_value' => isset($default_value['price']) ? $default_value['price'] : '',
        '#size' => 4,
        '#required' => TRUE,
      );
    }


    if ($this->isFirstEdit) {//NOT AVAILABLE ANY MORE!
      $form['price']['#default_value'] = $form['#node']->price; //this should use default value
    }
  }

  /**
   * searchFilter -
   * modifieds the search filter block according to images....
   */
  public function searchFilter(&$form, $form_state) {

    //look up the attribs properties
    $data = $this->getData($this->attributes[$this->name]);

    //check to see if we hide this
    if ($data->hide == 1) {
      //unset field_price
      //unset($form['price']);
      $form['price']['#disabled'] = TRUE;
    }
    else {
      //set the field_price title
      $form['price']['#title'] = $data->title;
    }
  }

  ///////////////HELPER FUNCTIONS///////////////////////////
}

//TODO: add validate functionality to attributes!

/**
 * validates the 'Create Ad' form
 */
function classifieds_attrib_price_create_ad_validate($node, $attrib_id) {

  $q = classifiecds_attrib_price_select($attrib_id);

  //the price field is required....
  //if(!$node->price && $node->price != '0'){
  //  form_set_error('price', t('Please enter a '.$q->title));
  //}

  if (!is_numeric($node->price)) {
    form_set_error('price', t('Only numbers and decimals are allowed in ' . $q->title));
  }
}

/**
 * ADMIN FORMS
 */

/**
 * The elements that this attribute will add to the
 * create attribute admin form.
 */
function classifieds_attrib_price_form() {

  //check to see if the update field is here... if so modify the form.
  if (is_numeric(arg(5))) {
    //arg(4) for is an aid... use the aid to select your attrib_id
    //look up days
    $sql = "SELECT * FROM {classifieds_attrib_price} AS a JOIN {classifieds_attributes} as b ON a.attrib_id=b.attrib_id WHERE b.aid = %d";

    $result = db_query($sql, arg(5));

    $data = db_fetch_object($result); //this var will hold the default values...

    $title = $data->title;
    $hide = $data->hide;


    //set hidden field
    $form['update_price'] = array(
      '#type' => 'hidden',
      '#value' => $data->attrib_id,
    );
  }

  $form['price'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#description' => t('Enter the title to be displayed, i.e.: Rent,Charge,Price'),
    '#default_value' => $title,
    '#maxlength' => 25,
    '#required' => TRUE,
    '#size' => 20,
  );


  $form['price_hide'] = array(
    '#type' => 'checkboxes',
    '#default_value' => array($hide),
    '#options' => array(1 => 'Do not display the price field'),
  );
  return $form;
}

/**
 * Submit the elements added to the form
 *
 * this has to save the form information, and return the id
 */
function classifieds_attrib_price_form_submit($form, $form_state) {

  //make sure the hide value has a value
  if (!isset($form['price_hide']['#value'][1])) {
    $form['price_hide']['#value'][1] = 0;
  }

  //check to see if we should insert or update(for edit forms)
  if (is_numeric($form['update_price']['#value'])) {

    //insert into database
    db_query('UPDATE {classifieds_attrib_price} SET title="%s", hide=%d WHERE attrib_id=%d', $form['price']['#value'], $form['price_hide']['#value'][1], $form['update_price']['#value']);

    //return id
    $new_id = $form['update_price']['#value'];
  }
  else {
    //insert into database
    db_query('INSERT INTO {classifieds_attrib_price} (title,hide) VALUES("%s",%d)', $form['price']['#value'], $form['price_hide']['#value'][1]);

    //get id
    $new_id = db_last_insert_id('classifieds_attrib_price', 'attrib_id');
  }

  //return attrib_id
  return $new_id;
}

/**
 * print out information for a price attribute given a attrib_id
 */
function _classifieds_attrib_price_print($attrib_id) {

  //lookup the number of days...
  $sql = "SELECT * FROM {classifieds_attrib_price} AS a WHERE a.attrib_id = %d";

  $result = db_query($sql, $attrib_id);

  $data = db_fetch_object($result);

  //format results
  $output = array(
    'name' => 'Price',
    'desc' => '<li><em>Price Field Title:</em> ' . $data->title . '</li>',
  );

  if ($data->hide == 1) {
    $output['desc'] = '<li><em>Hidden:</em> YES</li>';
  }

  //return
  return $output;
}

/**
 * deletes this attibute type
 */
function classifieds_attrib_price_delete($attrib_id) {

  $sql = "DELETE FROM {classifieds_attrib_price} WHERE attrib_id = %d";

  db_query($sql, $attrib_id);
}

