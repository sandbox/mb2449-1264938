<?php

/**
 * @file
 * Sale rule: Extra Lines (Print only)
 */
function classifieds_sales_lines_add_form(&$form) {

  //check to see if the update field is here... if so modify the form.
  if (is_numeric(arg(5))) {
    //arg(5) for is an sid... use the sid to select your sale_id
    //look up days
    $sql = "SELECT * FROM {classifieds_sales_lines} AS a JOIN {classifieds_sale_rules} as b ON a.sale_id=b.sale_id WHERE b.sid = %d";
    $result = db_query($sql, arg(5));
    $data = db_fetch_object($result); //this var will hold the default values...
    //set hidden field
    $form['update_lines'] = array(
      '#type' => 'hidden',
      '#value' => $data->sale_id,
    );
  }


  $form['price'] = array(
    '#title' => t('Price per Line'),
    '#type' => 'textfield',
    '#description' => t('Enter the price per line. Enter 0 for FREE, decimals allowed.'),
    '#default_value' => isset($data->price) ? $data->price : 0,
    '#maxlength' => 6,
    '#required' => TRUE,
    '#field_prefix' => '$',
    '#size' => 4,
  );

  $form['free_lines'] = array(
    '#title' => t('Number of Free Lines'),
    '#type' => 'textfield',
    '#description' => t('Enter the number of lines to be included with each ad\'s base price'),
    '#default_value' => isset($data->free_lines) ? $data->free_lines : 4,
    '#maxlength' => 2,
    '#required' => TRUE,
    '#size' => 2,
  );

  $form['max_lines'] = array(
    '#title' => t('Maximum Number of Lines'),
    '#type' => 'textfield',
    '#description' => t('Enter the maximum number of lines an ad can use. Enter 0 for unlimited.'),
    '#default_value' => isset($data->max_lines) ? $data->max_lines : 0,
    '#maxlength' => 2,
    '#required' => TRUE,
    '#size' => 2,
  );
}

/**
 * Validate the elements added to the form
 */
function classifieds_sales_lines_add_form_validate($form, $form_state) {

  //make sure that this number is numeric
  if (!is_numeric($form_state['values']['free_lines'])) {
    form_set_error('free_lines', t('"Number of Free Lines" must be a number.'));
  }

  //make sure that this number is numeric
  if (!is_numeric($form_state['values']['max_lines'])) {
    form_set_error('max_lines', t('"Maximum Number of Lines" must be a number.'));
  }

  //make sure that this number is numeric
  if (!is_numeric($form_state['values']['price'])) {
    form_set_error('price', t('"Price per Line" must be a number.'));
  }

  //make sure this number is within an acceptable range
  if ($form_state['values']['price'] < 0 || $form_state['values']['price'] > 100) {
    form_set_error('price', t('"Price per Line" must be $0 (FREE) or greater, but less then $100'));
  }

  //make sure this number is within an acceptable range
  if ($form_state['values']['free_lines'] < 1 || $form_state['values']['free_lines'] > 50) {
    form_set_error('free_lines', t('"Number of Free Lines" must be between 1 and 50'));
  }

  //make sure this number is within an acceptable range
  if ($form_state['values']['max_lines'] < 0 || $form_state['values']['max_lines'] > 50) {
    form_set_error('max_lines', t('"Maximum Number of Lines" must be between 0(Unlimited) and 50'));
  }
}

/**
 * Submit the elements added to the form
 *
 * this has to save the form information, and return the id
 */
function classifieds_sales_lines_add_form_submit($form, $form_state) {

  //drupal_set_message(dpm($form));
  //check to see if we should insert or update(for edit forms)
  if (is_numeric($form['update_lines']['#value'])) {

    //insert into database
    db_query('UPDATE {classifieds_sales_lines} SET price=%f, free_lines=%d, max_lines=%d WHERE sale_id=%d', $form['price']['#value'], $form['free_lines']['#value'], $form['max_lines']['#value'], $form['update_lines']['#value']);

    //return id
    $new_id = $form['update_lines']['#value'];
  }
  else {
    //insert into database
    db_query('INSERT INTO {classifieds_sales_lines} (price,free_lines,max_lines) VALUES(%f,%d,%d)', $form['price']['#value'], $form['free_lines']['#value'], $form['max_lines']['#value']);

    //get id
    $new_id = db_last_insert_id('classifieds_sales_lines', 'sale_id');
  }

  //return attrib_id
  return $new_id;
}

/**
 * Delete a base sale rule
 */
function classifieds_sales_lines_delete($sale_id) {
  $sql = "DELETE FROM {classifieds_sales_lines} WHERE sale_id = %d";
  db_query($sql, $sale_id);
}

/**
 * standard functions that define what the rule affects (or is it effects?)
 */
function classifieds_sales_lines_is_online() {
  return FALSE;
}