<?php
/**
 * @file
 * This is a views plugin that allows a user to filter classified ads 
 * by price
 */
class classifieds_views_price_handler_field extends views_handler_field {

  function render($values) {

    //find node id
    $price = $values->classifieds_online_ads_price;

    if ($price == 0) {
      $price_label = 'FREE';
    }
    else {
      $price_label = '$' . number_format($price);
    }

    //return
    return $price_label;
  }

}